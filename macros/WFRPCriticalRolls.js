
let applyChanges = false;
const critdata = {
    "Sharp Hand Weapons": {
        "Arm": {
            "1": "Your blow grazes your opponent's arm, causing them to drop anything held in that hand.",
            "2": "Your blow skins your opponent's knuckles, painfully but not seriously. The arm may be used normally, but anything held in the hand is dropped.",
            "3": "Your blow opens a small cut on your opponent's forearm, incapacitating the hand for the next round and causing anything held in the hand to be dropped.",
            "4": "Your blow slices into the back of your opponent's hand. Anything held in the hand is dropped, and the hand is incapacitated for the next D4 rounds. Until medical attention is received, any actions attempted with this arm suffer a -10 penalty.",
            "5": "Your blow gashes your opponent's forearm. Anything held in the hand is dropped, and the arm is incapacitated for the next D6 rounds. Until medical attention is received, any actions attempted with this arm suffer a -20 penalty.",
            "6": "Your blow jabs painfully into your opponent's shoulder, slipping between any pieces of armour worn there. Anything held in the hand is dropped, and the arm is incapacitated for the next D6 rounds. Until medical attention is received, any actions attempted with this arm suffer a -20 penalty.",
            "7": "Your blow opens a deep wound in your opponent's forearm. Anything held in the hand is dropped, and the arm is incapacitated until medical attention is received.",
            "8": "Your blow carves into your opponent's shoulder, laying it open to the bone. Anything held in the hand is dropped, and the whole arm is incapacitated until medical attention is received.",
            "9": "Your blow cuts deeply into your opponent's lower arm, breaking the bones there. Anything held in the hand is dropped, and the arm is incapacitated until medical attention is received.",
            "10": "Your blow cuts deeply into your opponent's upper arm, breaking the bones there. Anything held in the hand is dropped, and the arm is incapacitated until medical attention is received.",
            "11": "Your blow strikes your opponent's hand, severing D3 fingers. Anything held in the hand is dropped, and the hand is incapacitated until medical attention is received. Your opponent's Dex is permanently reduced by 5 points per finger lost, and any future Dex advances are +5 rather than +10. The GM may decide to impose additional penalties to tests relating to manual skills, depending on the nature of the skill.",
            "12": "Your blow cuts off your opponent's hand at the wrist, and blood gushes from the wound at the rate of D4 Wounds per round until staunched. Anything held in the hand is dropped, and your opponent falls to the ground unconscious. The loss of one hand halves Dex permanently, and means that all future Dex advances are +5 instead of +10. The GM may impose further penalties to tests relating to manual skills, according to the circumstances and the nature of the skill.",
            "13": "Your blow severs your opponent's arm at the elbow, and blood gushes from the wound at the rate of D4 Wounds per round until staunched. Your opponent falls to the ground unconscious. The loss of the lower arm halves Dex permanently, and means that all future Dex advances are +5 instead of +10. The GM may impose further penalties to tests relating to manual skills, according to the circumstances and the nature of the skill.",
            "14": "Your blow severs your opponent's arm at the shoulder, and blood cascades from the wound at the rate of D6 Wounds per round until staunched. Your opponent falls to the ground unconscious. The loss of an arm reduces Dex by 60% permanently, and means that all future Dex advances are +5 instead of +10. The GM may impose further penalties to tests relating to manual skills, according to the circumstances and the nature of the skill.",
            "15": "Your blow severs a major artery. Death from shock and blood loss is instantaneous.",
            "16": "Your opponent's arm drops to the ground in a welter of blood. Your opponent staggers backward for D3 yards, and falls dead."
        }, "Head": {
            "1": "Your opponent ducks as your weapon whistles past at head height, losing nothing but the tip of an ear or a helmet plume. Next round, your opponent is off-balance and may do nothing except parry.",
            "2": "A glancing blow stuns your opponent, who may do nothing except parry for the next round.",
            "3": "A cut to the side of the head stuns your opponent, who may do nothing except parry for the next D4 rounds.",
            "4": "A blow to the head stuns your opponent, who may do nothing for the next round.",
            "5": "A solid blow to the head stuns your opponent, who may do nothing for the next D4 rounds.",
            "6": "A blow to the head stuns your opponent, who is knocked to the ground, dropping any hand-held items. Your opponent counts as prone for the next round, and may do nothing except parry for D4 rounds after that.",
            "7": "Your blow opens a flesh-wound in your opponent's scalp, causing blood to flow down into your opponent's eyes. All tests which require the use of vision (including 'to hit' rolls) are made at a penalty of -10 until medical attention is received.",
            "8": "Your blow shears into your opponent's jaw, breaking the jawbone and knocking out several teeth. Your opponent can do nothing except party for the next round; the necessity to keep spitting out blood and tooth fragments causes your opponent to attack at -10 for the rest of the combat. It will leave an impressive scar, modifying the character's Leadership and Fellowship tests by +/-10 at the GM's option, according to the circumstances.",
            "9": "Your opponent loses an eye (determine which one randomly, if necessary). Your opponent may do nothing the next round, and attacks at -10 until medical attention is received. Any sight-related skills are lost, including Night Vision bonuses, and Ballistic Skill is reduced by 20 points, subject to a minimum score of 5.",
            "10": "Your opponent is concussed, and slumps to the ground unable to move. Unconsciousness last for D4 hours or until medical attention is received.",
            "11": "Your opponent is severely concussed, and is unconscious for D10 hours or until medical attention is received. On regaining consciousness, your opponent must make a successful Toughness test or lose 10 points from each percentage characteristic as a result of lasting brain damage.",
            "12": "Your blow severs your opponent's carotid artery, drenching both of you in a fountain of blood. Your opponent collapses, and will bleed to death in D4 rounds unless medical attention is received.",
            "13": "Your blow cleaves open your opponent's skull, causing them to collapse instantly. Your opponent will die in D4 rounds unless medical attention is received, and must make a successful Toughness test at a -20 penalty or lose D3 x 10 points from each percentage characteristic as a result of permanent brain damage.",
            "14": "Your blow cuts deeply into your opponent's skull. Your opponent dies almost instantly.",
            "15": "Your blow slices your opponent's skull almost completely in half, and unless you make a successful Weapon Skill test it will take you the whole of the next round to pull it free. Death is instant.",
            "16": "Your opponent's head flies off in a random direction, landing 2D6 feet away."
        }, "Body": {
            "1": "Your blow nicks your opponent's chest, but not seriously. Your opponent is winded by the force of the blow, and may only parry for the next round.",
            "2": "Your blow makes a shallow cut in your opponent's groin. Doubled up in agony, your opponent may do nothing for the next round.",
            "3": "Your blow cuts into your opponent's chest, jarring a rib. Your opponent is knocked to the ground, and may only parry for the next D4 rounds until he regains his feet.",
            "4": "Your blow makes a painful cut into your opponent's abdomen. By some miracle you miss any vital organs. but your opponent collapses in pain, dropping any hand-held items, and may do nothing (except party with a shield if applicable) for the next D4 rounds until he regains his feet.",
            "5": "Your blow cuts your opponent deeply across the ribs, throwing him D5 feet backward and knocking him to the ground. Your opponent is stunned and counts as prone for D4 rounds, and thereafter may only parry for D4 rounds until back on his feet.",
            "6": "Your blow breaks some of your opponent's ribs. Your opponent may take no action for the next round, and thereafter suffers a -10 penalty to all attacks (and to other actions at the GM's discretion) until medical attention is received.",
            "7": "Your blow snaps your opponent's collar-bone. The pain reduces all characteristics by 1 or 10 points as appropriate, until medical attention is received.",
            "8": "Your blow cuts deeply into your opponent's side, severing muscles and sinews. The pain reduces all characteristics by 1 or 10 points as appropriate, until medical attention is received. Your opponent is knocked to the ground and remains prone for D4 rounds, and must make a successful Init test every round thereafter or remain prone; passing the test means that your opponent may only parry for the next D4 rounds while regaining his feet. All movement and agility based skills are lost until medical attention is received.",
            "9": "Your blow cuts deeply into your opponent's abdomen, and your opponent collapses unconscious, losing 1 Wound per round to internal bleeding until medical attention is received.",
            "10": "Your blow slides between your opponent's ribs and punctures a lung, causing it to collapse. Your opponent falls unconscious, losing D4 Wounds per round until medical attention is received. Even then, your opponent will be totally incapacitated for at least 10 weeks, and loses 1 point of Toughness permanently.",
            "11": "Your blow pierces your opponent's abdomen, causing internal injuries. Your opponent falls to the ground in agony, only able to parry, and must make a Toughness test each round or pass out. Medical attention will allow movement at half the cautious rate and all physical characteristics are halved for 3D6 weeks. Any skills involving movement of any kind are lost until a full recovery is made.",
            "12": "Your blow bites into your opponent's spine. Your opponent falls to the ground, unable to do anything until medical attention is received, and must make a successful Toughness test or be permanently paralysed from the waist down. Medical attention will allow movement at half the cautious rate and all physical characteristics are halved for 3D6 weeks. Any skills involving movement of any kind are lost until a full recovery is made.",
            "13": "Your blow pierces your opponent's abdomen, causing internal bleeding. Your opponent falls to the ground in agony, only able to parry, and must make a Toughness test each round or pass out. In addition, your opponent loses D4 Wounds per round to internal bleeding until medical attention is received. Medical attention will allow movement at half the cautious rate and all physical characteristics are halved for 3D6 weeks. Any skills involving movement of any kind are lost until a full recovery is made in 10 weeks.",
            "14": "Your blow slips between your opponent's ribs, piercing the heart and causing death in a matter of seconds.",
            "15": "Your opponent is disembowelled, and dies instantly.",
            "16": "Your opponent falls to the ground in two separate places. Death is instantaneous."
        }, "Leg": {
            "1": "A glancing blow to the calf makes your opponent stumble, dropping any hand-held object unless a successful Dex test is made.",
            "2": "A glancing blow unbalances your opponent, who may only parry for the next round.",
            "3": "Your blow causes a flesh-wound to your opponent's calf. Your opponent is knocked to the ground, dropping any hand-held object unless a successful Dex test is made. Your opponent may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with.",
            "4": "Your blow causes a shallow but painful cut. Your opponent's Movement allowance and Init are halved for D4 rounds.",
            "5": "Your blow alts into your opponent's ankle, damaging the hamstring and requiring an Init test to avoid being knocked down, dropping any hand-held object unless a successful Dex test is made. Your opponent may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with. Movement allowance and I are halved until medical attention is received.",
            "6": "Your blow cuts into your opponent's hip, damaging muscles and tendons and requiring a test against half Init to avoid being knocked down. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright. Movement allowance and Init are halved until medical attention is received.",
            "7": "Your blow strikes your opponent's calf, severing the hamstring. Your opponent is knocked down, and movement allowance and Init are halved until medical attention is received. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright.",
            "8": "Your blow opens a deep wound in the leg, severely damaging muscle tendons and blood vessels. Your opponent is knocked down, and loses 1 Wound per round to heavy bleeding until medical attention is received. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright.",
            "9": "Your blow cuts deeply into your opponent's thigh, severing a major blood vessel. Your opponent is knocked down, and loses D4 Wounds per round to heavy bleeding until medical attention is received. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright.",
            "10": "Your blow opens a gaping wound in your opponent's leg, almost cutting it off. Your opponent is knocked down, and loses D4 Wounds per round to heavy bleeding until medical attention is received. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright. Medical attention will allow movement at half the cautious rate and all physical characteristics are halved for 3D6 weeks. Any skills involving movement of any kind are lost until a full recovery is made.",
            "11": "Your opponent stares for a second at blood gushing from the stump of an ankle, before falling to the ground unconscious, losing D4 Wounds per round until medical attention is received. Loss of a foot halves Movement and Init permanently. The cost of advances to those characteristics, and of movement-related skills, is doubled.",
            "12": "Your blow lops off your opponent's leg at the knee. Your opponent collapses, losing D4 Wounds per round from bleeding until medical attention is received. Loss of the lower leg halves Movement and Init permanently. The cost of advances to those characteristics, and of movement-related skills, is doubled.",
            "13": "Your blow amputates your opponent's leg at the hip. Your opponent collapses, losing D6 Wounds per round from bleeding until medical attention is received. Loss of the leg halves Movement and Init permanently. The cost of advances to those characteristics is tripled, and the character loses all movement-related skills and may not regain them.",
            "14": "Your blow severs your opponent's femoral artery, showering blood over a wide area. Death from blood loss is almost instant.",
            "15": "Your blow amputates your opponent's leg, and your opponent collapses, hitting his head on the ground or on some protruding object with enough force to cave in the skull. Death is instantaneous.",
            "16": "Your blow amputates your opponent's leg, and carries on into the groin. Death from shock, blood loss and internal damage is almost instantaneous."
        }
    },
    "Blunt Hand Weapons": {
        "Arm": {
            "1": "Your blow bruises your opponent's arm, making them drop anything held in that hand.",
            "2": "Your blow skins your opponent's knuckles, painfully but not seriously The arm may be used normally, but anything held in the hand is dropped.",
            "3": "Your blow jars your opponent's forearm, incapacitating it for the next round and making them drop anything held in the hand.",
            "4": "Your blow smashes into the back of your opponent's hand. Anything held in the hand is dropped, and the hand is incapacitated for the next D4 rounds. Until medical attention is received, any actions attempted with this arm suffer a -10 penalty.",
            "5": "Your blow severely bruises your opponent's forearm. Anything held in the hand is dropped, and the arm is incapacitated for the next D6 rounds. Until medical attention is received, any actions attempted with this arm suffer a -20 penalty.",
            "6": "Your blow smashes painfully into your opponent's shoulder. Anything held in the hand is dropped, and the arm is incapacitated for the next D6 rounds. Until medical attention is received, any actions attempted with this arm suffer a -20 penalty.",
            "7": "Your blow smashes into your opponent's forearm, dislocating the bones there. Anything held in the hand is dropped, and the arm is incapacitated until medical attention is received.",
            "8": "Your blow smashes into your opponent's shoulder, dislocating it. Anything held in the hand is dropped, and the arm is incapacitated until medical attention is received.",
            "9": "Your blow smashes into your opponent's lower arm, breaking the bone. Anything held in the hand is dropped, and the arm is incapacitated until healed. Broken bones take D6+6 weeks to heal, and if not set by a character with medical skill, they have a 50% chance of setting crooked, reducing the character's Dex by up to half at the GM's discretion.",
            "10": "Your blow smashes into your opponent's upper arm, breaking the bones there. Anything held in the hand is dropped, and the arm is incapacitated until healed. Broken bones take D6+6 weeks to heal, and if not set by a character with medical skill, they have a 50% chance of setting crooked, reducing the character's Dex by up to half at the GM's discretion.",
            "11": "Your blow strikes your opponent's hand, breaking D3 fingers. Anything held in the hand is dropped, and the hand is incapacitated until healed. Broken bones take D6+6 weeks to heal, and if not set by a character with medical skill, they have a 50% chance of setting crooked, reducing the character's Dex by up to half at the GM's discretion.",
            "12": "Your blow shatters your opponent's hand. Splinters of bone sever arteries and poke through the skin, losing blood at the rate of 1 Wound per round until treated. Anything held in the hand is dropped, and your opponent may do nothing until treated. The hand is useless until the bones heal. The severe damage imposes a -20 modifier to all medical treatment rolls; failure means that the hand is useless, and failure by more than 30 points means it must be amputated. The loss of one hand halves Dex permanently, and means all future Dex advances are +5 instead of +10. The GM may add further penalties to manual skill tests, according to the circumstances and the nature of the skill.",
            "13": "Your blow shatters your opponent's arm at the elbow, causing a compound fracture which bleeds at D4 Wounds per round until treated. Your opponent can do nothing until treated, and medical treatment has a -20 modifier from the severity of the fracture. The lower arm is incapacitated until the bones heal. Broken bones take D6+6 weeks to heal, and if not set by a character with medical skill, they have a 50% chance of setting crooked, reducing the character's Dex by up to half at the GM's discretion. Failed medical treatment results in a permanently useless arm.",
            "14": "Your blow shatters your opponent's shoulder and upper arm. Compound fractures cause internal bleeding at the rate of D6 Wounds per round until medical attention is received. Your opponent can do nothing until treated. Medical treatment has a -20 modifier from the severity of the fracture. The arm is incapacitated until the bones heal. Broken bones take D6+6 weeks to heal, and if not set by a character with medical skill, they have a 50% chance of setting crooked, reducing the character's Dex by up to half at the GM's discretion. Failed medical treatment results in a permanently useless arm.",
            "15": "Your blow shatters bones. and a sharp fragment severs a major artery. Death from shock and blood loss is instantaneous.",
            "16": "Your blow smashes your opponent's arm and drives the wreckage into their chest cavity. destroying internal organs. Your opponent is thrown D5 yards by the blow, and falls dead."
        }, "Head": {
            "1": "Your opponent ducks as your weapon whistles past at head height, barely making contact. Next round, your opponent is off balance, and may do nothing except parry.",
            "2": "A glancing blow stuns your opponent, who may do nothing except parry for the next round.",
            "3": "A blow to the side of the head stuns your opponent, who may do nothing except parry for the next D4 rounds.",
            "4": "A blow to the head stuns your opponent, who may do nothing for the next round.",
            "5": "A solid blow to the head stuns your opponent, who may do nothing for the next D4 rounds.",
            "6": "A blow to the head stuns your opponent, who is knocked to the ground, dropping any hand-held items. Your opponent counts as prone for the next round, and may do nothing except parry for D4 rounds after that.",
            "7": "Your blow slightly dazes your opponent. All tests (including 'to hit' rolls) are made at a -10 penalty until medical attention is received.",
            "8": "Your blow smashes your opponent's jaw, breaking the jawbone and knocking out teeth. Your opponent can do nothing except parry for the next round; the necessity to keep spitting out blood and tooth fragments causes your opponent to attack at -10 for the rest of the combat. This wound will leave an impressive scar, modifying the character's Ld and Fel tests by +/-10 at the GM's option, according to the circumstances.",
            "9": "Your opponent loses an eye (determine which one randomly, if necessary). Your opponent may do nothing the next round, and attacks at -10 until medical attention is received. Any sight-related skills are lost, including Night Vision bonuses, and BS is reduced by 20 points, subject to a minimum score of 5.",
            "10": "Your opponent is concussed, and slumps to the ground unable to move. Unconsciousness last for D4 hours or until medical attention is received.",
            "11": "Your opponent is severely concussed, and is unconscious for D10 hours or until medical attention is received. On regaining consciousness, your opponent must make a successful Toughness test or lose 10 points from each percentage characteristic as a result of lasting brain damage.",
            "12": "Your blow fractures your opponent's skull, causing a brain hemorrhage. Your opponent collapses, and will bleed to death in D4 rounds unless medical attention is received. On regaining consciousness, your opponent must make a successful Toughness test or lose 10 points from each percentage characteristic as a result of lasting brain damage.",
            "13": "Your blow smashes your opponent's skull, causing them to collapse instantly. Your opponent will die in D4 rounds unless medical attention is received, and must make a successful Toughness test at a -20 penalty or lose D3 x 10 points from each percentage characteristic as a result of permanent brain damage.",
            "14": "Your blow snaps your opponent's neck. Your opponent dies almost instantly.",
            "15": "Your blow shatters your opponent's skull, spattering both of you with blood and brains. Death is instantaneous.",
            "16": "Your opponent's head flies off in a random direction. landing 2D6 feet away."
        }, "Body": {
            "1": "Your opponent is winded by the force of your blow, and may only parry for the next round.",
            "2": "Your blow lands in your opponent's groin. Doubled up in agony, your opponent may do nothing for the next round.",
            "3": "Your blow hits your opponent's chest. bruising a few ribs. Your opponent is knocked to the ground, and may only parry for the next D4 rounds until he regains his feet.",
            "4": "Your blow smashes into your opponent's abdomen. Your opponent collapses in pain, dropping any hand-held items, and may do nothing (except parry with a shield if applicable) for the next D4 rounds until he regains his feet.",
            "5": "Your blow severely winds your opponent, throwing him D5 feet backward and knocking him to the ground. Your opponent is stunned and counts as prone for D4 rounds, and thereafter may only parry for D4 rounds until back on his feet.",
            "6": "Your blow breaks some of your opponent's ribs. Your opponent may take no action for the next round, and thereafter must suffer a -10 penalty to all attacks (and to other actions at the GM's discretion) until medical attention is received.",
            "7": "Your blow snaps your opponent's collar-bone. The pain reduces all characteristics by 1 or 10 points as appropriate, until medical attention is received.",
            "8": "Your blow smashes into your opponent's side, breaking ribs and bruising internal organs. The pain reduces all characteristics by 1 or 10 points as appropriate, until medical attention is received. Your opponent is knocked to the ground and remains prone for D4 rounds, and must make a successful Init test every round thereafter or remain prone; passing the test means that your opponent may only parry for the next D4 rounds while regaining his feet. All movement and agility-based skills are lost until medical attention is received.",
            "9": "Your blow smashes into your opponent's abdomen, and your opponent collapses unconscious, losing 1 Wound per round to internal bleeding until medical attention is received.",
            "10": "Your blow fractures one of your opponent's ribs and drives a bone splinter into a lung, causing it to collapse. Your opponent falls unconscious, losing D4 wounds per round until medical attention is received. Even then, your opponent will be totally incapacitated for at least 10 weeks, and loses I point of Toughness permanently.",
            "11": "Your blow causes severe internal injuries. Your opponent falls to the ground in agony, only able to parry, and must make a Toughness test each round or pass out. Medical attention will allow movement at half the cautious rate and all physical characteristics are halved for 3D6 weeks. Any skills involving movement of any kind are lost until a full recovery is made.",
            "12": "Your blow smashes into your opponent's spine. Your opponent falls to the ground, unable to do anything until medical attention is received, and must make a successful Toughness test or be permanently paralysed from the waist down. If the test is successful, medical attention will allow movement at half the cautious rate and all physical characteristics are halved for 3D6 weeks. Any skills involving movement of any kind are lost until a full recovery is made.",
            "13": "Your blow smashes into your opponent's abdomen, rupturing a kidney and causing severe internal bleeding. Your opponent falls to the ground in agony, only able to parry, and must make a Toughness test each round or pass out. In addition, your opponent loses D4 Wounds per round to internal bleeding until medical attention is received. Medical attention will allow movement at half the cautious rate and all physical characteristics are halved for 10 weeks. Any skills involving movement of any kind are lost until a full recovery is made.",
            "14": "Your blow caves in your opponent's chest, bursting the heart and causing death in a matter of seconds.",
            "15": "Your opponent's abdomen is reduced to pulp. Death from multiple organ failure and massive internal bleeding is instantaneous.",
            "16": "Your opponent's spine is shattered. He falls to the ground, twitches for a second, and is still."
        }, "Leg": {
            "1": "A glancing blow to the calf makes your opponent stumble, dropping any hand-held object unless a successful Dex test is made.",
            "2": "A glancing blow unbalances your opponent. who may only parry for the next round.",
            "3": "Your blow lands heavily on your opponent's foot. Your opponent's Movement and Init are halved for the next round and he may only parry (and hop and curse) for that time.",
            "4": "Your opponent is knocked to the ground, dropping any handheld object unless a successful Dex test is made. Your opponent may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with.",
            "5": "Your blow hits a nerve centre, deadening the leg. Your opponent’s movement allowance and Init are halved for D4 rounds.",
            "6": "Your blow dislocates your opponent's ankle, requiring an Init test to avoid being knocked down, dropping any handheld object unless a successful Dex test is made. Your opponent may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with. Movement allowance and Init are halved until medical attention is received.",
            "7": "Your blow dislocates your opponent's knee, requiring a test against half Init to avoid being knocked down, dropping any handheld object unless a successful Dex test is made. Your opponent may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with. Movement allowance and Init are halved until medical attention is received.",
            "8": "Your blow dislocates your opponent's hip, requiring a test against half Init to avoid being knocked down, dropping any handheld object unless a successful Dex test is made. Your opponent may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with. Movement allowance and Init are halved until medical attention is received.",
            "9": "Your blow strikes your opponent's shin, breaking the bones of the lower leg. Your opponent is knocked down, dropping any handheld object unless a successful Dex test is made. Your opponent may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with, and the leg is incapacitated until the bones heal. Broken bones take D6+6 weeks to heal, and if not attended by a character with medical skill, they have a 50% chance of setting crooked, reducing the character's Movement and Init by up to half at the GM's discretion.",
            "10": "Your blow breaks your opponent's femur. Your opponent is knocked down, dropping any handheld object unless a successful Dex test is made. Your opponent may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with, and loses 1 Wound per round to heavy bleeding until medical attention is received. The leg is incapacitated until the bone heals. Broken bones take D6+6 weeks to heal, and if not attended by a character with medical skill, they have a 50% chance of setting crooked, reducing the character's Movement and Init by up to half at the GM's discretion.",
            "11": "Your blow shatters the bones of your opponent's foot, with shards of bone piercing several blood vessels and causing internal bleeding. Your opponent falls to the ground unconscious, losing D4 Wounds per round until medical attention is received. Broken bones take D6+6 weeks to heal, and if not attended by a character with medical skill, they have a 50% chance of the foot and ankle will being fused rigid, reducing Movement and Init to three-quarters of their former values permanently. The cost of advances to those characteristics, and of movement-related skills, is doubled.",
            "12": "Your blow smashes your opponent's knee. Your opponent collapses, losing D4 Wounds per round from internal bleeding until medical attention is received. Broken bones take D6+6 weeks to heal, and if not attended by a character with medical skill, they have a 50% chance of the foot and ankle will being fused rigid, reducing Movement and Init to three-quarters of their former values permanently. The cost of advances to those characteristics, and of movement-related skills, is doubled.",
            "13": "Your blow shatters your opponent's hip. Your opponent collapses, losing D6 Wounds per round from internal bleeding until medical attention is received. Broken bones take D6+6 weeks to heal, and if not attended by a character with medical skill, they have a 50% chance of the foot and ankle will being fused rigid, reducing Movement and Init to three-quarters of their former values permanently. The cost of advances to those characteristics, and of movement-related skills, is doubled.",
            "14": "Your blow Smashes your opponent's femur and pelvis, driving a sharp bone fragment into your opponent's femoral artery. Death from shock and internal bleeding is almost instant.",
            "15": "Your blow smashes your opponent's leg, and your opponent collapses, hitting his head on the ground or on some protruding object with enough force to cave in the skull. Death is instantaneous.",
            "16": "Your blow smashes through your opponent's leg, and carries on into the groin. Death from shock, blood loss and internal damage is almost instantaneous."
        }
    },
    "Teeth and Claws": {
        "Arm": {
            "1": "The attack grazes the victim's arm, causing them to drop anything held in that hand.",
            "2": "The attack scratches across the victim's knuckles, painfully but not seriously. Anything held in the hand is dropped.",
            "3": "The victim's forearm is heavily scratched, incapacitating the hand for the next round and causing anything held in it to be dropped.",
            "4": "The victim's hand and wrist are heavily scratched. Anything held in the hand is dropped, and the hand is incapacitated for the next D4 rounds. Until medical attention is received, any actions attempted with this arm suffer a -10 penalty.",
            "5": "The attack causes several deep scratches on the victim's forearm. Anything held in the hand is dropped, and the arm is incapacitated for the next D6 rounds. Until medical attention is received, any actions attempted with this arm suffer a -20 penalty.",
            "6": "The victim's shoulder is mauled. Anything held in the hand is dropped, and the arm is incapacitated for the next D6 rounds. Until medical attention is received, any actions attempted with this arm suffer a -20 penalty.",
            "7": "The victim's forearm is mauled. Anything held in the hand is dropped, and the arm is incapacitated until medical attention is received.",
            "8": "The attack shreds the flesh of the victim's shoulder, laying it open to the bone. Anything held in the hand is dropped, and the arm is incapacitated until medical attention is received.",
            "9": "The victim's lower arm is seriously mauled. Anything held in the hand is dropped, and the arm is incapacitated until medical attention is received.",
            "10": "The victim's upper arm is seriously mauled. Anything held in the hand is dropped, and the arm is incapacitated until medical attention is received.",
            "11": "The victim's hand and wrist are badly mauled or chewed, resulting in the loss of D3 fingers. Anything held in the hand is dropped, and the hand is incapacitated until medical attention is received. The victim's Dex is permanently reduced by S points per finger lost, and any future Dex advances are +5 rather than +10. The GM may decide to impose additional penalties to tests relating to manual skills, depending on the nature of the skill.",
            "12": "The victim's hand is either shredded by claws or bitten off at the wrist. Blood gushes from the wound at the rate of D4 Wounds per round until staunched. Anything held in the hand is dropped, and the victim falls to the ground unconscious. The loss of one hand halves Dex permanently, and means that all future Dex advances are +5 instead of +10. The GM may impose further penalties to tests relating to manual skills, according to the circumstances and the nature of the skill.",
            "13": "The victim's arm is completely shredded below the elbow, and blood gushes from the wound at the rate of D4 Wounds per round until staunched. The victim falls to the ground unconscious. The loss of the lower arm halves Dex permanently, and means that all future Dex advances are +5 instead of +10. The GM may impose further penalties to tests relating to manual skills, according to the circumstances and the nature of the skill.",
            "14": "The victim's shoulder is so badly mauled that the arm is torn off. Blood cascades from the wound at the rate of D6 Wounds per round until staunched. The victim falls to the ground unconscious. The loss of an arm reduces Dex by 60% permanently, and means that all future Dex advances are +5 instead of + 10. The GM may impose further penalties to tests relating to manual skills, according to the circumstances and the nature of the skill.",
            "15": "A major artery is clawed or bitten through. Death from shock and blood loss is instantaneous.",
            "16": "A welter of blood pumps from the ruins of the victim's arm. The victim staggers backward for D3 yards, and then falls dead."
        }, "Head": {
            "1": "The victim ducks away from a snap or a slash, losing nothing but the tip of an ear or a helmet plume. Next round, the victim is off balance, and may do nothing except parry.",
            "2": "A glancing blow (or fetid breath) stuns the victim, who may do nothing except parry for the next round.",
            "3": "A cuff to the side of the head (or the biting off of an ear) stuns the victim, who may do nothing except parry for the next D4 rounds.",
            "4": "A raking blow to the head stuns the victim, who may do nothing for the next round.",
            "5": "A solid blow to the head stuns the victim, who may do nothing for the next D4 rounds.",
            "6": "A blow to the head stuns the victim, who is knocked to the ground dropping any hand-held items. The victim counts as prone for the next round, and may do nothing except parry for D4 rounds after that.",
            "7": "The attack opens a series of flesh-wounds in the victim's scalp causing blood to flow down into the victim's eyes. All tests which require the use of vision (including 'to hit’ rolls) are made at a -10 penalty until medical attention is received.",
            "8": "The victim's jaw is broken, and several teeth are knocked out The victim can do nothing except parry for the next round; the necessity to keep spitting out blood and tooth fragments causes the victim to attack at -10 for the rest of the combat. This wound will leave an impressive scar, modifying the character's Ld and Fel tests by +/-10 at the GM's option, according to the circumstances.",
            "9": "The victim loses an eye (determine which one randomly, if necessary). The victim may do nothing the next round, and attacks at -10 until medical attention is received. Any sight-related skills are lost, including Night Vision bonuses, and BS is reduced by 20 points, subject to a minimum score of 5.",
            "10": "The victim is concussed, and slumps to the ground unable to move. Unconsciousness lasts for D4 hours or until medical attention is received.",
            "11": "The victim is severely concussed and is unconscious for D10 hours or until medical attention is received. On regaining consciousness, the victim must make a successful Toughness test or lose 10 points from each percentage characteristic as a result of lasting brain damage.",
            "12": "The attack slashes or bites open the victim's carotid artery, drenching both combatants in a fountain of blood. The victim collapses, and will bleed to death in D4 rounds unless medical attention is received.",
            "13": "A crushing blow or bite cracks open the victim's skull. The victim collapses, dying in D4 rounds unless medical attention is received, and must make a successful Toughness test at a -20 penalty or lose D3 x 10 points from each percentage characteristic as a result of permanent brain damage.",
            "14": "The victim's throat is utterly shredded. Death is almost instantaneous.",
            "15": "The victim's neck is clawed or bitten right through, crushing the vertebrae of the neck and breaking the spinal cord. Death is instantaneous.",
            "16": "The victim's head is clawed or bitten right off. Death is instantaneous."
        }, "Body": {
            "1": "The attack rakes across the victim's chest, but not seriously. The victim is winded by the force of the blow, and may only parry for the next round.",
            "2": "The attack strikes the victim's groin, drawing blood but causing no serious damage. Doubled up in agony, the victim may do nothing for the next round.",
            "3": "The attack tears into the victim's chest, stopped only by a rib. The victim is knocked to the ground, and may only parry for the next D4 rounds until he regains his feet.",
            "4": "The attack slashes into the victim's abdomen. By some miracle no vital organs are hit, but the victim collapses in pain, dropping any hand-held items, and may do nothing (except parry with a shield if applicable) for the next D4 rounds until he regains his feet.",
            "5": "The attack cuts the victim deeply across the chest, throwing him D3 feet backward and knocking him to the ground. The victim is stunned and counts as prone for D4 rounds, and thereafter may only parry for D4 rounds until back on his feet.",
            "6": "The attack causes a deep wound in the victim's side, grazing a kidney. The victim may take no action for the next round, and thereafter suffers a -10 penalty to all attacks (and to other actions at the GM's discretion) until medical attention is received.",
            "7": "The attack misses the throat by inches, but the victim's shoulder is severely mauled and the collar-bone is broken. The pain reduces all characteristics by l or 10 points as appropriate, until medical attention is received.",
            "8": "The victim's abdomen is badly mauled, shredding muscles and sinews. The pain reduces all characteristics by 1 or 10 points as appropriate, until medical attention is received. The victim is knocked to the ground and remains prone for D4 rounds, and must make a successful Init test every round thereafter or remain prone; passing the test means that the victim may only parry for the next D4 rounds while regaining his feet. All movement and agility-based skills are lost until medical attention is received.",
            "9": "Several deep wounds are opened in the victim's abdomen. The victim collapses unconscious, bleeding for 1 Wound per round until medical attention is received.",
            "10": "The attack snaps one of the victim's ribs and drives a splinter of bone into a lung, causing it to collapse. The victim falls unconscious, losing D4 wounds per round until medical attention is received. Even then, the victim will be totally incapacitated for at least 10 weeks, and loses 1 point of Toughness permanently.",
            "11": "The attack crushes the victim's abdomen, causing internal injuries. The victim falls to the ground in agony, only able to parry, and must make a Toughness test each round or pass out. Medical attention allows movement at half the cautious rate. All physical characteristics are halved for 3D6 weeks. Any skills involving movement of any kind are lost until a full recovery is made.",
            "12": "The attack smashes into the victim's spine, crushing several vertebrae. The victim falls to the ground, unable to do anything until medical attention is received, and must make a successful Toughness test or be permanently paralysed from the waist down. If the test is successful, medical attention allows movement at half the cautious rate. All physical characteristics are halved for 3D6 weeks. Any skills involving movement of any kind are lost until a full recovery is made.",
            "13": "The attack lays open the victim's abdomen, rupturing organs and causing internal bleeding. The victim falls to the ground in agony, only able to parry, and must make a Toughness test each round or pass out. in addition, the victim loses D4 Wounds per round to internal bleeding until medical attention is received. Medical attention allows movement at half the cautious rate. All physical characteristics are halved for 3D6 weeks. Any skills involving movement of any kind are lost until a full recovery is made in 10 weeks.",
            "14": "The attack smashes through the victim's ribs, tearing out the heart and causing death in a matter of seconds.",
            "15": "The victim's abdomen is ripped open, spilling viscera onto the ground. Death is instantaneous.",
            "16": "The victim is clawed or bitten in half, and falls to the ground in two separate places. Death is instantaneous."
        }, "Leg": {
            "1": "A light scratch across the calf makes the victim stumble, dropping any hand-held object unless a successful Dex test is made.",
            "2": "A glancing blow unbalances the victim, who may only parry for the next round.",
            "3": "The attack causes a flesh-wound to the victim's calf The victim is knocked to the ground, and will drop any hand-held object unless a successful Dex test is made. The victim may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with.",
            "4": "The attack draws blood from the victim's thigh. The victim's movement allowance and Init are halved for D4 rounds.",
            "5": "The victim's ankle is mauled, damaging the hamstring and requiring an Init test to avoid being knocked down, dropping any hand-held object unless a successful Dex test is made. Your opponent may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with. Movement allowance and Init are halved until medical attention is received.",
            "6": "The victim's hip is mauled, damaging muscles and tendons and requiring a test against half Init to avoid being knocked down, dropping any hand-held object unless a successful Dex test is made. Your opponent may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with. Movement allowance and Init are halved until medical attention is received.",
            "7": "The attack shreds the tendons of the victim's leg. The victim is knocked down, dropping any hand-held object unless a successful Dex test is made. Your opponent may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with, and movement allowance and Init are halved until medical attention is received.",
            "8": "The victim's leg is badly mauled. The victim is knocked down, dropping any hand-held object unless a successful Dex test is made. Your opponent may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with, and loses 1 Wound per round to heavy bleeding until medical attention is received.",
            "9": "The attack rips into the victim's thigh, severing a major blood vessel. The victim is knocked down, dropping any hand-held object unless a successful Dex test is made. Your opponent may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with, and loses D4 Wounds per round to heavy bleeding until medical attention is received.",
            "10": "The attack reduces the victim's leg to a bleeding ruin. The victim is knocked down, dropping any hand-held object unless a successful Dex test is made. Your opponent may only parry for the next D4 rounds until upright again, provided he is still holding something to parry with, and loses D4 Wounds per round to heavy bleeding until medical attention is received. Medical attention will allow movement at half the cautious rate and all physical characteristics are halved for 3D6 weeks. Any skills involving movement of any kind are lost until a full recovery is made.",
            "11": "The victim's foot is clawed or bitten off at the ankle. The victim falls to the ground unconscious, losing D4 Wounds per round until medical attention is received. Loss of a foot halves Movement and Init permanently. The cost of advances to those characteristics, and of movement-related skills. is doubled.",
            "12": "The victim's leg is utterly shredded below the knee, and if it is not already bitten off it will have to be amputated. The victim collapses, losing D4 Wounds per round from bleeding until medical attention is received. Loss of the lower leg halves Movement and Init permanently. The cost of advances to those characteristics, and of movement-related skills. is doubled.",
            "13": "The victim's hip is reduced to bloody tatters, and nothing is left to hold the leg onto the body. The victim collapses, losing D6 Wounds per round from bleeding until medical attention is received. Loss of the leg halves Movement and Init permanently. The cost of advances to those characteristics is tripled, and the character loses all movement-related skills and may not regain them.",
            "14": "The victim's femoral artery is shredded, showering blood over a wide area. Death from blood loss is almost immediate.",
            "15": "The victim's leg is clawed or bitten completely off, and the rest of the victim's body is thrown to one side, hitting his head on the ground or on some protruding object with enough force to cave in the skull. Death is instantaneous.",
            "16": "The attack crushes the bones of the victim's leg, sending sharp fragments into major organs and blood vessels in the leg and groin. Death from shock, blood loss and internal damage is almost instantaneous."
        }
    },
    "Arrows and Bolts": {
        "Arm": {
            "1": "Your shot nicks the back of the hand slightly, causing your target to drop anything held in it.",
            "2": "Your shot opens a cut on your target's hand, causing no serious damage but forcing anything held in the hand to be dropped.",
            "3": "Your shot strikes your target's hand, causing a superficial flesh wound. Any object held in the hand is dropped and the hand is incapacitated for the next round only.",
            "4": "Your shot cuts your target's hand badly. Anything held in the hand is dropped and the hand is incapacitated until medical attention is received.",
            "5": "Your shot transfixes your target's hand. Anything held in the hand is dropped and the hand is incapacitated until medical attention is received.",
            "6": "Your shot strikes your opponent's arm, grazing the bone and severing nerves. The arm is incapacitated for D6 rounds, due to extreme pain.",
            "7": "Your shot tears into your target's shoulder, ripping the ligament. The arm is incapacitated until medical attention is received.",
            "8": "Your shot transfixes your target's arm, poking out on the other side. Anything held in the hand is dropped, and the arm is incapacitated until medical attention is received. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "9": "Your shot strikes your target's forearm, slicing through ligaments. Anything held in the hand is dropped and the arm below the elbow is incapacitated until medical attention is received.",
            "10": "Your shot strikes your target's upper arm, embedding itself in the bicep. Anything held in the hand is dropped and the arm is incapacitated until medical attention is received. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "11": "Your target lets out a scream as your shot neatly removes D4 fingers. Anything held in the hand is dropped (along with the fingers). The target loses 1 Wound per round through blood loss until medical attention is received. Your target's Dex is permanently reduced by 5 points per finger lost, and any future Dex advances are +5 rather than + l0. The GM may decide to impose additional penalties to tests relating to manual skills, depending on the nature of the skill.",
            "12": "Your shot transfixes your target's wrist, severing an artery. Your target collapses and may do nothing until medical attention is received. D4 Wounds are lost per round due to bleeding until medical attention is received. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "13": "Your shot grazes the inside of your target's elbow, severing an artery. Your target collapses and may do nothing until medical attention is received. D4 Wounds are lost per round due to bleeding until medical attention is received.",
            "14": "Your shot sinks deep into your target's shoulder, causing blood to begin leaking into a lung. Your target collapses and may do nothing until medical attention is received. D6 Wounds are lost per round until medical attention is received. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "15": "Your shot passes through your target's arm, passing on into the chest cavity and striking the heart. Death is instantaneous.",
            "16": "Your shot grazes your target's shoulder and embeds itself in the neck. Death is almost instantaneous."
        }, "Head": {
            "1": "Your shot nicks an ear, but does no serious damage. Your target may only parry next round.",
            "2": "Your shot grazes your target's temple. Your target is stunned and may do nothing next round except party.",
            "3": "Your shot takes the skin off one of your target's cheeks. Your target may do nothing except parry for D4 rounds.",
            "4": "Your shot strikes the top of the head and glances off the skull. Your target is dazed and may do nothing at all next round.",
            "5": "Your shot glances off your target's temple. Your target is dazed and may do nothing at all for the next D4 rounds.",
            "6": "Your shot strikes your target's forehead, but for some reason it glances off instead of penetrating. Your target is knocked down, counts as prone for the next round, and may do nothing except parry for a further D4 rounds.",
            "7": "Your shot slices off your target's nose. As well as impeding your target's sense of smell somewhat, the pain gives your target a -10 modifier to all rolls until medical attention is received. The loss of the nose reduces the character's Fel score by -20 permanently.",
            "8": "Your shot glances off your target's jawbone, shattering teeth and splitting gums on the way. Your target may do nothing except parry for the next round, following which your target attacks at -10 'to hit' until medical attention is received.",
            "9": "Your shot glances off the bridge of the nose and punctures one of your target's eyes, destroying it totally. Your target may do nothing next round and attacks at -10 until medical attention is received. Any sight-related skills are lost (including Night Vision), and BS is reduced by 20%, to a minimum of 5.",
            "10": "Your shot smashes through your target's front teeth at an angle and exits through the jawbone beneath the ear. Your target falls to the ground unconscious and may do nothing for D4 hours or until medical attention is received, whichever is shorter. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "11": "Your shot strikes your target's forehead, fracturing the bone and driving it inwards towards the brain. Your target falls unconscious and may do nothing for D10 hours or until medical attention is received. Also, your target must test against Toughness or forfeit 10 points from all percentage characteristics as a result of permanent brain damage.",
            "12": "Your shot nicks the carotid artery and blood sprays across a wide area. Your target falls to the ground and will die in D6 rounds unless medical attention is received.",
            "13": "Your shot strikes square on to the nose, ripping its way through to penetrate the outer area of the brain. Your target falls to the ground and will die in D4 rounds unless medical attention is received. Additionally, your target must test against Toughness or lose 10 points from all percentage characteristics due to permanent brain damage.",
            "14": "Your shot transfixes the neck, severing the spinal column. Your target collapses, gurgles and then dies.",
            "15": "Your shot penetrates one of your target's eyes, going on into the brain. Death is instantaneous.",
            "16": "Your shot lands straight between your target's eyes. Death is instantaneous."
        }, "Body": {
            "1": "Your target loses balance in an attempt to avoid your shot, which scrapes across your target's chest. Your target can do nothing except parry next round while regaining balance.",
            "2": "Your shot nicks the groin, causing your target a great deal of pain. Your target may do nothing for the whole of the next round.",
            "3": "Your shot grazes the chest, glancing off a rib. Your target falls to the ground and may do nothing except parry for the next D4 rounds until upright again.",
            "4": "Your shot causes a serious flesh-wound in the groin. Your target falls to the ground in agony, dropping anything hand-held, and is not able to do anything except parry with a shield (if he has one) for the next D4 rounds while staggering upright.",
            "5": "Your shot smashes into the chest just below the shoulder, sending your target sprawling to the ground. Your target is stunned for D4 rounds, and thereafter may do nothing except parry for D4 rounds while clambering upright.",
            "6": "Your shot strikes your target's abdomen: painful but not serious. Your target attacks at -10 to hit until medical attention is received.",
            "7": "Your shot strikes your target's collar-bone, fracturing it. The agony reduces all characteristics by 1 or 10 points as appropriate until medical attention is obtained.",
            "8": "Your shot lodges itself between your target's ribs. All characteristics are reduced by 1 or 10 points as appropriate and movement allowance is reduced by half until medical attention is received. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "9": "Your shot embeds itself in your target's abdomen with a satisfying thunk. Your target loses consciousness and collapses to the ground, losing 1 Wound per round from internal bleeding until medical attention is received. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "10": "Your shot punctures a lung. Your target falls unconscious, losing D4 Wounds per round until medical attention is received. Even following this, your target will be totally incapacitated for at least 10 weeks and permanently loses 1 point of Toughness. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "11": "Your shot rips its way into your target's abdomen, severely damaging several internal organs. Your target collapses in agony, unable to do anything until medical attention is received and losing D4 Wounds per round. A test against 'Toughness must be made each round in order to remain conscious. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "12": "Your shot strikes your target's spine, squeezing between the vertebrae to the spinal cord. Your target falls to the ground, may do nothing at all until medical attention is received, and must test against Toughness or be permanently paralyzed from the waist down. A failure on any medical treatment roll to remove your missile results in automatic paralysis from the waist down.",
            "13": "Your shot fractures the target's spine, lodging itself half-way through. Your target collapses unconscious. All characteristics are halved until a full recovery is made and D4 Wounds are lost per round due to internal bleeding until medical attention is received. it takes 10 weeks for a full recovery to be made. Until then all skills involving movement are lost. A failure on any medical treatment roll to remove your missile results in automatic paralysis from the waist down.",
            "14": "Your shot sinks into your target’s abdomen, damaging several vital organs. Your target collapses, coughing up blood. Death is almost instantaneous.",
            "15": "Your shot totally destroys several of the vertebrae in your target's upper spine. The neck sags dangerously, and then there is a snap. Death is almost instantaneous.",
            "16": "Your shot slips between the ribs to your target's heart, causing it to cease functioning instantly, along with its owner."
        }, "Leg": {
            "1": "Your shot grazes the back of the calf, making your target stumble and drop any hand-held object unless they make a Dex test.",
            "2": "Your target loses balance while attempting to avoid your shot, and may only parry in the next round.",
            "3": "Your shot grazes the thigh, causing your target to lose balance and fall to the ground. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright.",
            "4": "Your shot slices into the leg with some force. Your target's Movement and Init scores are halved for D4 rounds while hopping around in agony.",
            "5": "Your shot damages the tendons in your target's ankle, incapacitating it. Movement and Init are halved until medical attention is obtained and your target must make an Init test or fall down. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright.",
            "6": "Your shot transfixes your target's calf. Movement and Init are halved until medical attention is received. Your target must also test against Init or go down. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "7": "Your shot transfixes your target's thigh. Movement and Init are halved until medical attention is received. Your target must test against Init or go down. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright. Failure on any medical treatment roll to remove the missile means an extra D4 damage.",
            "8": "Your shot transfixes your target's foot, pinning it to the ground. All tests relating to movement are automatically failed until it is removed; until then, your target can only move in very small circles. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "9": "Your shot cuts deeply into the leg, tearing through the muscle and grazing the bone to emerge on the opposite side. Your target collapses and loses 1 Wound per round until medical attention is received. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "10": "Your shot sinks deep into the thigh, piercing a major blood vessel. Your target is knocked down and may not get up until a successful Init test is made. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright. Your target loses 1 W per round until treated. A failure on any medical treatment roll to remove the missile results in an extra D4 damage.",
            "11": "Your shot rips through the leg, piercing an artery. Your target crashes to the ground and drops anything hand-held. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright. D4 Wounds are lost per round until medical attention is received. Your target may only stand or walk if supported by at least one other character. Failure on any medical treatment roll to remove your missile means an additional D4 damage.",
            "12": "Your shot completely destroys the ankle and your target collapses, only able to parry and losing D4 Wounds per round until medical attention is received. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "13": "Your shot crunches through your target's kneecap and out the other side, wrecking the knee. Your target falls to the ground unconscious from the pain. D4 Wounds are lost per round until medical attention is received. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "14": "Your shot glances off the pelvis and lodges itself in your target's groin. Your target falls to the ground unconscious from the pain, and D4 Wounds are lost per round until medical attention is received. A failure on any medical treatment roll to remove your missile results in an additional D4 damage.",
            "15": "Your shot strikes a major blood vessel in the leg. Blood splatters everywhere and your target collapses to the ground. Death from shock and blood loss is almost instantaneous.",
            "16": "Your shot strikes your target's upper leg, grazing the pelvis and carrying on into the abdomen to cause terminal damage to several internal organs. Your target dies instantaneously from shock and blood loss."
        }
    },
    "Firearms": {
        "Arm": {
            "1": "Your shot grazes the back of the hand slightly, causing your target to drop anything held in it.",
            "2": "Your shot jars your target's hand, causing no serious damage but forcing anything held in the hand to be dropped.",
            "3": "Your shot strikes your target's hand, causing a superficial flesh wound. Any object held in the hand is dropped and the hand is incapacitated for the next round only.",
            "4": "Your shot passes cleanly through your target's hand. Anything held in the hand is dropped and the hand is incapacitated until medical attention is received.",
            "5": "Your shot strikes your target's hand, breaking some minor bones. Anything held in the hand is dropped and the hand is incapacitated until medical attention is received.",
            "6": "Your shot strikes your opponent's arm, grazing the bone. The arm is incapacitated for D6 rounds, due to extreme pain.",
            "7": "Your shot tears into your target's shoulder. The arm is incapacitated until medical attention is received.",
            "8": "Your shot penetrates your target's arm totally, tearing its way through the muscle. Anything held in the hand is dropped, and the arm is incapacitated until medical attention is received.",
            "9": "Your shot strikes your target's forearm, smashing the bone. Anything held in the hand is dropped and the arm below the elbow is incapacitated until medical attention is received.",
            "10": "Your shot strikes your target's upper arm, embedding itself in the bicep. Anything held in the hand is dropped and the arm is incapacitated until medical attention is received. A failure on any medical treatment roll to remove your shot results in an additional D4 damage.",
            "11": "Your target lets out a scream as your shot smashes D4 fingers. Anything held in the hand is dropped (along with the fingers). The target loses 1 Wound per round through blood loss until medical attention is received. Your target's Dex is permanently reduced by 5 points per finger lost, and any future Dex advances are +5 rather than +10. The GM may decide to impose additional penalties to tests relating to manual skills, depending on the nature of the skill.",
            "12": "Your shot smashes the bones of the wrist. Blood sprays up your target's arm as multiple arteries and veins are severed. Anything held in the hand is dropped and your target falls to the ground unconscious, losing D4 Wounds per round until medical attention is received.",
            "13": "Your shot strikes the elbow, shattering the bone and driving splinters through several arteries. Your target collapses and may do nothing until medical attention is received. D4 Wounds are lost per round due to bleeding until medical attention is received.",
            "14": "Your shot sinks deep into your target's shoulder, shattering bones and rupturing blood vessels. Your target collapses and may do nothing until medical attention is received. D4 Wounds are lost per round until medical attention is received.",
            "15": "Your shot strikes a major blood vessel, and your target collapses to the ground. Death from shock and blood loss is almost instantaneous.",
            "16": "Your shot strikes the side of the shoulder, glancing off the shoulder-blade and downward into the heart. Death is almost instantaneous."
        }, "Head": {
            "1": "Your shot nicks an ear. Your target may only parry next round.",
            "2": "Your shot grazes your target's neck. Your target is stunned and may do nothing next round except parry.",
            "3": "Your shot glances off one of your target's cheekbones. Your target is stunned, and may do nothing except parry for D4 rounds.",
            "4": "Your shot grazes your target's skull. Your target is dazed and may do nothing at all next round.",
            "5": "Your shot gazes your targets temple. Your target is dazed and may do nothing at all for the next D4 rounds.",
            "6": "Your shot glances off your target's forehead, knocking him down. Your target counts as prone for the next round, and may do nothing except parry for a further D4 rounds.",
            "7": "Your shot blows off your opponent's nose. As well as impeding your targets sense of smell somewhat, the pain and obstruction to vision gives your target a -10 modifier to all 'to hit' rolls until medical attention is received.",
            "8": "Your shot strikes your target's jaw, shattering teeth and splitting gums. Your target may do nothing except parry for the next round, following which your target attacks at -10 'to hit’ until medical attention is received.",
            "9": "Your shot glances off the side of the skull, destroying one of your target's eyes but doing no other serious damage. Your target may do nothing next round and attacks at -10 'to hit' until medical attention is received. Any sight-related skills are lost (including Night Vision), and BS is reduced by 20%, to a minimum of 5.",
            "10": "Your shot smashes through your target's front teeth and lodges in the neck. Your target falls to the ground unconscious and may do nothing for D4 hours or until medical attention is received, whichever is shorter. A failure on any medical treatment roll to remove your shot results in an additional D4 damage.",
            "11": "Your shot strikes your target's forehead, lodging in the brain. Your target falls unconscious and may do nothing for D10 hours or until medical attention is received. Also, your target must test against Toughness or forfeit 10 points from all percentile characteristics as a result of permanent brain damage. A failure on any medical treatment roll to remove your shot results in automatic death.",
            "12": "Your shot passes through the neck and lodges in the spinal column. Your target falls to the ground and will die in D6 rounds unless medical attention is received. Even then, your target must make a successful Toughness test or be paralysed from the neck down. A failure on any medical treatment roll to remove your shot results in automatic paralysis.",
            "13": "Your shot penetrates the brain. Your target falls to the ground and will die in D4 rounds unless medical attention is obtained. Additionally, your target must test against Toughness or lose 15 points from all percentile characteristics due to permanent brain damage. A failure on any medical treatment roll to remove your shot results in automatic death.",
            "14": "Your shot hits the neck, shattering vertebrae and severing the spinal column. Your target collapses, gurgles and then dies.",
            "15": "Your shot penetrates one of your target's eyes, and on into the brain. Death is instantaneous.",
            "16": "Your shot strikes right between the eyes. Death is instantaneous."
        }, "Body": {
            "1": "Your shot grazes your target's chest. Your target can do nothing except parry next round while regaining balance.",
            "2": "Your shot wings your target in the side, causing pain but no serious damage. Your target may do nothing in the next round.",
            "3": "Your shot grazes the chest, glancing off a rib. Your target falls to the ground and may do nothing except parry for the next D4 rounds until upright again.",
            "4": "Your shot grazes your target's pelvis. Your target falls to the ground, dropping anything hand-held and is not able to do anything except parry with a shield (if he has one) for the next D4 rounds while staggering upright.",
            "5": "Your shot ricochets off your target's ribs just below the shoulder, sending your target sprawling to the ground. Your target is stunned for D4 rounds, and thereafter may do nothing except parry for D4 rounds while clambering upright.",
            "6": "Your shot strikes your target's pelvis and lodges in the abdomen. Your target attacks at -10 to hit until medical attention is received. A failure on any medical treatment roll to remove your shot results in an additional D4 damage.",
            "7": "Your shot fractures your target's collar-bone, lodging just in front of the shoulder. The agony reduces all characteristics by 1 or 10 points as appropriate until medical attention is obtained. A failure on any medical treatment roll to remove your shot results in an additional D4 damage.",
            "8": "Your shot lodges itself between your target's ribs. All characteristics are reduced by 1 or 10 points as appropriate and movement allowance is reduced by half until medical attention is received. A failure on any medical treatment roll to remove your shot results in an additional D4 damage.",
            "9": "Your shot lodges in your target's abdomen. Your target loses consciousness and collapses to the ground, losing 1 Wound per round from internal bleeding until medical attention is received. A failure on any medical treatment roll to remove your shot results in an additional D4 damage.",
            "10": "Your shot punctures a lung. Your target falls unconscious, losing D4 Wounds per round until medical attention is received. Even following this, your target will be totally incapacitated for at least 10 weeks and permanently loses 1 point of Toughness. A failure on any medical treatment roll to remove your shot results in an additional D4 damage.",
            "11": "Your shot rips its way into your target's abdomen, severely damaging several internal organs. Your target collapses in agony, unable to do anything until medical attention is received and losing D4 Wounds per round. A test against Toughness must be made each round in order to remain conscious. A failure on any medical treatment roll to remove your shot results in an additional D4 damage.",
            "12": "Your shot lodges in your target's spine. Your target falls to the ground, may do nothing until medical attention is received and must test against Toughness or be permanently paralyzed from the waist down. A failure on any medical treatment roll to remove the shot results in automatic paralysis from the waist down.",
            "13": "Your shot fractures your target's spine, lodging half-way through. Your target collapses unconscious. D4 Wounds are lost per round due to internal bleeding until internal medical attention is received. All characteristics are halved until a full recovery is made (ten weeks). Until then all skills involving movement are lost. A failure on any medical treatment roll to remove your shot results in automatic paralysis from the waist down.",
            "14": "Your shot passes through your target's abdomen, damaging several vital organs. Your target collapses, coughing up blood. Death is almost instantaneous.",
            "15": "Your shot hits your target's upper spine, severing the spinal column. Death is almost instantaneous.",
            "16": "Your shot passes between your target's ribs and finds the heart. Death is instantaneous."
        }, "Leg": {
            "1": "Your shot grazes the calf, making your target stumble and drop any hand-held object unless a test against Dex is passed.",
            "2": "Your shot grazes your target's hip, and your target may only parry next round.",
            "3": "Your shot grazes the thigh, causing your target to lose balance and fall to the ground. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright.",
            "4": "Your shot nicks your target's leg with some force. Your target's Movement and Init scores are halved for D4 rounds.",
            "5": "Your shot damages your target's ankle, incapacitating it. Movement and Init are halved until medical attention is obtained. Your target must make an Init test or fall down. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright.",
            "6": "Your shot lodges in your target's calf. Movement and Init are halved until medical attention is received. Your target must also test against Init or go down. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright. A failure on any medical treatment roll to remove your shot results in an additional D4 damage.",
            "7": "Your shot lodges in your target's thigh. Movement and Init are halved until medical attention is received. Your target must also test against Init or go down. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright. A failure on any medical treatment roll to remove your shot results in an additional D4 damage.",
            "8": "Your shot passes through your target's foot, destroying a couple of toes. Your target must test against Init or go down, and suffers a -10 modifier to all tests relating to movement and combat until medical attention is received. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright.",
            "9": "Your shot cuts deeply into the leg, tearing through the muscle and grazing the bone before exiting on the opposite side. Your target collapses and loses 1 Wound per round until medical attention is received. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright.",
            "10": "Your shot glances off the thigh-bone, piercing a major blood vessel and damaging muscle and sinew. Your target is knocked down and may not get up until a successful Init test is made. Any hand-held objects are dropped unless a Dex test is passed, and your target may only parry for the next D4 rounds while clambering back upright. Your target loses 1 Wound per round until medical attention is received. A failure on any medical treatment roll to remove your shot results in an additional D4 damage.",
            "11": "Your shot rips through the leg, shattering into shrapnel on contact with the bone and causing serious damage. Your target crashes to the ground and drops anything handheld. D4 Wounds are lost per round until medical attention is received. Your target may only stand or walk if supported by at least one other character. Failure on any medical treatment roll to remove the fragments of shot means an extra D4 damage.",
            "12": "Your shot completely destroys the ankle, and your target collapses, only able to parry and losing D4 Rounds per round until medical attention is received. A failure on any medical treatment roll results in a permanently stiff ankle and a -10 modifier to all tests relating to movement and combat.",
            "13": "Your shot shatters your target's kneecap, wrecking the knee. Your target falls to the ground unconscious from the pain. D4 Wounds are lost per round until medical attention is received. A failure on any medical treatment roll results in a permanently stiff knee and a -20 modifier to all tests relating to movement and combat.",
            "14": "Your shot glances off the pelvis and lodges in your target's groin. Your target falls to the ground unconscious from the pain, and D4 Wounds are lost per round until medical attention is received. A failure on any medical treatment roll to remove your shot results in an additional D6 damage.",
            "15": "Your shot shatters the bone of your target's leg, and the sharp fragments of bone cause massive internal damage. Death from shock and blood loss is almost instantaneous.",
            "16": "Your shot strikes your target's upper leg, grazing the pelvis and carrying on into the abdomen to cause terminal damage to several internal organs. Your target dies instantaneously from shock and blood loss."
        }
    },
    "Falling and Crushing": {
        "Body": {
            "1": "Winded, the victim may do nothing for next round.",
            "2": "The victim is seriously winded, and may make no further actions this round or the next.",
            "3": "The victim is dazed, and may make no actions for the next D4 rounds.",
            "4": "The victim is bruised and stunned, and may make no actions for the next D4 rounds. All rolls relating to physical activity suffer a -10 penalty for the next 2D6 minutes.",
            "5": "The victim is seriously bruised, and is prone for the next round. Thereafter, the victim may only parry for D4 rounds while regaining his feet, and suffers a -10 penalty to all rolls relating to physical activity for the next D3 hours.",
            "6": "A couple of broken ribs and severe bruising restrict the victim to cautious rate or slower, and halve all percentage characteristics until medical attention is received. Even with medical attention, the victim suffers a -10 penalty to all rolls relating to physical activity for the next 2D4 hours.",
            "7": "The victim suffers severe bruising and some broken bones, restricting the victim to cautious rate or slower, and halving all percentage characteristics until medical attention is received. Even with medical attention, the victim suffers a -10 penalty to all rolls relating to physical activity for the next 2D4 hours. In addition, D3 random locations suffer a critical result from the blunt hand weapons table; roll a D10 for each location to determine what this result is.",
            "8": "The victim suffers severe bruising and internal injuries, will be prone for D4 rounds, and makes all rolls at -20 until medical attention is received. internal bleeding costs the victim 1 Wound point per round until medical attention is received.",
            "9": "The victim suffers severe injuries, restricting the victim to cautious rate or slower, and halving all percentage characteristics until medical attention is received. The victim will be prone for D4 rounds, and makes all rolls at -20 until medical attention is received. Internal bleeding costs the victim 1 Wound point per round until medical attention is received. Even with medical attention, the victim suffers a -10 penalty to all rolls relating to physical activity for the next 2D4 hours. In addition, D3 random locations suffer a critical result from the blunt hand weapons table; roll a D10 for each location to determine what this result is.",
            "10": "The victim is horribly mangled, and must make a Toughness test each round or lose consciousness. Blood loss from internal and external injuries costs the victim D4 Wound points per round until medical attention is received. The victim can only crawl, at one-quarter of cautious movement rate. Additionally, D6 random locations suffer effects equivalent to a blunt weapon critical of level 2D4. The victim's Toughness is permanently reduced by 1 point.",
            "11": "The victim is concussed, and is unconscious for D10 hours or until medical attention is received. On regaining consciousness, the victim must make a successful Toughness test or lose 10 points from each percentage characteristic as a result of lasting brain damage. Blood loss from internal and external injuries costs the victim D4 Wound points per round until medical attention is received. The victim can only crawl, at one-quarter of cautious movement rate. Additionally, D6 random locations suffer effects equivalent to a blunt weapon critical of level 2D4. The victim's Toughness is permanently reduced by 1 point.",
            "12": "More or less every bone in the victim's body is broken. Blood loss from internal and external injuries costs the victim D6 Wound points per round until medical attention is received. The victim can only crawl, at one-quarter of cautious movement rate. Additionally, D6+4 random locations suffer effects equivalent to a blunt weapon critical of level 2D4. The victim's Toughness is permanently reduced by 2 points.",
            "13": "The victim is horribly mangled and suffers permanent injury and disfigurement. Blood loss from internal and external injuries costs the victim D6 Wound points per round until medical attention is received. The victim can only crawl, at one-quarter of cautious movement rate. Additionally, D6+4 random locations suffer effects equivalent to a blunt weapon critical of level 2D4. The victim's Toughness is permanently reduced by 2 points. All characteristic scores are permanently halved upon recovery, and the victim receives D6+6 Insanity points from the ordeal, halved if a successful Cool test is made.",
            "14": "Several vital organs are ruptured, causing death in a matter of seconds.",
            "15": "The victim's chest is crushed like an eggshell. Death is instant.",
            "16": "The victim is reduced to a bloody smear. They didn't feel a thing."
        }
    },
    "Fire and Energy": {
        "Body": {
            "1": "Breathless, the victim may only parry next round.",
            "2": "Acrid, thick smoke rising from clothing and the surroundings chokes the victim, who may make no further actions this round or the next.",
            "3": "The victim is stunned. and may make no actions for the remainder of this and the next round. If the critical was caused by an explosion or lightning, the victim is also deafened for D6 rounds.",
            "4": "The victim's clothing, hair or fur is scorched, and the victim is only able to parry for D4 rounds. If the critical was caused by an explosion or lightning. the victim is also deafened for D10 rounds.",
            "5": "The victim is thrown back 2D6 yards, and is prone for the next round. Thereafter. the victim may only parry for D4 rounds while regaining his feet. If the critical was caused by an explosion or lightning, the victim is also deafened for D10 rounds.",
            "6": "The victim's clothing or fur bursts into flames, causing D4 Wounds per round until extinguished. No actions (apart from rolling on the ground or jumping into nearby water) are possible until the flames are extinguished, and the victim counts as prone during this time, however, attackers who get too close may be at risk from the flames themselves. Naked or hairless opponents are incapacitated for one round only. If the critical was caused by art explosion or lightning, the victim is also deafened for D4 hours.",
            "7": "The victim suffers first-degree burns to any exposed flesh. Until medical attention is received. the victim makes all rolls at -10 owing to the pain. If the critical was caused by an explosion or lightning, the victim is also deafened for D6 hours, and must make a Toughness test or suffer a -20 modifier to all hearing-related tests permanently.",
            "8": "The victim suffers second-degree burns to exposed areas, and first-degree burns to most other areas. Clothing is ruined, and armour is reduced by 1 point per location. Non-magical equipment is damaged according to its material, as the GM sees fit. The victim is prone for D4 rounds, and makes all rolls at -20 until medical attention is received. A failure on any medical treatment roll results in permanent loss of D6 Fel points because of scarring. If the critical was caused by an explosion or lightning, the victim is also deafened for 2D6 hours and must make a Toughness test or suffer a -20 modifier to all hearing-related tests permanently.",
            "9": "The victim is horribly burned, and must make a Toughness test each round to remain conscious. No actions arc possible, and the victim loses 1 Wound per round until medical attention is received, as shock begins to set in. The burns will take D4+4 weeks to heal, and the victim loses 2D10 Fel points to scarring. This is doubled if any medical treatment roll is failed. If the critical was caused by an explosion or lightning, the victim suffers a -20 modifier to all hearing-related tests permanently. The victim trust make a Cool test or receive an Insanity point.",
            "10": "The victim suffers serious burns, losing D4 Wounds per round until medical attention is received. Clothing is ruined, and armour is reduced by 1 point per location. Non-magical equipment is damaged according to its material, as the GM sees fit. The victim is prone for D4 rounds, and makes all rolls at -20 until medical attention is received. A failure on any medical treatment roll results in permanent loss of D6 Fel points because of scarring. If the critical was caused by an explosion or lightning, the victim is also deafened for 2D6 hours and must make a Toughness test or suffer a -20 modifier to all hearing-related tests permanently. This victim must make a Cool test or receive D5 insanity points.",
            "11": "The victim is in shock from the concussion and burns, losing D4 Wounds per round until medical attention is received. Clothing is ruined, and armour is reduced by 1 point per location. Non-magical equipment is damaged according to its material, as the GM sees fit. The victim is prone for D4 rounds, and makes all rolls at -20 until medical attention is received. A failure on any medical treatment roll results in permanent loss of D6 Fel points because of scarring. If the critical was caused by an explosion or lightning, the victim is also deafened for 2D6 hours and must make a Toughness test or suffer a -20 modifier to all hearing-related tests permanently. In addition, they must pass a Toughness test every round until medical attention is received, or die. The victim must make a Cool test or receive D6 Insanity points.",
            "12": "The victim is horribly burned, and must make a Toughness test each round to remain conscious. No actions arc possible, and the victim loses D4 Wounds per round until medical attention is received, as shock begins to set in. The burns will take D4+4 weeks to heal, and the victim loses 2D10 Fel points to scarring. This is doubled if any medical treatment roll is failed. If the critical was caused by an explosion or lightning, the victim suffers a -20 modifier to all hearing-related tests permanently. In addition, the victim must pass a Toughness test every round until medical attention is received, or die from shock. If he survives, the victim must make a Cool test or receive 2D4 Insanity points.",
            "13": "The victim is horribly burned, suffers permanent injury and disfigurement, and must make a Toughness test each round to remain conscious. No actions arc possible, and the victim loses D4 Wounds per round until medical attention is received, as shock begins to set in. The burns will take D4+4 weeks to heal, and the victim loses 2D10 Fel points to scarring. This is doubled if any medical treatment roll is failed. If the critical was caused by an explosion or lightning, the victim suffers a -20 modifier to all hearing-related tests permanently. In addition, the victim must pass a Toughness test every round until medical attention is received, or die from shock. All characteristic scores are permanently halved upon recovery, and the victim receives D6+6 Insanity points from the ordeal, halved if a successful Cool test is made.",
            "14": "The victim erupts in a ball of flame, staggering D3 yards in a random direction before falling to the ground dead.",
            "15": "The victim explodes, causing a single S3 hit to everything in a 3-yard radius. Death is instantaneous.",
            "16": "There is a blinding flash, and nothing remains of the victim at all. Except, perhaps, for a pair of smoking boots..."
        }
    }
}

let d = new Dialog({
    title: `WFRP 1e Critical Hit Rolls`,
    content: `
        <form>
            <div class="form-group">
                <label>Critical Type</label>
                <select id="crittype" name="crittype">
                    <option value="blunt">Blunt Hand Weapons</option>
                    <option value="sharp">Sharp Hand Weapons</option>
                    <option value="arrows">Arrows and Bolts</option>
                    <option value="firearms">Firearms</option>
                    <option value="teeth">Teeth and Claws</option>
                    <option value="falling">Falling and Crushing</option>
                    <option value="fire">Fire and Energy</option>
                </select>
            </div>
            <div class="form-group">
                <label>Critical Level</label>
                <select id="critlevel" name="critlevel">
                    <option value="0">D10</option>
                    <option value="1">+1</option>
                    <option value="2">+2</option>
                    <option value="3">+3</option>
                    <option value="4">+4</option>
                    <option value="5">+5</option>
                    <option value="6">+6</option>
                </select>
            </div>
            <div class="form-group">
                <label>Hit Location</label>
                <select id="hitlocation" name="hitlocation">
                    <option value="none">Full Body</option>
                    <option value="head">01-15 Head</option>
                    <option value="rightarm">16-35 Right Arm</option>
                    <option value="leftarm">36-55 Left Arm</option>
                    <option value="body">56-80 Body</option>
                    <option value="rightleg">81-90 Right Leg</option>
                    <option value="leftleg">91-00 Left Leg</option>
                </select>
            </div>
        </form>
    `,
    buttons: {
        yes: {
            icon: "<i class='fas fa-check'></i>",
            label: `Roll Crit`,
            callback: () => {
                applyChanges = true;
                d.render(true)
            }
        },
        no: {
            icon: "<i class='fas fa-times'></i>",
            label: `Exit`
        },
    },
    default: "yes",
    close: async html => {
        if (applyChanges) {
            applyChanges = false;
            let crittype = html.find('[name="crittype"]')[0].value || "";
            let crittypename = "";
            let hitlocation = html.find('[name="hitlocation"]')[0].value || "";
            switch (crittype) {
                case "sharp": crittypename = "Sharp Hand Weapons"; break;
                case "blunt": crittypename = "Blunt Hand Weapons"; break;
                case "arrows": crittypename = "Arrows and Bolts"; break;
                case "firearms": crittypename = "Firearms"; break;
                case "teeth": crittypename = "Teeth and Claws"; break;
                case "falling": crittypename = "Falling and Crushing"; hitlocation = "none"; break;
                case "fire": crittypename = "Fire and Energy"; hitlocation = "none"; break;
            }
            let critlevel = html.find('[name="critlevel"]')[0].value || "";
            let critlevelname = `+${critlevel}`;
            if (critlevel == "0") {
                critlevelname = "N/A";
                const locationroll = await new Roll("1d100").evaluate({ async: true });
                if (locationroll.total > 90) {
                    hitlocation = "leftleg";
                } else if (locationroll.total > 80) {
                    hitlocation = "rightleg";
                } else if (locationroll.total > 55) {
                    hitlocation = "body";
                } else if (locationroll.total > 35) {
                    hitlocation = "leftarm";
                } else if (locationroll.total > 15) {
                    hitlocation = "rightarm";
                } else {
                    hitlocation = "head";
                }
            }
            let hitlocationname = "";
            switch (hitlocation) {
                case "none": hitlocationname = "Full Body"; break;
                case "head": hitlocationname = "Head"; break;
                case "body": hitlocationname = "Body"; break;
                case "rightarm": hitlocationname = "Right Arm"; break;
                case "leftarm": hitlocationname = "Left Arm"; break;
                case "rightleg": hitlocationname = "Right Leg"; break;
                case "leftleg": hitlocationname = "Left Leg"; break;
            }
            if (crittype == "") return;
            const roll = await new Roll("1d10").evaluate({ async: true });
            const critchart = [
                ["", "+1", "+2", "+3", "+4", "+5", "+6"],
                ["1:-", "1:-", "3:-", "5:-", "7:-", "11:F", "14:F"],
                ["2:-", "2:-", "4:-", "6:-", "9:F", "13:F", "15:D"],
                ["3:-", "3:-", "5:-", "8:F", "14:F", "16:D", "16:D"],
                ["4:-", "4:-", "7:-", "10:F", "13:F", "15:D", "15:D"],
                ["5:-", "5:-", "9:F", "14:F", "16:D", "16:D", "16:D"],
                ["6:-", "7:-", "12:F", "15:D", "15:D", "15:D", "15:D"],
                ["7:-", "9:F", "16:D", "16:D", "16:D", "16:D", "16:D"],
                ["8:-", "11:F", "15:D", "15:D", "15:D", "15:D", "15:D"],
                ["9:-", "16:D", "16:D", "16:D", "16:D", "16:D", "16:D"],
                ["10:-", "15:D", "15:D", "15:D", "15:D", "15:D", "15:D"]
            ];
            const result = critchart[roll.total][Number(critlevel)];
            const resultindex = result.split(":")[0];
            const hitlocationindex = hitlocationname.split(" ")[1] || hitlocationname.split(" ")[0];
            const resulttext = critdata[crittypename][hitlocationindex][resultindex];
            console.log([crittype, critlevel, hitlocation, roll.total, result]);
            ChatMessage.create({
                speaker: { actor: game.settings.get("grpga", "currentActor") },
                content: `
                <p>You have achieved a result of <b class="critfail">${result}</b></br>
                on the <b>${critlevelname}</b> column</br>
                of the <b>${crittypename}</b> table</br>
                to your opponent's <b>${hitlocationname}</b>.</p>
                <p class="dynamicitem" data-type="dynamic">${resulttext}</p>`,
                flags: {"system.itemdata": {
                    name: `${hitlocationname} [${result}]`, 
                    type: "Rollable", 
                    data: {
                        category: "technique",
                        charType: "CharacterD120",
                        notes: `<h2>${crittypename}</h2><p>${resulttext}</p>`
                    }
                }}
            });
        }
    }
}).render(true);
