if (canvas.tokens.controlled.length === 0)
    return ui.notifications.error("Please select a token first");

let applyChanges = false;
new Dialog({
    title: `Token Vision Configuration`,
    content: `
<form>
    <div class="form-group">
        <label>Vision Conditions:</label>
        <select id="light-conditions" name="light-conditions">
            <option value="nochange">No Change</option>
            <option value="photo">Dark: Photo Goggles</option>
            <option value="infra">Dark: Infra Sight</option>
            <option value="dark">Dark: No Gear</option>
            <option value="light">Lights On</option>
        </select>
    </div>
    <div class="form-group">
        <label>Highlight Colour:</label>
        <select id="light-source" name="light-source">
            <option value="nochange">No Change</option>
            <option value="none">None</option>
            <option value="red">Red</option>
            <option value="green">Green</option>
            <option value="blue">Blue</option>
        </select>
    </div>
    <div class="form-group">
        <label>Radius Marker:</label>
        <select id="radius-marker" name="radius-marker">
            <option value="nochange">No Change</option>
            <option value="none">None</option>
            <option value="three">3"</option>
            <option value="six">6"</option>
            <option value="twelve">12"</option>
        </select>
    </div>
</form>
    `,
    buttons: {
        yes: {
            icon: "<i class='fas fa-check'></i>",
            label: `Apply Changes`,
            callback: () => applyChanges = true
        },
        no: {
            icon: "<i class='fas fa-times'></i>",
            label: `Cancel Changes`
        },
    },
    default: "yes",
    close: html => {
        if (applyChanges) {
            for (let token of canvas.tokens.controlled) {
                let visionType = token.actor?.system.dynamic.visionType || "normal";
                let lightConditions = html.find('[name="light-conditions"]')[0].value || "none";
                let lightSource = html.find('[name="light-source"]')[0].value || "none";
                let radius = html.find('[name="radius-marker"]')[0].value || "none";
                let dimSight = 0;
                let brightSight = 0;
                let dimLight = 0;
                let brightLight = 0;
                let colour = "#000000";
                let lightAngle = 360;
                let lockRotation = token.data.lockRotation;
                // Get Vision Type Values
                switch (lightConditions) {
                    case "photo":
                    case "infra": {
                        dimSight = 0;
                        brightSight = 12;
                        break;
                    }
                    case "dark": {
                        dimSight = 0;
                        brightSight = 3;
                        break;
                    }
                    case "light": {
                        dimSight = 0;
                        brightSight = 48;
                        break;
                    }
                    case "nochange":
                    default: {
                        dimSight = token.data.dimSight;
                        brightSight = token.data.brightSight;
                    }
                }
                // Get Light Source Values
                switch (lightSource) {
                    case "none":
                        colour = "";
                        break;
                    case "red":
                        colour = "#ff0000";
                        break;
                    case "green":
                        colour = "#00ff00";
                        break;
                    case "blue":
                        colour = "#0000ff";
                        break;
                    case "nochange":
                    default:
                        colour = token.data.tint;
                }
                // Get Light Source Values
                switch (radius) {
                    case "none": {
                        brightLight = 0;
                        break;
                    }
                    case "three": {
                        brightLight = 3;
                        break;
                    }
                    case "six": {
                        brightLight = 6;
                        break;
                    }
                    case "twelve": {
                        brightLight = 12;
                        break;
                    }
                    case "nochange":
                    default:
                        brightLight = token.data.light.bright;
                }
                let updateData = {
                    vision: true,
                    visionType: visionType,
                    dimSight: dimSight,
                    brightSight: brightSight,
                    tint: colour,
                    light: {
                        bright: brightLight,
                        color: colour,
                        luminosity: 1.0,
                        saturation: 1,
                        gradual: false
                    },
                    lockRotation: true
                };
                // Update Token
                console.debug(`Updating Light settings for ${token.data.name} with ${visionType} vision:\n`, updateData);
                token.document.update(updateData);
            }
        }
    }
}).render(true);