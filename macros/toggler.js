main()
async function main() {
    if (canvas.tokens.controlled.length == 0) {
        ui.notifications.error("Please select at least one token first");
        return;
    }

    // filter for modifiers with condition checked
    const items = canvas.tokens.controlled[0].actor.system.items.filter(function (item) {
        return item.system.data.condition;
    });

    const buttondata = {
        pinned: {
            label: "Pinned",
            icon: "systems/grpga/icons/postures/crawling.png"
        },
        blinded: {
            label: "Blinded",
            icon: "systems/grpga/icons/conditions/blinded.svg"
        }
    }
    const buttons = {};
    for (const button of Object.entries(items)) {
        buttons[button[0]] = {
            label: button[1].name,
            callback: () => {
                label = button[1].name,
                    icon = button[1].img,
                    d.render(true)
            }
        }
    }

    let d = new Dialog({
        title: `Set Status Effect`,
        buttons: buttons,
        close: async html => {
            if (icon) {
                const tokens = canvas.tokens.controlled;
                for (const token of tokens) {
                    await token.toggleEffect({
                        icon: icon,
                        id: label,
                        label: label
                    });
                    const actor = token.document.actor;
                    const inEffect = actor.system.effects.find(i => i.data.label == label) != undefined;
                    const modname = actor.slugify(label);
                    const mod = actor.system.modifiers[modname];
                    if (mod) {
                        await actor.updateEmbeddedDocuments("Item", [{ _id: mod._id, "data.inEffect": inEffect }]);
                    }
                }
                label = "", icon = "";
            }
        }
    },
        {
            width: 200,
            classes: ["mydialog"],
            top: 0,
            left: 0
        });
    d.render(true);
}