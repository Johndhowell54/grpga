main()
async function main() {
    const expression = "10d10x";
    const roll = await new Roll(expression).evaluate({ async: true });
    const rolls = roll.terms[0].values.sort(function (a, b) { return b - a });
    const rollsearch = [...rolls];
    console.debug(`Result\n`, rollsearch);
    const raises = [];
    let remainder;
    do {
        const raise = [];
        // add the largest roll to the raise
        raise.push(rolls.shift());
        // if there are no more rolls, stop
        if (rolls.length == 0) {
            remainder = raise;
            break;
        }
        // is this a valid raise?
        while (raise.reduce(getSum) < 10) {
            // not yet, add another
            raise.push(rolls.pop());
            // sort smallest to the front
            raise.sort(function (a, b) { return a - b });
            // if there are no more rolls, stop
            if (rolls.length == 0) {
                // if the current raise is valid, the remainder is null
                if (raise.reduce(getSum) < 10) {
                    // if the remainder is two elements, try to swap it with a single one
                    if (raise.length > 1) {
                        // the sum of the remainder
                        const sum = raise.reduce(getSum);
                        console.debug(`Multi-remainder\n`, raise);
                        for (let r of raises) {
                            // the index of a value greater than or equal to the sum of the remainder
                            for (let index = 0; index < r.length; index++) {
                                if (r[index] >= sum && r[index] - sum <= r.reduce(getSum) - 10) {
                                    // move each remainder to the end of the raise
                                    for (let swap of raise) {
                                        r.push(swap);
                                    }
                                    // move the matched element to the raise
                                    raise.push(r.shift());
                                }
                            }
                        }
                    }
                    // if no reduction was possible
                    remainder = raise;
                } else {
                    remainder = null;
                }
                break;
            }
        }
        // is this raise too big?
        while (raise.reduce(getSum) - 10 >= raise[0]) {
            // put the smallest element back on the rolls
            rolls.push(raise.shift());
        }

        // append a valid raise to raises, sorted largest to smallest
        if (raise.reduce(getSum) >= 10)
            raises.push(raise.sort(function (a, b) { return b - a }));
    }
    while (rolls.length > 0)
    console.debug(`Raises and remainder\n`, [raises, remainder]);
}

function getSum(total, num) {
    return total + num;
}