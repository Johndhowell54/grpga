if (canvas.tokens.controlled.length === 0)
    return ui.notifications.error("Please select a token first");

let applyChanges = false;
let d = new Dialog({
    title: `Declare Action`,
    content: `
<form>
    <div class="form-group">
        <label>Declared Action:</label>
        <select id="action" name="action">
            <option value="wait">Wait - Ready</option>
            <option value="move">Move</option>
            <option value="spella1">Cast Prepared Spell</option>
            <option value="spella2">Cast Instant Spell</option>
            <option value="rangeda1">Aimed Ranged Attack</option>
            <option value="rangeda2">Throw Ready Weapon</option>
            <option value="longestplus">Longest Melee plus</option>
            <option value="longest">Longest Melee</option>
            <option value="long">Long Melee</option>
            <option value="short">Short Melee</option>
            <option value="hand">Hand Melee</option>
            <option value="handminus">Hand Melee minus</option>
            <option value="rangedb">Unaimed Ranged Attack</option>
            <option value="spellb">Cast Unprepared Spell</option>
            <option value="other">Other Action</option>
        </select>
    </div>
</form>
    `,
    buttons: {
        yes: {
            icon: "<i class='fas fa-check'></i>",
            label: `Apply`,
            callback: () => {
                applyChanges = true;
                d.render(true)
            }
        },
        no: {
            icon: "<i class='fas fa-times'></i>",
            label: `Cancel`,
            callback: () => applyChanges = true
        },
    },
    default: "yes",
    close: html => {
        if (applyChanges) {
            for (let token of canvas.tokens.controlled) {
                let declaration = token.actor.system.dynamic["declared-action"]
                let label = "", formula = "", value = 0;
                if(declaration) {
                    let action = html.find('[name="action"]')[0].value || "none";
                    switch (action) {
                        case "wait": {
                            label = "Wait - Ready"
                            formula = "71"
                            value = 71
                            break;
                        }
                        case "move": {
                            label = "Move"
                            formula = "61"
                            value = 61
                            break;
                        }
                        case "spella1": {
                            label = "Cast Prepared Spell"
                            formula = "51"
                            value = 51
                            break;
                        }
                        case "spella2": {
                            label = "Cast Instant Spell"
                            formula = "51"
                            value = 51
                            break;
                        }
                        case "rangeda1": {
                            label = "Aimed Ranged Attack"
                            formula = "41"
                            value = 41
                            break;
                        }
                        case "rangeda2": {
                            label = "Throw Ready Weapon"
                            formula = "41"
                            value = 41
                            break;
                        }
                        case "longestplus": {
                            label = "Longest Melee plus"
                            formula = "36"
                            value = 36
                            break;
                        }
                        case "longest": {
                            label = "Longest Melee"
                            formula = "35"
                            value = 35
                            break;
                        }
                        case "long": {
                            label = "Long Melee"
                            formula = "34"
                            value = 34
                            break;
                        }
                        case "short": {
                            label = "Short Melee"
                            formula = "33"
                            value = 33
                            break;
                        }
                        case "hand": {
                            label = "Hand Melee"
                            formula = "32"
                            value = 32
                            break;
                        }
                        case "handminus": {
                            label = "Hand Melee minus"
                            formula = "31"
                            value = 31
                            break;
                        }
                        case "rangedb": {
                            label = "Unaimed Ranged Attack"
                            formula = "21"
                            value = 21
                            break;
                        }
                        case "spellb": {
                            label = "Cast Unprepared Spell"
                            formula = "11"
                            value = 11
                            break;
                        }
                        case "other": {
                            label = "Other Action"
                            formula = "1"
                            value = 1
                            break;
                        }
                        default: {
                        }
                    }
                }
                let item = token.actor.items.get(declaration.id)
                let updates= item.system.data;
                updates.label = label;
                updates.formula = formula;
                updates.value = value;
                item.update({['data']: updates})
                console.debug(`Declared Action [${label}] for Token [${token.name}]`);
            }
        }
    }
});
d.render(true);