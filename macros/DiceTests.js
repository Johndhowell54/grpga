function sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }

const diff = 1;
    for (let dice = 1; dice < 11; dice++) {
        let successes = 0;
        for (let x = 0; x < 10000; x++) {
            let icons = 0;
            for (let i = 0; i < dice; i++) {
                const roll = new Roll("1d6");
                const result = await roll.evaluate({ async: true });
                const total = result.total;
                switch (total) {
                    case 4:
                    case 5: icons++; break;
                    case 6: icons += 2; break;
                }
            }
            if (icons >= diff) successes++;
        }
        console.debug(`DN-${diff} with ${dice}d6 produced ${successes/100}% success.`);
        sleep(1000);
    }
