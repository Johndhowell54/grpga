import { system } from "../config.js";

export class ActionPointCombat extends Combat {

    async startCombat() {
        await this.setupTurns();
        await this.fetchActorData();
        return super.startCombat();
    }

    /**
     * @override
     */
    async rollAll(options) {
        await super.rollAll(options);
        // make sure the top combatant is selected
        return this.update({ turn: 0 });
    }

    async nextTurn() {
        let turn = this.turn;

        // Determine the next turn number
        let next = turn;
        let found = false;
        // find a non-defeated turn after this one with at least one action remaining
        while (++next < this.turns.length) {
            let t = this.turns[next];
            if (t.defeated) continue;
            if (t.flags.grpga.actions < 1) continue;
            if (t.actor?.statuses.has('defeated')) continue;
            found = true;
            break;
        }
        if (!found) {// hit the end of the turn order so start from the front
            next = -1;
            while (++next <= turn) {
                let t = this.turns[next];
                if (t.defeated) continue;
                if (t.flags.grpga.actions < 1) continue;
                if (t.actor?.statuses.has('defeated')) continue;
                found = true;
                break;
            }
        }

        // the round will not advance if no-one has actions remaining
        if (!found) return this;

        // before advancing to the next combatant, decrement the current one's actions
        await this.combatant.update({ ["flags.grpga.actions"]: this.turns[turn].flags.grpga.actions - 1 });

        // Update the encounter
        return this.update({ round: this.round, turn: next });
    }

    async nextRound() {
        await this.fetchActorData();
        return super.nextRound();
    }

    async fetchActorData() {
        await this.resetAll();
        await this.rollAll();
        // this needs to go after the reset and rollAll or the data gets wiped
        this.combatants.forEach(async c => {
            await c.update({
                ['flags.grpga.ready']: false,
                ['flags.grpga.actions']: c.actor.system.dynamic.apr?.system.moddedvalue || 1
            });
            c.actor.resetModVars(true);
        });
    }
}

export class ActionPointCombatTracker extends CombatTracker {
    get template() {
        return "systems/grpga/templates/combat/apcombat-tracker.hbs";
    }

    _onConfigureCombatant(li) {
        const combatant = this.viewed.combatants.get(li.data('combatant-id'));
        new ActionPointCombatantConfig(combatant, {
            top: Math.min(li[0].offsetTop, window.innerHeight - 350),
            left: window.innerWidth - 720,
            width: 400
        }).render(true);
    }

    async getData(options) {
        const data = await super.getData(options);

        if (!data.hasCombat) {
            return data;
        }
        data.settings.useD6Pool = CONFIG.system.hijack == "UseD6Pool";

        for (let turn of data.turns) {
            let combatant = this.viewed.combatants.get(turn.id);
            turn.actions = combatant.getFlag("grpga", "actions");
            turn.ready = combatant.getFlag("grpga", "ready");
        }
        return data;
    }

    activateListeners(html) {
        super.activateListeners(html);
        html.find(".actions").change(this._onActionsChanged.bind(this));
        html.find(".interrupt").click(this._onInterrupt.bind(this));
        html.find(".initiative").change(this._onInitiativeChanged.bind(this));
        html.find(".name").click(this._onToggleReady.bind(this));
    }

    async _onToggleReady(event) {
        const btn = event.currentTarget;
        const li = btn.closest(".combatant");
        const c = this.viewed.combatants.get(li.dataset.combatantId);
        if (!c.isOwner) return;
        await c.setFlag("grpga", "ready", !c.getFlag("grpga", "ready"));
    }

    async _onActionsChanged(event) {
        const btn = event.currentTarget;
        const li = btn.closest(".combatant");
        const c = this.viewed.combatants.get(li.dataset.combatantId);
        if (!c.isOwner) return;
        await c.update({ ["flags.grpga.actions"]: btn.value });
    }

    async _onInitiativeChanged(event) {
        const btn = event.currentTarget;
        const li = btn.closest(".combatant");
        const c = this.viewed.combatants.get(li.dataset.combatantId);
        if (!c.isOwner) return;
        await c.update({ initiative: btn.value });
    }

    async _onInterrupt(event) {
        const btn = event.currentTarget;
        const li = btn.closest(".combatant");
        const c = this.viewed.combatants.get(li.dataset.combatantId);
        if (!c.isOwner) return;
        await c.update({ ["flags.grpga.actions"]: c.getFlag("grpga", "actions") - 1 });
    }
}

export class ActionPointCombatantConfig extends CombatantConfig {
    get template() {
        return "systems/grpga/templates/combat/apcombatant-config.hbs";
    }
}

export class ActionPointCombatant extends Combatant {
    _onCreate(data, options, userId) {
        super._onCreate(data, options, userId);
        if (this.isOwner)
            this.update({
                initiative: null,
                ['flags.grpga.ready']: false,
                ['flags.grpga.actions']: this.actor.system.dynamic.apr?.system.moddedvalue || 1
            });
    }
}
