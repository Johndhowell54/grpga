export const system = {}

system.dataRgx = /[^-(){}\d<>/=*+., DdflorceiMmaxthbsunvpwkxX]/g;

system.hasStatBonusTable = () => {
  return game.tables.getName(CONFIG.system.statBonusTableName) != undefined;
}

system.sbs = (stat) => {
  if (game.tables.getName(CONFIG.system.statBonusTableName)) {
    let result = game.tables.getName(CONFIG.system.statBonusTableName).getResultsForRoll(stat);
    return (result.length == 0) ? -100 : Number(result[0].text);
  } else {
    return stat;
  }
}

CONFIG.ChatMessage.template = "systems/grpga/templates/chat/chat-message.hbs";

CONFIG.postures = [
  "systems/grpga/icons/postures/standing.png",
  "systems/grpga/icons/postures/sitting.png",
  "systems/grpga/icons/postures/crouching.png",
  "systems/grpga/icons/postures/crawling.png",
  "systems/grpga/icons/postures/kneeling.png",
  "systems/grpga/icons/postures/lyingback.png",
  "systems/grpga/icons/postures/lyingprone.png",
  "systems/grpga/icons/postures/sittingchair.png"
];

CONFIG.sizemods = [
  "systems/grpga/icons/sizemods/smneg1.png",
  "systems/grpga/icons/sizemods/smneg2.png",
  "systems/grpga/icons/sizemods/smneg3.png",
  "systems/grpga/icons/sizemods/smneg4.png",
  "systems/grpga/icons/sizemods/smpos1.png",
  "systems/grpga/icons/sizemods/smpos2.png",
  "systems/grpga/icons/sizemods/smpos3.png",
  "systems/grpga/icons/sizemods/smpos4.png"
];

CONFIG.crippled = [
  "systems/grpga/icons/crippled/crippledleftarm.png",
  "systems/grpga/icons/crippled/crippledlefthand.png",
  "systems/grpga/icons/crippled/crippledleftleg.png",
  "systems/grpga/icons/crippled/crippledleftfoot.png",
  "systems/grpga/icons/crippled/crippledrightarm.png",
  "systems/grpga/icons/crippled/crippledrighthand.png",
  "systems/grpga/icons/crippled/crippledrightleg.png",
  "systems/grpga/icons/crippled/crippledrightfoot.png",
];

CONFIG.statusEffects3d6 = [
  { icon: 'systems/grpga/icons/postures/standing.png', id: 'standing', label: 'local.postures.standing' },
  { icon: 'systems/grpga/icons/postures/sitting.png', id: 'sitting', label: 'local.postures.sitting' },
  { icon: 'systems/grpga/icons/postures/crouching.png', id: 'crouching', label: 'local.postures.crouching' },
  { icon: 'systems/grpga/icons/postures/crawling.png', id: 'crawling', label: 'local.postures.crawling' },
  { icon: 'systems/grpga/icons/postures/kneeling.png', id: 'kneeling', label: 'local.postures.kneeling' },
  { icon: 'systems/grpga/icons/postures/lyingback.png', id: 'lyingback', label: 'local.postures.proneb' },
  { icon: 'systems/grpga/icons/postures/lyingprone.png', id: 'lyingprone', label: 'local.postures.pronef' },
  { icon: 'systems/grpga/icons/postures/sittingchair.png', id: 'sittingchair', label: 'local.postures.sitting' },
  { icon: 'systems/grpga/icons/conditions/shock1.png', id: 'shock1', label: 'local.conditions.shock' },
  { icon: 'systems/grpga/icons/conditions/shock2.png', id: 'shock2', label: 'local.conditions.shock' },
  { icon: 'systems/grpga/icons/conditions/shock3.png', id: 'shock3', label: 'local.conditions.shock' },
  { icon: 'systems/grpga/icons/conditions/shock4.png', id: 'shock4', label: 'local.conditions.shock' },
  { icon: 'systems/grpga/icons/conditions/reeling.png', id: 'reeling', label: 'local.conditions.reeling' },
  { icon: 'systems/grpga/icons/conditions/tired.png', id: 'tired', label: 'local.conditions.tired' },
  { icon: 'systems/grpga/icons/conditions/collapse.png', id: 'collapse', label: 'local.conditions.collapse' },
  { icon: 'systems/grpga/icons/conditions/unconscious.png', id: 'unconscious', label: 'local.conditions.unconscious' },
  { icon: 'systems/grpga/icons/conditions/minus1xhp.png', id: 'minushp1', label: 'local.conditions.minushp' },
  { icon: 'systems/grpga/icons/conditions/minus2xhp.png', id: 'minushp2', label: 'local.conditions.minushp' },
  { icon: 'systems/grpga/icons/conditions/minus3xhp.png', id: 'minushp3', label: 'local.conditions.minushp' },
  { icon: 'systems/grpga/icons/conditions/minus4xhp.png', id: 'minushp4', label: 'local.conditions.minushp' },
  { icon: 'systems/grpga/icons/conditions/stunned.png', id: 'stunned', label: 'local.conditions.stunned' },
  { icon: 'systems/grpga/icons/conditions/surprised.png', id: 'surprised', label: 'local.conditions.surprised' },
  { icon: 'systems/grpga/icons/defeated.png', id: 'defeated', label: 'local.conditions.defeated' },
  { icon: 'systems/grpga/icons/blank.png', id: 'none', label: 'local.conditions.none' },
  { icon: 'systems/grpga/icons/stances/hth.svg', id: 'hth', label: 'local.stances.hth' },
  { icon: 'systems/grpga/icons/stances/magic.svg', id: 'magic', label: 'local.stances.magic' },
  { icon: 'systems/grpga/icons/stances/ranged.svg', id: 'ranged', label: 'local.stances.ranged' },
  { icon: 'systems/grpga/icons/stances/thrown.svg', id: 'thrown', label: 'local.stances.thrown' },
  { icon: 'systems/grpga/icons/crippled/crippledleftarm.png', id: 'crippledleftarm', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/crippled/crippledlefthand.png', id: 'crippledlefthand', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/crippled/crippledleftleg.png', id: 'crippledleftleg', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/crippled/crippledleftfoot.png', id: 'crippledleftfoot', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/crippled/crippledrightarm.png', id: 'crippledrightarm', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/crippled/crippledrighthand.png', id: 'crippledrighthand', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/crippled/crippledrightleg.png', id: 'crippledrightleg', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/crippled/crippledrightfoot.png', id: 'crippledrightfoot', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/sizemods/smneg1.png', id: 'smaller1', label: 'local.conditions.smaller' },
  { icon: 'systems/grpga/icons/sizemods/smneg2.png', id: 'smaller2', label: 'local.conditions.smaller' },
  { icon: 'systems/grpga/icons/sizemods/smneg3.png', id: 'smaller3', label: 'local.conditions.smaller' },
  { icon: 'systems/grpga/icons/sizemods/smneg4.png', id: 'smaller4', label: 'local.conditions.smaller' },
  { icon: 'systems/grpga/icons/sizemods/smpos1.png', id: 'larger1', label: 'local.conditions.larger' },
  { icon: 'systems/grpga/icons/sizemods/smpos2.png', id: 'larger2', label: 'local.conditions.larger' },
  { icon: 'systems/grpga/icons/sizemods/smpos3.png', id: 'larger3', label: 'local.conditions.larger' },
  { icon: 'systems/grpga/icons/sizemods/smpos4.png', id: 'larger4', label: 'local.conditions.larger' }
];
CONFIG.statusEffectsmnm = CONFIG.statusEffects3d6;
CONFIG.statusEffectsd120 = CONFIG.statusEffects3d6;
CONFIG.statusEffectsbbf = CONFIG.statusEffects3d6;
CONFIG.statusEffectsd6 = CONFIG.statusEffects3d6;
CONFIG.statusEffectsd100 = CONFIG.statusEffects3d6;
CONFIG.statusEffectsvsd = [
  { icon: 'systems/grpga/icons/conditions/bruised.svg', id: 'bruised', label: 'local.conditions.bruised' },
  { icon: 'systems/grpga/icons/conditions/weary.svg', id: 'weary', label: 'local.conditions.weary' },
  { icon: 'systems/grpga/icons/conditions/incapacitated.svg', id: 'incapacitated', label: 'local.conditions.incapacitated' },
  { icon: 'systems/grpga/icons/conditions/dying.svg', id: 'dying', label: 'local.conditions.dying' },

  { icon: 'systems/grpga/icons/conditions/bleeding.svg', id: 'bleeding', label: 'local.conditions.bleeding' },
  { icon: 'systems/grpga/icons/conditions/blinded.svg', id: 'blind', label: 'local.conditions.blind' },
  { icon: 'systems/grpga/icons/conditions/deafened.svg', id: 'deaf', label: 'local.conditions.deaf' },
  { icon: 'systems/grpga/icons/conditions/silenced.svg', id: 'mute', label: 'local.conditions.mute' },

  { icon: 'systems/grpga/icons/conditions/burning.svg', id: 'burning', label: 'local.conditions.burning' },
  { icon: 'systems/grpga/icons/conditions/diseased.svg', id: 'diseased', label: 'local.conditions.diseased' },
  { icon: 'systems/grpga/icons/conditions/freezing.svg', id: 'freezing', label: 'local.conditions.freezing' },
  { icon: 'systems/grpga/icons/conditions/poisoned.svg', id: 'poisoned', label: 'local.conditions.poisoned' },

  { icon: 'systems/grpga/icons/conditions/healing.svg', id: 'healing', label: 'local.conditions.healing' },
  { icon: 'systems/grpga/icons/conditions/engaged.svg', id: 'engaged', label: 'local.conditions.engaged' },
  { icon: 'systems/grpga/icons/conditions/shield.svg', id: 'shielded', label: 'local.conditions.shielded' },
  { icon: 'systems/grpga/icons/conditions/stealthy.svg', id: 'stealthy', label: 'local.conditions.stealthy' },

  { icon: 'systems/grpga/icons/postures/prone.svg', id: 'prone', label: 'local.postures.prone' },
  { icon: 'systems/grpga/icons/postures/flying.svg', id: 'flying', label: 'local.postures.flying' },
  { icon: 'systems/grpga/icons/postures/swimming.svg', id: 'swimming', label: 'local.postures.swimming' },
  { icon: 'systems/grpga/icons/blank.png' },

  { icon: 'systems/grpga/icons/conditions/held.svg', id: 'held', label: 'local.conditions.held' },
  { icon: 'systems/grpga/icons/conditions/stunned.svg', id: 'stunned', label: 'local.conditions.stunned' },
  { icon: 'systems/grpga/icons/conditions/surprised.svg', id: 'surprised', label: 'local.conditions.surprised' },
  { icon: 'systems/grpga/icons/conditions/frightened.svg', id: 'frightened', label: 'local.conditions.frightened' },

  { icon: 'systems/grpga/icons/conditions/concentrating-1.svg', id: 'concentrating1', label: 'local.conditions.concentrating' },
  { icon: 'systems/grpga/icons/conditions/concentrating-2.svg', id: 'concentrating2', label: 'local.conditions.concentrating' },
  { icon: 'systems/grpga/icons/conditions/concentrating-3.svg', id: 'concentrating3', label: 'local.conditions.concentrating' },
  { icon: 'systems/grpga/icons/conditions/concentrating-4.svg', id: 'concentrating4', label: 'local.conditions.concentrating' },

  { icon: 'systems/grpga/icons/conditions/aiming-1.svg', id: 'aiming1', label: 'local.conditions.aiming' },
  { icon: 'systems/grpga/icons/conditions/aiming-2.svg', id: 'aiming2', label: 'local.conditions.aiming' },
  { icon: 'systems/grpga/icons/conditions/aiming-3.svg', id: 'aiming3', label: 'local.conditions.aiming' },
  { icon: 'systems/grpga/icons/conditions/aiming-4.svg', id: 'aiming4', label: 'local.conditions.aiming' },

  { icon: 'systems/grpga/icons/conditions/loading-1.svg', id: 'loading1', label: 'local.conditions.loading' },
  { icon: 'systems/grpga/icons/conditions/loading-2.svg', id: 'loading2', label: 'local.conditions.loading' },
  { icon: 'systems/grpga/icons/crippled/torsoinjury.svg', id: 'crippledtorso', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/crippled/headinjury.svg', id: 'crippledhead', label: 'local.conditions.crippled' },

  { icon: 'systems/grpga/icons/crippled/leftarminjury.svg', id: 'crippledleftarm', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/crippled/lefthandinjury.svg', id: 'crippledlefthand', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/crippled/leftleginjury.svg', id: 'crippledleftleg', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/crippled/leftfootinjury.svg', id: 'crippledleftfoot', label: 'local.conditions.crippled' },

  { icon: 'systems/grpga/icons/crippled/rightarminjury.svg', id: 'crippledrightarm', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/crippled/righthandinjury.svg', id: 'crippledrighthand', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/crippled/rightleginjury.svg', id: 'crippledrightleg', label: 'local.conditions.crippled' },
  { icon: 'systems/grpga/icons/crippled/rightfootinjury.svg', id: 'crippledrightfoot', label: 'local.conditions.crippled' },

  { icon: 'systems/grpga/icons/conditions/enclight.svg', id: 'lightenc', label: 'local.conditions.encumbered' },
  { icon: 'systems/grpga/icons/conditions/encnorm.svg', id: 'normenc', label: 'local.conditions.encumbered' },
  { icon: 'systems/grpga/icons/conditions/encheavy.svg', id: 'heavyenc', label: 'local.conditions.encumbered' },
  { icon: 'systems/grpga/icons/conditions/encover.svg', id: 'overenc', label: 'local.conditions.encumbered' }
];

CONFIG.controlIcons.defeated = "systems/grpga/icons/defeated.png";

CONFIG.JournalEntry.noteIcons = {
  "Marker": "systems/grpga/icons/buildings/point_of_interest.png",
  "Apothecary": "systems/grpga/icons/buildings/apothecary.png",
  "Beastmen Herd 1": "systems/grpga/icons/buildings/beastmen_camp1.png",
  "Beastmen Herd 2": "systems/grpga/icons/buildings/beastmen_camp2.png",
  "Blacksmith": "systems/grpga/icons/buildings/blacksmith.png",
  "Bretonnian City 1": "systems/grpga/icons/buildings/bret_city1.png",
  "Bretonnian City 2": "systems/grpga/icons/buildings/bret_city2.png",
  "Bretonnian City 3": "systems/grpga/icons/buildings/bret_city3.png",
  "Bretonnian Worship": "systems/grpga/icons/buildings/bretonnia_worship.png",
  "Caste Hill 1": "systems/grpga/icons/buildings/castle_hill1.png",
  "Caste Hill 2": "systems/grpga/icons/buildings/castle_hill2.png",
  "Caste Hill 3": "systems/grpga/icons/buildings/castle_hill3.png",
  "Castle Wall": "systems/grpga/icons/buildings/castle_wall.png",
  "Cave 1": "systems/grpga/icons/buildings/cave1.png",
  "Cave 2": "systems/grpga/icons/buildings/cave2.png",
  "Cave 3": "systems/grpga/icons/buildings/cave3.png",
  "Cemetery": "systems/grpga/icons/buildings/cemetery.png",
  "Chaos Portal": "systems/grpga/icons/buildings/chaos_portal.png",
  "Chaos Worship": "systems/grpga/icons/buildings/chaos_worship.png",
  "Court": "systems/grpga/icons/buildings/court.png",
  "Dwarf Beer": "systems/grpga/icons/buildings/dwarf_beer.png",
  "Dwarf Hold 1": "systems/grpga/icons/buildings/dwarf_hold1.png",
  "Dwarf Hold 2": "systems/grpga/icons/buildings/dwarf_hold2.png",
  "Dwarf Hold 3": "systems/grpga/icons/buildings/dwarf_hold3.png",
  "Empire Barracks": "systems/grpga/icons/buildings/empire_barracks.png",
  "Empire City 1": "systems/grpga/icons/buildings/empire_city1.png",
  "Empire City 2": "systems/grpga/icons/buildings/empire_city2.png",
  "Empire City 3": "systems/grpga/icons/buildings/empire_city3.png",
  "Farm": "systems/grpga/icons/buildings/farms.png",
  "Food": "systems/grpga/icons/buildings/food.png",
  "Guard Post": "systems/grpga/icons/buildings/guards.png",
  "Haunted Hill": "systems/grpga/icons/buildings/haunted_hill.png",
  "Haunted Wood": "systems/grpga/icons/buildings/haunted_wood.png",
  "Inn 1": "systems/grpga/icons/buildings/inn1.png",
  "Inn 2": "systems/grpga/icons/buildings/inn2.png",
  "Kislev City 1": "systems/grpga/icons/buildings/kislev_city1.png",
  "Kislev City 2": "systems/grpga/icons/buildings/kislev_city2.png",
  "Kislev City 3": "systems/grpga/icons/buildings/kislev_city3.png",
  "Lumber": "systems/grpga/icons/buildings/lumber.png",
  "Magic": "systems/grpga/icons/buildings/magic.png",
  "Metal": "systems/grpga/icons/buildings/metal.png",
  "Mountain 1": "systems/grpga/icons/buildings/mountains1.png",
  "Mountain 2": "systems/grpga/icons/buildings/mountains2.png",
  "Orcs": "systems/grpga/icons/buildings/orcs.png",
  "Orc Camp": "systems/grpga/icons/buildings/orc_city.png",
  "Port": "systems/grpga/icons/buildings/port.png",
  "Road": "systems/grpga/icons/buildings/roads.png",
  "Ruins": "systems/grpga/icons/buildings/ruins.png",
  "Scroll": "systems/grpga/icons/buildings/scroll.png",
  "Sigmar": "systems/grpga/icons/buildings/sigmar_worship.png",
  "Stables": "systems/grpga/icons/buildings/stables.png",
  "Standing Stones": "systems/grpga/icons/buildings/standing_stones.png",
  "Swamp": "systems/grpga/icons/buildings/swamp.png",
  "Temple": "systems/grpga/icons/buildings/temple.png",
  "Textile": "systems/grpga/icons/buildings/textile.png",
  "Tower 1": "systems/grpga/icons/buildings/tower1.png",
  "Tower 2": "systems/grpga/icons/buildings/tower2.png",
  "Tower Hill": "systems/grpga/icons/buildings/tower_hill.png",
  "Wizard Tower": "systems/grpga/icons/buildings/wizard_tower.png",
  "Ulric": "systems/grpga/icons/buildings/ulric_worship.png",
  "Village 1": "systems/grpga/icons/buildings/village1.png",
  "Village 2": "systems/grpga/icons/buildings/village2.png",
  "Village 3": "systems/grpga/icons/buildings/village3.png",
  "Wood Elves 1": "systems/grpga/icons/buildings/welves1.png",
  "Wood Elves 2": "systems/grpga/icons/buildings/welves2.png",
  "Wood Elves 3": "systems/grpga/icons/buildings/welves3.png"
};
CONFIG.tokenEffects = {
  effectSize: {
    xLarge: 2,
    large: 3,
    medium: 4,
    small: 5
  },
  effectSizeChoices: {
    small: "Small (Default) - 5x5",
    medium: "Medium - 4x4",
    large: "Large - 3x3",
    xLarge: "Extra Large - 2x2"
  }
};

system.postures = {
  "standing": "local.postures.standing",
  "crouching": "local.postures.crouching",
  "kneeling": "local.postures.kneeling",
  "crawling": "local.postures.crawling",
  "sitting": "local.postures.sitting",
  "pronef": "local.postures.pronef",
  "proneb": "local.postures.proneb"
};
system.combat = {
  defaultActionCost: 1,
  missingInitiative: "You must roll initiative first."
};
system.vsd = {
  combat: [
    { action: "Select", initiative: 90 },
    { action: "Move", initiative: 61 },
    { action: "Cast Prepared Spell", initiative: 52 },
    { action: "Cast Instantaneous Spell", initiative: 51 },
    { action: "Aimed Ranged Attack", initiative: 42 },
    { action: "Throw Ready Weapon", initiative: 41 },
    { action: "Longest Melee Plus", initiative: 36 },
    { action: "Longest Melee", initiative: 35 },
    { action: "Long Melee", initiative: 34 },
    { action: "Short Melee", initiative: 33 },
    { action: "Hand Melee", initiative: 32 },
    { action: "Hand Melee Minus", initiative: 31 },
    { action: "Unaimed Ranged Attack", initiative: 21 },
    { action: "Cast Unprepared Spell", initiative: 11 },
    { action: "Other Action", initiative: 1 },
    { action: "Wait - Ready", initiative: 0 }
  ],
  // skills tab
  oddskills: ["body", "armor"],
  skillsort: [
    { name: "Combat", data: ["blunt", "blades", "ranged", "polearms", "brawl"] },
    { name: "Adventuring", data: ["athletics", "ride", "hunting", "nature", "wandering"] },
    { name: "Roguery", data: ["acrobatics", "stealth", "locks-traps", "perception", "deceive"] },
    { name: "Lore", data: ["arcana", "charisma", "cultures", "healer", "songs-tales"] }
  ],
  skillVariables: ["difficulty", "helpers"],

  // spells tab
  spellVariables: ["spell-range", "attack-spell-range", "prepaim-time", "edb", "mp", "ah1", "ah3", "ah5", "ra5", "tokens"],
  specialrolls: ["spell-failure", "magic-resonance-roll"],
  specialmods: ["critical-severity", "mrr-location", "mrr-spell-type", "weave", "overcasting", "failed-spell-type"],

  // combat tab
  defenceVariables: ["move-mode", "move-rate", "armor-type", "melee-db", "missile-db"],
  savesort: ["willpower-saving-roll", "toughness-saving-roll"],

  defencemodifiers: ["attack-level", "sst", "hp", "drive", "hero"],

  attackVariables: ["missile-range", "prepaim-time", "edb", "parry", "tokens"],

  specialcombatrolls: ["fumble-meleethrown", "fumble-missile"],
  specialcombatmods: ["fumble-mod", "critical-severity"],

  // main tab
  primarysort: {
    name: "Stats",
    data: ["brn", "swi", "for", "wit", "wsd", "bea"]
  },
  advancementpools: ["drive", "hero", "xp", "wl"],

  // stat block import lists of items to remove from the template
  npcremove: [
    "bea", "brn", "for", "swi", "wsd", "wit",
    "blunt", "blades", "polearms", "ranged", "brawl",
    "magic-resonance-roll", "spell-failure", "armor", "body",
    "blades-dagger-pierce", "brawl-dagger-pierce", "brawl-grappling-grapple", "brawl-kick-impact", "brawl-punch-impact",
    "drive", "xp", "hero", "mp", "wl",
    "armor-type", "attack-spell-range", "helpers", "mrr-location", "mrr-spell-type", "move-mode", "ranks-level-1",
    "spell-range", "weave", "critical-grapple", "critical-impact", "critical-pierce", "critical-cut"
  ],
  npcremovemods: [
    "attack-spell-range", "spell-range", "target-is-static", "failed-spell-type",
    "mrr-location-mod", "mrr-spell-type-mod", "weave-mod",
    "bearing-bonus", "brawn-bonus", "swiftness-bonus", "fortitude-bonus", "wisdom-bonus", "wits-bonus",
    "character-attribute-creation", "drive-bonus", "helpers"
  ]
};
system.d100 = {
  primaryAttributes: ["ag", "em", "co", "in", "me", "pr", "re", "qu", "sd", "st"],
  headerVariables: [],
  skillVariables: ["difficulty","range","pp"],
  spellVariables: [],
  defenceVariables: ["difficulty"],
  attackVariables: ["pp","range"],
};
system.d120 = {
  primaryAttributes: ["iq", "ps", "pb", "me", "pp", "spd", "ma", "pe"],
  headerVariables: ["Align", "XP", "Lvl"],
  skillVariables: [],
  spellVariables: [],
  defenceVariables: [],
  attackVariables: [],
};
system.bbf = {
  primaryAttributes: ["str", "dex", "log", "wil"],
  headerVariables: ["Descr", "DP", "Rnk"],
  skillVariables: [],
  spellVariables: [],
  defenceVariables: ["hp"],
  attackVariables: ["arrows"],
};
system.d6 = {
  primaryAttributes: ["mgt", "agl", "wit", "cha"],
  headerVariables: ["Race", "Personality", "Quote", "CP", "HP", "Descr"],
  skillVariables: [],
  spellVariables: [],
  defenceVariables: ["wound_level", "actions"],
  attackVariables: ["arrows"],
};
system["3d6"] = {
  primaryAttributes: ["st", "dx", "iq", "ht", "qu"],
  headerVariables: ["Align", "XP", "Lvl"],
  skillVariables: [],
  spellVariables: [],
  defenceVariables: [],
  attackVariables: [],
};
system.mnm = {
  primaryAttributes: ["str","sta","agl","dex","fgt","int","awe","pre"],
  headerVariables: [],
  skillVariables: ["ranks_skills"],
  spellVariables: [],
  defenceVariables: ["ranks_defenses"],
  attackVariables: [],
  trainedonlyskills: { "Acrobatics": true, "Expertise": true, "Investigation": true, "Sleight of Hand": true, "Treatment": true, "Technology": true, "Vehicles": true }
};
