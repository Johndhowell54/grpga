export const init = function () {
  //Add support for the Drag Ruler module: https://foundryvtt.com/packages/drag-ruler
  Hooks.once('dragRuler.ready', SpeedProvider => {
    class FANSpeedProvider extends SpeedProvider {
      get colors() {
        return [
          { id: "ap1", default: 0x00ff00, name: "local.FANSpeedProvider.speeds.ap1" },
          { id: "ap2", default: 0x44ff00, name: "local.FANSpeedProvider.speeds.ap2" },
          { id: "ap3", default: 0x88ff00, name: "local.FANSpeedProvider.speeds.ap3" },
          { id: "ap4", default: 0xbbff00, name: "local.FANSpeedProvider.speeds.ap4" },
          { id: "ap5", default: 0xffff00, name: "local.FANSpeedProvider.speeds.ap5" },
          { id: "ap6", default: 0xffbb44, name: "local.FANSpeedProvider.speeds.ap6" },
          { id: "ap7", default: 0xff8888, name: "local.FANSpeedProvider.speeds.ap7" },
          { id: "ap8", default: 0xff44bb, name: "local.FANSpeedProvider.speeds.ap8" },
          { id: "ap9", default: 0xff00ff, name: "local.FANSpeedProvider.speeds.ap9" },
          { id: "apa", default: 0xff00bb, name: "local.FANSpeedProvider.speeds.apa" },
          { id: "apb", default: 0xff0088, name: "local.FANSpeedProvider.speeds.apb" },
          { id: "apc", default: 0xff0044, name: "local.FANSpeedProvider.speeds.apc" },
        ]
      }

      /**
       * @param {GRPGAToken} token
       */
      getRanges(token) {
        const speedAttribute = this.getSetting("speedAttribute");
        if (!speedAttribute) return [];
        const tokenSpeed = parseFloat(getProperty(token, speedAttribute));
        if (tokenSpeed === undefined) {
          console.warn(
            `Drag Ruler (FAN Speed Provider) | The configured token speed attribute "${speedAttribute}" didn't return a speed value. To use colors based on drag distance set the setting to the correct value (or clear the box to disable this feature).`,
          );
          return [];
        }
        const dashMultiplier = this.getSetting("dashMultiplier");
        switch (dashMultiplier) {
          case 2: {
            return [
              { range: tokenSpeed, color: "ap1" },
              { range: tokenSpeed * 2, color: "ap7" }
            ]
          }
          case 3: {
            return [
              { range: tokenSpeed, color: "ap1" },
              { range: tokenSpeed * 2, color: "ap5" },
              { range: tokenSpeed * 3, color: "ap9" }
            ]
          }
          case 4: {
            return [
              { range: tokenSpeed, color: "ap1" },
              { range: tokenSpeed * 2, color: "ap4" },
              { range: tokenSpeed * 3, color: "ap7" },
              { range: tokenSpeed * 4, color: "apa" }
            ]
          }
          case 6: {
            return [
              { range: tokenSpeed, color: "ap1" },
              { range: tokenSpeed * 2, color: "ap3" },
              { range: tokenSpeed * 3, color: "ap5" },
              { range: tokenSpeed * 4, color: "ap7" },
              { range: tokenSpeed * 5, color: "ap9" },
              { range: tokenSpeed * 6, color: "apb" }
            ]
          }
          case 12: {
            return [
              { range: tokenSpeed, color: "ap1" },
              { range: tokenSpeed * 2, color: "ap2" },
              { range: tokenSpeed * 3, color: "ap3" },
              { range: tokenSpeed * 4, color: "ap4" },
              { range: tokenSpeed * 5, color: "ap5" },
              { range: tokenSpeed * 6, color: "ap6" },
              { range: tokenSpeed * 7, color: "ap7" },
              { range: tokenSpeed * 8, color: "ap8" },
              { range: tokenSpeed * 9, color: "ap9" },
              { range: tokenSpeed * 10, color: "apa" },
              { range: tokenSpeed * 11, color: "apb" },
              { range: tokenSpeed * 12, color: "apc" }
            ]
          }
          default: {
            return [
              { range: tokenSpeed, color: "ap1" }
            ]
          }
        }
      }
      get settings() {
        return [
          {
            id: "speedAttribute",
            name: "drag-ruler.genericSpeedProvider.settings.speedAttribute.name",
            hint: "drag-ruler.genericSpeedProvider.settings.speedAttribute.hint",
            scope: "world",
            config: true,
            type: String,
            default: "actor.system.dynamic.movement.system.moddedvalue",
          },
          {
            id: "dashMultiplier",
            name: "local.FANSpeedProvider.settings.dashMultiplier.name",
            hint: "local.FANSpeedProvider.settings.dashMultiplier.hint",
            scope: "world",
            config: true,
            type: Number,
            default: 1,
          },
        ];
      }

    }
    dragRuler.registerSystem('grpga', FANSpeedProvider)
  })
}