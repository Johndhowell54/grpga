import { system } from "../config.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class baseActorSheet extends ActorSheet {

  itemContextMenu = [{
    name: game.i18n.localize("local.sheet.clone"),
    icon: '<i class="fas fa-copy"></i>',
    callback: (element) => {
      const item = duplicate(this.actor.items.get(element.data("item-id")));
      delete item._id;
      delete item.sort;
      if (item.flags) delete item.flags;
      if (item.effects) delete item.effects;
      this.actor.createEmbeddedDocuments("Item", [item], {
        renderSheet: true,
      });
    },
  },
  {
    name: game.i18n.localize("local.sheet.edit"),
    icon: '<i class="fas fa-edit"></i>',
    callback: (element) => {
      const item = this.actor.items.get(element.data("item-id"));
      item.sheet.render(true);
    }
  }, {
    name: game.i18n.localize("local.sheet.delete"),
    icon: '<i class="fas fa-trash"></i>',
    callback: (element) => {
      this.deleteItem(element.data("item-id"));
    }
  }];

  /** @override */
  getData() {
    if (CONFIG.system.testMode) console.debug("entering getData() in baseActor-sheet\n", this);

    const isOwner = this.actor.isOwner;
    const context = {
      owner: isOwner,
      limited: this.actor.limited,
      options: this.options,
      editable: this.isEditable,
      cssClass: isOwner ? "editable" : "locked",
      config: CONFIG.system,
      rollData: this.actor.getRollData.bind(this.actor)
    };

    // The Actor's data
    const actor = this.actor;
    const actordata = actor.system;
    context.actor = actor;
    context.system = actordata;

    context.mode = context.system.mode || context.config.ruleset;
    context.modeSpecificText = {
      mode: context.mode,
      stepT: game.i18n.localize(`local.actor.bm.${context.mode}.stepT`),
      step: game.i18n.localize(`local.actor.bm.${context.mode}.step`),
      quarterT: game.i18n.localize(`local.actor.bm.${context.mode}.quarterT`),
      quarter: game.i18n.localize(`local.actor.bm.${context.mode}.quarter`),
      halfT: game.i18n.localize(`local.actor.bm.${context.mode}.halfT`),
      half: game.i18n.localize(`local.actor.bm.${context.mode}.half`),
      moveT: game.i18n.localize(`local.actor.bm.${context.mode}.moveT`),
      move: game.i18n.localize(`local.actor.bm.${context.mode}.move`),
      sprintT: game.i18n.localize(`local.actor.bm.${context.mode}.sprintT`),
      sprint: game.i18n.localize(`local.actor.bm.${context.mode}.sprint`),
      advantages: game.i18n.localize(`local.sections.${context.mode}.advantages`),
      disadvantages: game.i18n.localize(`local.sections.${context.mode}.disadvantages`),
      perks: game.i18n.localize(`local.sections.${context.mode}.perks`),
      quirks: game.i18n.localize(`local.sections.${context.mode}.quirks`),
      defences: game.i18n.localize(`local.sections.${context.mode}.defences`),
      checks: game.i18n.localize(`local.sections.${context.mode}.checks`),
      hitLocations: game.i18n.localize(`local.sections.${context.mode}.hitLocations`),
      bsabbr: game.i18n.localize(`local.actor.bs.${context.mode}.abbr`),
      bsname: game.i18n.localize(`local.actor.bs.${context.mode}.name`),
      armourDiv: game.i18n.localize(`local.actor.melee-attack.${context.mode}.armourDiv`),
      minST: game.i18n.localize(`local.actor.melee-attack.${context.mode}.minST`),
      reach: game.i18n.localize(`local.actor.melee-attack.${context.mode}.reach`),
      range: game.i18n.localize(`local.actor.ranged-attack.${context.mode}.range`),
    }

    // My shortcuts
    const dynamic = actordata.dynamic;
    const tracked = actordata.tracked;

    actordata.primaryattributes = [];
    actordata.equipment = [];
    actordata.hitLocations = [];
    actordata.skills = [];
    actordata.techniques = [];
    actordata.rms = [];
    actordata.spells = [];
    actordata.checks = [];
    actordata.advantages = [];
    actordata.disadvantages = [];
    actordata.perks = [];
    actordata.quirks = [];
    actordata.attacks = [];
    actordata.defences = [];
    actordata.parrys = [];
    actordata.blocks = [];
    actordata.dodges = [];
    actordata.pools = [];
    actordata.containers = [];
    actordata.modifiers = [];
    actordata.variables = [];
    actordata.attackdamagemods = [];
    actordata.defencemods = [];
    actordata.reactionmods = [];
    actordata.skillmods = [];
    actordata.spellmods = [];
    actordata.checkmods = [];
    actordata.primods = [];
    actordata.mnmadvantages = [];
    actordata.mnmpowers = [];


    for (const item of actordata.items) {
      const itemdata = item.system;
      switch (item.type) {
        case "Primary-Attribute": {
          actordata.primaryattributes.push(item);
          break;
        }
        case "Equipment": {
          actordata.equipment.push(item);
          break;
        }
        case "Hit-Location": {
          actordata.hitLocations.push(item);
          break;
        }
        case "Rollable": {
          switch (itemdata.category) {
            case "technique":
              actordata.techniques.push(item);
              break;
            case "skill": {
              actordata.skills.push(item);
              break;
            }
            case "rms":
              actordata.rms.push(item);
              break;
            case "spell": {
              actordata.spells.push(item);
              break;
            }
            case "check": {
              actordata.checks.push(item);
              break;
            }
          }
          break;
        }
        case "Advantage": {
          actordata.mnmadvantages.push(item);
          break;
        }
        case "Power": {
          actordata.mnmpowers.push(item);
          break;
        }
        case "Trait": {
          if (itemdata.group.split(" ")[0].toLowerCase() != "notrait") {
            switch (itemdata.category) {
              case "advantage": {
                actordata.advantages.push(item);
                break;
              }
              case "disadvantage": {
                actordata.disadvantages.push(item);
                break;
              }
              case "quirk": {
                actordata.quirks.push(item);
                break;
              }
              case "perk": {
                actordata.perks.push(item);
                break;
              }
            }
          }
          break;
        }
        case "Ranged-Attack":
        case "Melee-Attack": {
          actordata.attacks.push(item);
          break;
        }
        case "Defence": {
          actordata.defences.push(item);
          switch (itemdata.category) {
            case "block": {
              actordata.blocks.push(item);
              break;
            }
            case "parry": {
              actordata.parrys.push(item);
              break;
            }
            case "dodge": {
              actordata.dodges.push(item);
              break;
            }
          }
          break;
        }
        case "Pool": {
          actordata.pools.push(item);
          break;
        }
        case "Container": {
          actordata.containers.push(item);
          break;
        }
        case "Variable": {
          actordata.variables.push(item);
          break;
        }
        case "Modifier": {
          actordata.modifiers.push(item);
          if (itemdata.attack || itemdata.damage) actordata.attackdamagemods.push(item);
          if (itemdata.defence) actordata.defencemods.push(item);
          if (itemdata.reaction) actordata.reactionmods.push(item);
          if (itemdata.skill) actordata.skillmods.push(item);
          if (itemdata.spell) actordata.spellmods.push(item);
          if (itemdata.check) actordata.checkmods.push(item);
          if (itemdata.primary) actordata.primods.push(item);
          break;
        }
      }
      if (item.type != "Trait") {// For non-trait items to be displayed with them
        // add the item to the trait list
        switch (itemdata.group.split(" ")[0].toLowerCase()) {
          case "gift":
          case "feat":
          case "advantage": {
            actordata.advantages.push(item);
            break;
          }
          case "racial":
          case "disadvantage": {
            actordata.disadvantages.push(item);
            break;
          }
          case "class":
          case "quirk": {
            actordata.quirks.push(item);
            break;
          }
          case "character":
          case "perk": {
            actordata.perks.push(item);
            break;
          }
        }
      }
    }

    // filter items in All Items
    actordata.allitemstemp = [];
    let filterstring = "";
    for (const filter of Object.entries(actordata.allitems)) {
      const itemtype = {
        name: filter[0],
        filtered: filter[1],
        localname: game.i18n.localize(`local.item.${filter[0]}.itemname`)
      }
      actordata.allitemstemp.push(itemtype);
      if (filter[1]) filterstring += filter[0];
    }
    if (filterstring == "") {
      context.filtereditems = actordata.items;
    } else {
      context.filtereditems = actordata.items.filter(function (item) {
        if (filterstring.includes(item.type)) return item;
        return null;
      });
    }

    actordata.attackVariables = [];
    for (let varname of system[context.mode].attackVariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.attackVariables.push(dynamic[varname]);
      else if (tracked[varname]) actordata.attackVariables.push(tracked[varname]);
    }
    actordata.defenceVariables = [];
    for (let varname of system[context.mode].defenceVariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.defenceVariables.push(dynamic[varname]);
      else if (tracked[varname]) actordata.defenceVariables.push(tracked[varname]);
    }
    actordata.skillVariables = [];
    for (let varname of system[context.mode].skillVariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.skillVariables.push(dynamic[varname]);
      else if (tracked[varname]) actordata.skillVariables.push(tracked[varname]);
    }
    actordata.spellVariables = [];
    for (let varname of system[context.mode].spellVariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.spellVariables.push(dynamic[varname]);
      else if (tracked[varname]) actordata.spellVariables.push(tracked[varname]);
    }

    return context;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.item-create').click(this._onItemCreate.bind(this));
    html.find('.item-export').click(this._onItemExport.bind(this));
    html.find('.importJSON').click(this._onImportJSON.bind(this));

    html.find('.iteminput').change(this._onInputChange.bind(this));
    html.find('.plus').click(this._onPlusMinus.bind(this));
    html.find('.minus').click(this._onPlusMinus.bind(this));
    html.find('.rollable').click(this._onRoll.bind(this));
    html.find('.rollmacro').click(this._rollmacro.bind(this));
    html.find('.item-edit').click(this._onItemEdit.bind(this));
    html.find('.item-delete').click(this._onItemDelete.bind(this));
    html.find('.item-toggle').click(this._onToggleItem.bind(this));
    html.find('.collapsible').click(this._onToggleCollapse.bind(this));

    new ContextMenu(html, ".item", this.itemContextMenu);

    let handler = ev => this._onDragStart(ev);
    html.find('.draggable').each((i, li) => {
      li.setAttribute("draggable", true);
      li.addEventListener("dragstart", handler, false);
    });

    html.on('click', '.filter', ev => {
      ev.preventDefault();
      const type = ev.currentTarget.dataset.type;
      const current = this.actor.system.allitems[type];
      const update = `system.allitems.${type}`;
      this.actor.update({ [update]: !current });
    });

    html.on('click', '.open-journal', ev => {
      ev.preventDefault();
      const journal = ev.currentTarget.dataset.table;
      try {
        Journal._showEntry(game.journal.getName(journal).uuid, "text", true);
      } catch (err) {
        if (journal) ui.notifications.error(`There is no matching Journal Entry for ${journal}. Perhaps you have yet to import it from the compendium?`);
      }
    });

  }

  /**
   * Each field is a comma-separated list of the things you want
   * to include in the result. It matches against the text you
   * enter, so a category of "s" will match skill and rms.
   *
   * type: may be any of {Primary, Melee, Ranged,
   * Attack, Rollable}
   *
   * category: applies only to:
   * Rollable: {skill, spell, technique, rms, check}
   * and may be used without specifying the type.
   *
   * group: is a user-defined field for use in filtering
   * this result.
   *
   * target: Any case-sensitive portion of the item name.
   */
  _rollmacro(ev) {
    const options = {
      position: {
        width: 300,
        top: 100,
        left: 100,
      },
      type: ev.currentTarget.dataset.type || "",
      category: ev.currentTarget.dataset.category || "",
      group: ev.currentTarget.dataset.group || "",
      target: ev.currentTarget.dataset.target || "",
      damage: ev.currentTarget.dataset.damage == "damage" || false,
    };
    this.actor.rollables(options);
  }

  async _onRoll(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;

    let flavour = "";
    if (game.i18n.format(`local.phrases.${this.actor.system.mode}.${dataset.type}`, { name: dataset.name })) {
      flavour = game.i18n.format(`local.phrases.${this.actor.system.mode}.${dataset.type}`, { name: dataset.name });
    } else if (game.i18n.format(`local.phrases.${dataset.type}`, { name: dataset.name })) {
      flavour = game.i18n.format(`local.phrases.${dataset.type}`, { name: dataset.name });
    }
    const rolldata = {
      actor: this.actor,
      flavour: flavour,
      name: dataset.name || "",
      roll: dataset.roll || "",
      type: dataset.type || "",
      modtype: dataset.modtype || "",
      id: dataset.itemId
    }

    this.actor.roll(rolldata);
  }

  async _onImportJSON(event) {
    event.preventDefault();
    let scriptdata = this.actor.system.itemscript || "";
    // preserve newlines, etc - use valid JSON
    scriptdata = scriptdata.replace(/\\n/g, "\\n")
      .replace(/\\'/g, "\\'")
      .replace(/\\"/g, '\\"')
      .replace(/\\&/g, "\\&")
      .replace(/\\r/g, "\\r")
      .replace(/\\t/g, "\\t")
      .replace(/\\b/g, "\\b")
      .replace(/\\f/g, "\\f");
    // remove non-printable and other non-valid JSON chars
    scriptdata = scriptdata.replace(/[\u0000-\u0019]+/g, "");
    const data = JSON.parse(scriptdata);
    console.log("JSON Object\n", data);
  }

  _onToggleCollapse(event) {
    event.preventDefault();
    if (CONFIG.system.testMode) console.debug("entering _onToggleCollapse()", [this, event]);
    // do not collapse when creating an item
    if (event.target.parentElement.className.includes('item-create')) return;
    if (event.target.parentElement.className.includes('char-name')) return;
    let section = event.currentTarget.dataset.section;
    let current = this.actor.system.sections[section];
    let update = `system.sections.${section}`;
    let data = (current === "block") ? "none" : "block";
    this.actor.update({ [update]: data });
  }

  async _onPlusMinus(event) {
    event.preventDefault();

    if (CONFIG.system.testMode) console.debug("entering _onPlusMinus()", [this, event]);

    let field = event.currentTarget.firstElementChild;
    let actordata = this.actor.system;
    let fieldName = field.name;
    let change = Number(field.value);
    var value;
    var fieldValue;

    if (field.className.includes("pool")) {
      return this.actor.modifyTokenAttribute(`tracked.${fieldName.toLowerCase()}`, field.value, true, true);

    } else { // haven't figured out non-pools yet

      switch (fieldName) {
        case "gmod": {
          fieldValue = "system.gmod.value";
          value = change + actordata.gmod.value;
          this.actor.update({ [fieldValue]: value });
          break;
        }
        case "IS": {
          fieldValue = "system.is.value";
          value = change + actordata.is.value;
          this.actor.update({ [fieldValue]: value });
          break;
        }
        case "DLtH": {
          fieldValue = "system.dlth.value";
          value = change + actordata.dlth.value;
          this.actor.update({ [fieldValue]: value });
          break;
        }
        default: {
          break;
        }
      }
    }

  }

  _onToggleItem(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);
    let attr = "";
    switch (item.type) {
      case "Modifier": {
        attr = "system.inEffect";
        break;
      }
      default: {
        ui.notifications.warning(`Toggling of ${item.type} is not yet supported.`)
      }
    }
    return this.actor.updateEmbeddedDocuments("Item", [{ _id: itemId, [attr]: !getProperty(item, attr) }]);
  }

  _onItemEdit(event) {
    event.preventDefault();
    let element = event.currentTarget;
    let itemId = element.closest(".item").dataset.itemId;
    let item = this.actor.items.get(itemId);
    item.sheet.render(true);
  }

  deleteItem(itemId) {
    const item = this.actor.items.get(itemId);
    const items = [itemId];
    if (item.type == "Container") {
      if (item.system.dropped)
        for (let dropped of item.system.dropped) {
          if (this.actor.items.get(dropped))
            items.push(dropped);
        }
    }
    this.actor.deleteEmbeddedDocuments("Item", items);
  }

  _onItemDelete(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const itemId = element.closest(".item").dataset.itemId;
    this.deleteItem(itemId);
  }

  async dropItem(item) { // special container behaviour

    // the item has already been created on the actor sheet
    let baseitem = await this.actor.items.get(item[0].id);

    if (baseitem.type == "Container") {
      // unpack the container
      const created = await this.actor.createEmbeddedDocuments("Item", baseitem.system.entries);
      const dropped = [];
      for (let item of created) {
        dropped.push(item.id);
      }
      baseitem.update({ 'system.dropped': dropped });
    } else {
      // show the item sheet
      baseitem.sheet.render(true);
    }
    return baseitem;
  }

  async dropData(dragItem) {
    if (CONFIG.system.testMode) console.debug(`processing ${dragItem.type}\n`, dragItem);
    switch (dragItem.type) {
      case "dynamic": {
        const chatmessage = await game.collections.get("ChatMessage").get(dragItem.id);
        const itemdata = chatmessage.getFlag("grpga", "itemdata");
        const item = await this.actor.createEmbeddedDocuments("Item", [itemdata]);
        const baseitem = await this.actor.items.get(item[0].id);
        baseitem.sheet.render(true);
        break;
      }
      case "damage": {
        // apply damage so it will update status and bar
        await this.actor.modifyTokenAttribute("tracked.hp", -dragItem.hits, true);
        ChatMessage.create({
          speaker: { actor: this.actor.id },
          content: `Has taken ${dragItem.hits} damage.`,
        });
        break;
      }
      case "healing": {
        await this.actor.modifyTokenAttribute("tracked.hp", dragItem.hits, true);
        ChatMessage.create({
          speaker: { actor: this.actor.id },
          content: `Has regained ${dragItem.hits} health.`,
        });
        break;
      }
      case "souldamage": {
        // apply damage so it will update status and bar
        const item = await this.actor.getNamedItem("dynamic.sd");
        await item.update({ "system.attr": Number(item.system.attr) + Number(dragItem.hits) });
        ChatMessage.create({
          speaker: { actor: this.actor.id },
          content: `Has taken ${dragItem.hits} soul damage.`,
        });
        break;
      }
    }
  }

  /** @override */
  async _onDrop(event) {
    // Try to extract the data
    let dragData;
    try {
      dragData = JSON.parse(event.dataTransfer.getData('text/plain'));
    } catch (err) {
      return false;
    }
    const actor = this.actor;

    // a contained item or wound effect will not have a uuid
    const temp = dragData.uuid?.split(".") || false;

    let itemids = {};
    if (temp) {
      for (let i = 0; i < temp.length; i++) {
        itemids[temp[i]] = temp[++i];
      }
    }

    const sameActor = (actor.isToken) ? actor.uuid.includes(itemids.Token) : actor.id == itemids.Actor;

    // wait for the item to be copied to the actor
    let item = await super._onDrop(event);

    if (sameActor) return item;
    if (item) {
      // a proper item was dropped and identified so unpack it or render it
      return this.dropItem(item);
    } else {
      // a piece of non-item data was dropped
      this.dropData(dragData);
    }
  }

  async _onTokenDrop(dragItem) {
    // wait for the item to be copied to the actor
    if (dragItem.type == "Item") {
      let item = await this._onDropItem(null, dragItem);
      return this.dropItem(item);
    } else {
      this.dropData(dragItem);
    }
  }

  _onInputChange(event) {
    event.preventDefault();

    if (CONFIG.system.testMode) console.debug("entering _onInputChange()", [this, event]);

    const target = event.currentTarget;
    // get the item id
    const itemId = target.parentElement.attributes["data-item-id"]?.value;
    // get the name of the changed element
    let dataname = target.attributes["data-name"].value;
    // get the new value
    let value = (target.type === "checkbox") ? target.checked : target.value;
    // Get the original item.
    const item = this.actor.items.get(itemId);

    if (dataname == "system.alwaysOn") {
      item.update({ "system.alwaysOn": value, "system.inEffect": true });
      return;
    }

    // redirect changes to pool values
    if (target.className.includes("pool")) {
      let isDelta = (value.startsWith("+") || value.startsWith("-"));
      return this.actor.modifyTokenAttribute(`tracked.${target.dataset.abbr.toLowerCase()}`, value, isDelta, true);
    }
    // update the item with the new value for the element or the actor if it is gmod
    (item) ? item.update({ [dataname]: value }) : this.actor.update({ [dataname]: value });
  }

  checkSpellTypeLevel(spell, level) {
    var levelString = spell.system.notes.match("<p><b>Level</b>:[^<]*");
    return levelString[0].match(level);
  }

  _onItemCreate(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;
    const scriptdata = this.actor.system.itemscript || "";
    if (!dataset.type) {
      this._createItems(scriptdata);
    } else {
      this._createItem(dataset);
    }
  }

  _onItemExport() {
    let items = duplicate(this.actor.system.items);
    var exported = [];
    for (var itemdata of items) {
      if (itemdata.type == "Container") continue;
      delete itemdata._id;
      delete itemdata.flags;
      delete itemdata.sort;
      delete itemdata.effects;
      exported.push(itemdata);
    }
    exported = JSON.stringify(exported);
    this.actor.update({ "system.itemscript": exported });
  }

  async _createItems(scriptdata) {
    // preserve newlines, etc - use valid JSON
    scriptdata = scriptdata.replace(/\\n/g, "\\n")
      .replace(/\\'/g, "\\'")
      .replace(/\\"/g, '\\"')
      .replace(/\\&/g, "\\&")
      .replace(/\\r/g, "\\r")
      .replace(/\\t/g, "\\t")
      .replace(/\\b/g, "\\b")
      .replace(/\\f/g, "\\f");
    // remove non-printable and other non-valid JSON chars
    scriptdata = scriptdata.replace(/[\u0000-\u0019]+/g, "");
    // swap the chartype with this actors chartype
    let type = this.actor.type;
    scriptdata = scriptdata.replace(/Character\w+/g, type);
    await this.actor.createEmbeddedDocuments('Item', JSON.parse(scriptdata));
    await this.actor.update({ 'system.itemscript': '' });
    ui.notifications.info("Creation of multiple items completed!");
  }

  async _createItem(dataset) {
    let itemdata = {
      name: dataset.type,
      type: dataset.type,
      system: {
        chartype: this.actor.type,
      }
    };
    switch (dataset.type) {
      case "Modifier": {
        itemdata.system.attack = (dataset.mod1 == "attack" || dataset.mod2 == "attack");
        itemdata.system.defence = (dataset.mod1 == "defence" || dataset.mod2 == "defence");
        itemdata.system.skill = (dataset.mod1 == "skill" || dataset.mod2 == "skill");
        itemdata.system.spell = (dataset.mod1 == "spell" || dataset.mod2 == "spell");
        itemdata.system.check = (dataset.mod1 == "check" || dataset.mod2 == "check");
        itemdata.system.reaction = (dataset.mod1 == "reaction" || dataset.mod2 == "reaction");
        itemdata.system.damage = (dataset.mod1 == "damage" || dataset.mod2 == "damage");
        itemdata.system.pool = (dataset.mod1 == "pool" || dataset.mod2 == "pool");
        itemdata.system.primary = (dataset.mod1 == "primary" || dataset.mod2 == "primary");
        break;
      }
      case "Defence":
      case "Trait":
      case "Rollable": {
        itemdata.system.category = dataset.category;
        break;
      }
    }
    return await this.actor.createEmbeddedDocuments('Item', [itemdata], { renderSheet: true });
  }
}