import { system } from "../config.js";

/**
 * A utility class defining behaviour for ruleset-specific actors
 */
export class ActorD100 {

  constructor(actor) {
    this.actor = actor;
    this.compendiumName = "Base Items";
  }

  prepareBaseData() {
    const actordata = this.actor.system;
    actordata.bm.quarter = Number(actordata.bm.step * 1.5);
    actordata.bm.half = actordata.bm.step * 2;
    actordata.bm.move = actordata.bm.step * 3;
    actordata.bm.sprint = actordata.bm.step * 4;
    actordata.bm.dash = actordata.bm.step * 5;
    actordata.dynamic.woundsbleed = { "value": 0 };
    actordata.dynamic.woundspen = { "value": 0 };
    actordata.hasStatBonusTable = system.hasStatBonusTable();
  }

  prepareAdditionalData() {
    const actor = this.actor;
    const actordata = actor.system;
    if (CONFIG.system.testMode) console.debug("entering prepareDerivedData()\n", [actor, actordata]);

    const useRolemasterDefenses = actor.type == "CharacterD100";

    // for counting the ranks stored in Variable Items for some systems
    let actorranks = {};
    let actorgcranks = {};
    let actorcombranks = {};
    let actorlimranks = {};
    let actorspecranks = {};
    let rankitems = [];
    let rankgcitems = [];
    let rankcombitems = [];
    let ranklimitems = [];
    let rankspecitems = [];

    // group the items for efficient iteration
    const actormods = [];
    const nonmods = [];
    const attackmods = [];
    const primods = [];
    const valuemods = [];
    const values = [];
    const skillmods = [];
    const skills = [];
    const rollables = [];
    const primaries = [];

    // store references to primary attribute and pool items
    let previousLayer = [];
    let nextLayer = [];

    // write the items into the model
    for (const item of actordata.items) {
      const itemdata = item.system;
      let itemID = actor.slugify(item.name);
      // collect non-modifier items with formula
      if (itemdata.formula) nonmods.push(item);

      switch (item.type) {
        case "Primary-Attribute": {
          itemID = actor.slugify(itemdata.abbr);
          // only process primaries with abbreviations
          if (itemID) {
            itemdata.value = Number(itemdata.attr);
            if (actordata.hasStatBonusTable && system[actordata.mode].primaryAttributes.includes(itemdata.abbr.toLowerCase())) {
              // this makes the stat modifiers available as a reference
              itemdata.moddedvalue = system.sbs(itemdata.value);
            } else {
              // this makes the "stat" behave normally
              itemdata.moddedvalue = itemdata.value;
            }
            // store a reference to the item in the data structure
            actordata.dynamic[itemID] = item;
            // add this to the processed references
            nextLayer.push(itemID);
            primaries.push(item);
          }
          break;
        }
        case "Rank-Progression": {
          actordata.rankitem = item;
          break;
        }
        case "Trait": {
          // store a reference in the data structure
          actordata.traits[itemID] = item;
          switch (item.name) {
            case "Keen Senses":
            case "Night Sight":
            case "Dark Sight":
            case "Darkvision":
            case "Star Sight": {
              actordata.dynamic.visionType = item.name;
            }
          }
          break;
        }
        case "Melee-Attack":
        case "Ranged-Attack":
        case "Advantage":
        case "Power":
        case "Defence":
        case "Rollable": {
          // If the formula is numeric, store it in the array of valid references
          if (Number.isNumeric(itemdata.formula)) {
            nextLayer.push(itemID);
            itemdata.value = itemdata.moddedvalue = Number(itemdata.formula);
          } else if (itemdata.formula.startsWith("#")) {
            nextLayer.push(itemID);
          } else {
            itemdata.moddedformula = "";
            itemdata.value = itemdata.moddedvalue = 0;
          }
          // store a reference in the data structure
          actordata.dynamic[itemID] = item;
          // store a reference in a temporary array
          switch (itemdata.category) {
            case "check":
            case "technique": {
              values.push(item);
              break;
            }
            case "skill":
            case "spell": {
              skills.push(item);
              break;
            }
            default: {
              rollables.push(item);
            }
          }
          break;
        }
        case "Hit-Location": {
          // summary of wound effects
          actordata.dynamic.woundsbleed.value += itemdata.damageResistance;
          actordata.dynamic.woundspen.value += itemdata.damage;
          break;
        }
        case "Pool": {
          itemID = actor.slugify(itemdata.abbr);
          if (itemID) {
            itemdata.moddedvalue = itemdata.value;
            nextLayer.push(itemID);
            // store a reference in the data structure for resources
            actordata.tracked[itemID] = item;
            nonmods.push(item);
          }
          break;
        }
        case "Variable": {
          // If the formula is numeric, store it in the array of valid references
          if (Number.isNumeric(itemdata.formula)) {
            itemdata.value = itemdata.moddedvalue = Number(itemdata.formula);
            nextLayer.push(itemID);
          }
          // store a reference in the data structure
          actordata.dynamic[itemID] = item;

          // add any items whose name starts with "Ranks:" to the list to be polled next
          if (item.name.startsWith("Ranks:")) rankitems.push(item);
          if (item.name.startsWith("RanksGC:")) rankgcitems.push(item);
          if (item.name.startsWith("RanksComb:")) rankcombitems.push(item);
          if (item.name.startsWith("RanksLim:")) ranklimitems.push(item);
          if (item.name.startsWith("RanksSpec:")) rankspecitems.push(item);

          break;
        }
        case "Modifier": {
          itemdata.tempName = item.name;
          // store a reference in the data structure array of modifiers
          actordata.modifiers[itemID] = item;
          // if it modifies ability stats, add it to a temporary array
          if (itemdata.primary) primods.push(item);
          if (itemdata.attack) attackmods.push(item);
          if (itemdata.check || itemdata.reaction) valuemods.push(item);
          if (itemdata.skill || itemdata.spell) skillmods.push(item);
          actormods.push(item);
          break;
        }
        default: { // Traits that define a vision type for the actor
          actordata.dynamic[itemID] = item;
        }
      }
    }

    /**
     * Only count ranks for actors of the current ruleset and possessing a rankitem.
     * 
     * Calculate the ranks, then process number formulae
     */
    if (actordata.rankitem && (actordata.ruleset == actordata.mode)) {
      if (CONFIG.system.testMode) console.debug("Rankitems to be polled:\n", [this, rankitems, rankgcitems, rankcombitems, ranklimitems, rankspecitems]);

      // count the rank standard skill items
      for (const item of rankitems) {
        for (const entry of item.system.entries) {
          let target = actor.slugify(entry.label);
          // if the name is not blank
          if (target) {
            entry.value = Number(entry.formula);
            // the target may be one that does not exist on the actor so capture it anyway
            actorranks[target] = (actorranks[target]) ? actorranks[target] + entry.value : entry.value;
          }
        }
      }
      // count the rank skill group and category items
      for (const item of rankgcitems) {
        for (const entry of item.system.entries) {
          let target = actor.slugify(entry.label);
          // if the name is not blank
          if (target) {
            entry.value = Number(entry.formula);
            // the target may be one that does not exist on the actor so capture it anyway
            actorgcranks[target] = (actorgcranks[target]) ? actorgcranks[target] + entry.value : entry.value;
          }
        }
      }
      // count the rank combination skill items
      for (const item of rankcombitems) {
        for (const entry of item.system.entries) {
          let target = actor.slugify(entry.label);
          // if the name is not blank
          if (target) {
            entry.value = Number(entry.formula);
            // the target may be one that does not exist on the actor so capture it anyway
            actorcombranks[target] = (actorcombranks[target]) ? actorcombranks[target] + entry.value : entry.value;
          }
        }
      }
      // count the rank limited skill items
      for (const item of ranklimitems) {
        for (const entry of item.system.entries) {
          let target = actor.slugify(entry.label);
          // if the name is not blank
          if (target) {
            entry.value = Number(entry.formula);
            // the target may be one that does not exist on the actor so capture it anyway
            actorlimranks[target] = (actorlimranks[target]) ? actorlimranks[target] + entry.value : entry.value;
          }
        }
      }
      // count the rank limited skill items
      for (const item of rankspecitems) {
        for (const entry of item.system.entries) {
          let target = actor.slugify(entry.label);
          // if the name is not blank
          if (target) {
            entry.value = Number(entry.formula);
            // the target may be one that does not exist on the actor so capture it anyway
            actorspecranks[target] = (actorspecranks[target]) ? actorspecranks[target] + entry.value : entry.value;
          }
        }
      }

      if (CONFIG.system.testMode) console.debug("Rankdata after polling:\n", [this, actorranks, actorgcranks, actorcombranks, actorlimranks, actorspecranks]);

      let rankprog = actordata.rankitem.system.standard || "0:1:1:1:1";

      // assign the ranks to the targets in dynamic
      for (const [name, ranks] of Object.entries(actorranks)) {
        if (actordata.dynamic[name]) {
          const item = actordata.dynamic[name];
          const itemdata = item.system;
          // calculate rank bonus to assign to value and moddedvalue
          const value = actor.rankCalculator(ranks, rankprog);
          itemdata.formula = `#${ranks}`;
          itemdata.ranks = ranks;
          itemdata.value = value;
          itemdata.moddedvalue = value;
          nextLayer.push(name);
        } else {
          if (CONFIG.system.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
        }
      }
      rankprog = actordata.rankitem.system.skillcategory || "0:1:1:1:1";
      // assign the ranks to the targets in dynamic
      for (const [name, ranks] of Object.entries(actorgcranks)) {
        if (actordata.dynamic[name]) {
          const item = actordata.dynamic[name];
          const itemdata = item.system;
          // calculate category rank bonus to assign to value and moddedvalue
          const value = actor.rankCalculator(ranks, rankprog);
          itemdata.formula = `#${ranks}`;
          itemdata.ranks = ranks;
          itemdata.value = value;
          itemdata.moddedvalue = value;
          nextLayer.push(name);
        } else {
          if (CONFIG.system.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
        }
      }
      rankprog = actordata.rankitem.system.limited || "0:1:1:1:1";
      // assign the ranks to the targets in dynamic
      for (const [name, ranks] of Object.entries(actorlimranks)) {
        if (actordata.dynamic[name]) {
          const item = actordata.dynamic[name];
          const itemdata = item.system;
          // calculate category rank bonus to assign to value and moddedvalue
          const value = actor.rankCalculator(ranks, rankprog);
          itemdata.formula = `#${ranks}`;
          itemdata.ranks = ranks;
          itemdata.value = value;
          itemdata.moddedvalue = value;
          nextLayer.push(name);
        } else {
          if (CONFIG.system.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
        }
      }
      rankprog = actordata.rankitem.system.combined || "0:1:1:1:1";
      // assign the ranks to the targets in dynamic
      for (const [name, ranks] of Object.entries(actorcombranks)) {
        if (actordata.dynamic[name]) {
          const item = actordata.dynamic[name];
          const itemdata = item.system;
          // calculate category rank bonus to assign to value and moddedvalue
          const value = actor.rankCalculator(ranks, rankprog);
          itemdata.formula = `#${ranks}`;
          itemdata.ranks = ranks;
          itemdata.value = value;
          itemdata.moddedvalue = value;
          nextLayer.push(name);
        } else {
          if (CONFIG.system.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
        }
      }
      // assign the ranks to the targets in dynamic
      for (const [name, ranks] of Object.entries(actorspecranks)) {
        if (actordata.dynamic[name]) {
          const item = actordata.dynamic[name];
          if (item.name == "PPDev") {
            rankprog = actordata.rankitem.system.powerpointdev || "0:1:1:1:1";
          } else {
            rankprog = actordata.rankitem.system.bodydev || "0:1:1:1:1";
          }
          const itemdata = item.system;
          // calculate category rank bonus to assign to value and moddedvalue
          const value = actor.rankCalculator(ranks, rankprog);
          itemdata.formula = `#${ranks}`;
          itemdata.ranks = ranks;
          itemdata.value = value;
          itemdata.moddedvalue = value;
          nextLayer.push(name);
        } else {
          if (CONFIG.system.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
        }
      }
    }

    // Primary Attribute modifiers first
    for (const item of primods) {
      if (item.system.inEffect) {
        for (const entry of item.system.entries) {
          // this entry is a primod
          if (entry.category == "primary") {
            // process primods formulae before the mods are applied
            actor.processModifierFormula(entry, item);
            const cases = entry.targets.split(",").map(word => word.trim().toLowerCase());
            for (const key of cases) {
              const target = actordata.dynamic[key];
              if (target?.type == "Primary-Attribute") {
                target.system.moddedvalue += entry.value;
                if (actordata.hasStatBonusTable && system[actordata.mode].primaryAttributes.includes(target.system.abbr.toLowerCase())) {
                  /**
                   * This is a primary attribute and 
                   * there is a stat bonus table in effect so 
                   * the value and moddedvalue must remain separate.
                   */
                } else {
                  // this makes the "stat" behave normally
                  target.system.value = target.system.moddedvalue;
                }
              }
            }
          }
        }
      }
    }

    // this will store all valid modifier entries for later processing efficiency
    const activeModEntries = [];

    /**
     * Process value formulae, value modifiers, then calculate modified values
     */
    let valueNextLayer = [...nextLayer];
    let valuePreviousLayer = [];
    while (valueNextLayer.length !== 0) {
      valuePreviousLayer = valueNextLayer;
      valueNextLayer = [];

      for (const item of values) {
        const itemdata = item.system;
        //ignore items with no formula or whose formula has already been processed
        if (Number.isNumeric(itemdata.formula) || (Number(itemdata.moddedformula) == itemdata.value && itemdata.moddedformula != ""))
          continue;

        let dynID = actor.slugify(item.name);

        if (itemdata.formula.includes("@")) {
          for (const target of valuePreviousLayer) {
            // if this formula refers to an item in the previous layer then process it
            if (itemdata.formula.includes(target)) {
              let formData = actor._replaceData(itemdata.formula);
              try {
                itemdata.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, "")) * 100) / 100;
                itemdata.moddedformula = "" + itemdata.value; // store the result as a String
              } catch (err) {
                // store the formula ready to be rolled
                itemdata.moddedformula = formData.value;
                itemdata.value = 0; // eliminate any old values
                console.warn("Non-Modifier formula evaluation incomplete:\n", [actor.name, item.name, itemdata.moddedformula]);
              }
              // put this item into the next layer
              if (!nextLayer.includes(dynID)) {
                valueNextLayer.push(dynID);
                nextLayer.push(dynID);
              }
            }
          }
        }
      }
    }
    for (const item of valuemods) {
      const itemdata = item.system;
      if (itemdata.inEffect) {
        for (const entry of itemdata.entries) {
          // this entry is a value modifier
          switch (entry.category) {
            case "check":
            case "technique":
              actor.processModifierFormula(entry, item);
              if (entry.value == 0) continue;
              activeModEntries.push(entry);
          }
        }
      }
    }
    for (const item of values) {
      const itemdata = item.system;
      itemdata.moddedvalue = itemdata.value;
      // if there is no modified formula, make it the value
      if (itemdata.moddedformula == undefined) itemdata.moddedformula = "" + itemdata.value;
      // apply the modifiers calculated above
      itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "check", activeModEntries);
    };

    /**
     * Process skill/spell formulae, skill/spell modifiers, then calculate modified values
     */
    let skillNextLayer = [...nextLayer];
    let skillPreviousLayer = [];
    while (skillNextLayer.length !== 0) {
      skillPreviousLayer = skillNextLayer;
      skillNextLayer = [];

      for (const item of skills) {
        const itemdata = item.system;
        //ignore items with no formula or whose formula has already been processed
        if (Number.isNumeric(itemdata.formula) || (Number(itemdata.moddedformula) == itemdata.value && itemdata.moddedformula != ""))
          continue;

        let dynID = actor.slugify(item.name);

        if (itemdata.formula.includes("@")) {
          for (const target of skillPreviousLayer) {
            // if this formula refers to an item in the previous layer then process it
            if (itemdata.formula.includes(target)) {
              let formData = actor._replaceData(itemdata.formula);
              try {
                itemdata.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, "")) * 100) / 100;
                itemdata.moddedformula = "" + itemdata.value; // store the result as a String
              } catch (err) {
                // store the formula ready to be rolled
                itemdata.moddedformula = formData.value;
                itemdata.value = 0; // eliminate any old values
                console.warn("Non-Modifier formula evaluation incomplete:\n", [actor.name, item.name, itemdata.moddedformula]);
              }
              // put this item into the next layer
              if (!nextLayer.includes(dynID)) {
                skillNextLayer.push(dynID);
                nextLayer.push(dynID);
              }
            }
          }
        }
      }
    }
    for (const item of skillmods) {
      const itemdata = item.system;
      if (itemdata.inEffect) {
        for (const entry of itemdata.entries) {
          // this entry is a value modifier
          switch (entry.category) {
            case "skill":
            case "spell":
              actor.processModifierFormula(entry, item);
              if (entry.value == 0) continue;
              activeModEntries.push(entry);
          }
        }
      }
    }
    for (const item of skills) {
      const itemdata = item.system;
      itemdata.moddedvalue = itemdata.value;
      // if there is no modified formula, make it the value
      if (itemdata.moddedformula == undefined) itemdata.moddedformula = "" + itemdata.value;
      // apply the modifiers calculated above
      itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, itemdata.category, activeModEntries);
    };

    /**
     * Calculate every item with a formula, one layer at a time, starting with
     * those whose references have been calculated. Once an item has been
     * processed, add it to the layer of those available as references.
     *
     * TODO: This section needs another look to see if there is a way to
     * avoid the requirement to have all reference depths the same.
     */
    while (nextLayer.length !== 0) {
      previousLayer = nextLayer;
      nextLayer = [];

      for (const item of nonmods) {
        const itemdata = item.system;

        if (item.type == "Pool") { // new pool formula functionality
          const dynID = actor.slugify(itemdata.abbr);
          //ignore numeric formulae
          if (itemdata.hasmax && itemdata.hasmin) continue;
          if (!itemdata.hasmax) { // formula has not yet been calculated
            for (const target of previousLayer) {
              // only need to process the terms once
              if (itemdata.hasmax) continue;
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.maxForm.includes(target)) {
                const formData = actor._replaceData(itemdata.maxForm);
                try {
                  itemdata.max = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, '')) * 100) / 100;
                } catch (err) {
                  itemdata.max = 0; // eliminate any old values
                  console.warn("Pool-max formula evaluation error:\n", [actor.name, item.name, itemdata.maxForm]);
                }
                itemdata.hasmax = true;
              }
            }
          }
          if (!itemdata.hasmin) { // formula has not yet been calculated
            for (const target of previousLayer) {
              // only need to process the terms once
              if (itemdata.hasmin) continue;
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.minForm.includes(target)) {
                const formData = actor._replaceData(itemdata.minForm);
                try {
                  itemdata.min = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, '')) * 100) / 100;
                } catch (err) {
                  itemdata.min = 0; // eliminate any old values
                  console.warn("Pool-min formula evaluation error:\n", [actor.name, item.name, itemdata.minForm]);
                }
                itemdata.hasmin = true;
              }
            }
          }
          if (itemdata.hasmax && itemdata.value > itemdata.max) itemdata.value = itemdata.max;
          if (itemdata.hasmin && itemdata.value < itemdata.min) itemdata.value = itemdata.min;
          itemdata.ratio = Math.trunc(itemdata.value / itemdata.max * 100);
          if (itemdata.hasmax && itemdata.hasmin)
            nextLayer.push(dynID);
        } else {
          //ignore items with no formula or whose formula has already been processed
          if (!itemdata.formula || Number.isNumeric(itemdata.formula) || itemdata.formula.startsWith("#") || (Number(itemdata.moddedformula) == itemdata.value && itemdata.moddedformula != "")) continue;
          const dynID = actor.slugify(item.name);
          if (previousLayer.includes(dynID)) continue;

          // the formula is a reference
          if (itemdata.formula.includes("@")) {
            for (const target of previousLayer) {
              // only need to process the terms once
              if (Number(itemdata.moddedformula) == itemdata.value && itemdata.moddedformula != "") continue;
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.formula.includes(target)) {
                const formData = actor._replaceData(itemdata.formula);
                try {
                  itemdata.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, '')) * 100) / 100;
                  itemdata.moddedvalue = itemdata.value; // default reference is now moddedvalue so set it now
                  itemdata.moddedformula = "" + itemdata.value; // store the result as a String
                } catch (err) {
                  // store the formula ready to be rolled
                  itemdata.moddedformula = formData.value;
                  itemdata.value = itemdata.moddedvalue = 0; // eliminate any old values
                  console.warn("Non-Modifier formula evaluation incomplete:\n", [actor.name, item.name, itemdata.moddedformula]);
                }
                // put this item into the next layer
                nextLayer.push(dynID);
              }
            }
          } else { // the formula might be a dice expression
            if (itemdata.formula.toLowerCase().includes("d")) {
              itemdata.moddedformula = itemdata.formula;
              itemdata.value = itemdata.moddedvalue = 0; // eliminate any old values
              console.debug("Encountered a Dice Expression:\n", [actor.name, item.name, itemdata.moddedformula]);
            }
          }
        }
      }
    }

    // process all modifiers again after the non-modifiers have been processed
    for (const item of actormods) {
      if (!item.system.inEffect) continue;
      for (const entry of item.system.entries) {
        actor.processModifierFormula(entry, item);
        if (activeModEntries.includes(entry) || entry.value == 0) continue;
        activeModEntries.push(entry);
      }
    }

    // calculate the modified value of the item
    for (const item of rollables) {
      const itemdata = item.system;
      // if there is no modified formula, make it the value
      if (itemdata.moddedformula == undefined) itemdata.moddedformula = "" + itemdata.value;
      switch (item.type) {
        case "Melee-Attack":
        case "Ranged-Attack": {
          itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "attack", activeModEntries);
          break;
        }
        case "Defence": {
          itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "defence", activeModEntries);
          break;
        }
        case "Rollable": {
          switch (itemdata.category) {
            case "technique":
            case "check": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "check", activeModEntries);
              break;
            }
            case "skill": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "skill", activeModEntries);
              break;
            }
            case "spell": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "spell", activeModEntries);
              break;
            }
          }
          break;
        }
      }
    }

    // DB calculations for Rolemaster-family games
    if (useRolemasterDefenses) {
      actordata.bs.value = (actordata.dynamic.declared_action) ? actordata.dynamic.declared_action.value : actordata.bs.value;
      actordata.defence = {
        armourType: actordata.dynamic.armor_type || {},
        meleeDB: actordata.dynamic.melee_db || {},
        missileDB: actordata.dynamic.missile_db || {},
        parry: actordata.dynamic.parry || {},
        edb: actordata.dynamic.edb || {}
      };
      for (const item of valuemods) {
        actordata.defence.meleeDB.system.shield = actordata.defence.missileDB.system.shield = 0;
        if (!item.system.inEffect) continue;
        if (item.system.group.includes("shield") || item.name.includes("Shield")) {
          for (const entry of item.system.entries) {
            if (entry.targets.includes("Melee")) actordata.defence.meleeDB.system.shield += entry.value;
            if (entry.targets.includes("Missile")) actordata.defence.missileDB.system.shield += entry.value;
          }
          break;
        }
      }
    }
    // this is the end of prepareDerivedData
  }

  /**
   * rolldata consists of:
   * - name: the name of the roll
   * - type: the type of roll (used for filtering modifiers)
   * - roll: the value being processed by this function
   * - modtype: if the type is "modlist" then this is the type of the roll
   * - id: the _id of the item being rolled, so we can access all it's raw data
   */
  async roll(rolldata) {
    let flavour = rolldata.flavour || "";
    const actor = rolldata.actor;
    const actordata = actor.system;
    game.settings.set("grpga", "currentActor", actor.id);

    if (rolldata.type == "tochat") { // send the notes to the chat and return
      const isplaintext = rolldata.roll.replace(/<[^>]*>?/gm, '') == rolldata.roll;
      ChatMessage.create({
        speaker: ChatMessage.getSpeaker({ actor: actor }),
        flavor: `<b>${rolldata.name}</b><hr>`,
        content: isplaintext ? rolldata.roll.replace(/(?:\r\n|\r|\n)/g, '<br>') : rolldata.roll,
      });
      return;
    }

    // get the modifiers
    const modList = actor.sheet.fetchRollModifiers(actor, { type: rolldata.type, modtype: rolldata.modtype, name: rolldata.name });
    // process the modifiers
    let modformula = "";
    flavour += (rolldata.type != "modlist") ? ((rolldata.type == "damage") ? ` [<b>${rolldata.roll} ${damageType}</b>]` : ` [<b>${rolldata.roll}</b>]`) : `<p class="chatmod">[<b>${rolldata.roll}</b>]: ${rolldata.name}<br>`;
    const hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      flavour += (rolldata.type != "modlist") ? `<p class="chatmod">` : "";
      for (const mod of modList) {
        sign = typeof mod.modifier === 'string' ? "" : mod.modifier > -1 ? "+" : "";
        modformula += `${sign}${mod.modifier}`;
        flavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    // add the item-id to the flavour
    const item = await actor.items.get(rolldata.id);
    flavour += `<span data-item-id="${item.id}"></span>`;

    let unmodified = 0;
    const rollValue = rolldata.roll;
    let rollType = "oe";
    // prepare a critroll for autocrit
    const critRoll = await new Roll("1d100").evaluate({ async: true });
    const isReaction = rolldata.type == "reaction";
    let attacktable = "";
    let crittable = "";
    let sectable = "";

    let openEndedRange = 5;
    const openEndedChange = actor.system.dynamic.openendedchange;
    if (openEndedChange) openEndedRange = openEndedChange.system.value;

    let formula = rolldata.type == "reaction" || rolldata.type == "modlist" ? rollValue : "1d100 + " + rollValue;

    // process the original roll
    let roll = await new Roll(formula + modformula).evaluate({ async: true });
    let firstRoll = roll.terms[0].total;

    switch (rolldata.type) {
      // Standard D100 used for straight table rolls (probably crit or failure)
      case "dodge": { // Standard from Various Rolls
        attacktable = item.system.notes;

        flavour += `<p>${game.i18n.localize(`local.rolls.d100roll`)}: [<b>${firstRoll}</b>] - `;
        flavour += `<a class="open-journal" data-table="${attacktable}">${game.i18n.localize(`local.rolls.table`)}</a></p>`;

        try {
          Journal._showEntry(game.journal.getName(attacktable).uuid, "text", true);
        } catch (err) {
          if (attacktable)
            ui.notifications.error(`There is no matching Journal Entry for ${attacktable}. Have you imported it from the compendium?`);
        }
        break;
      }
      // Toughness or Willpower Save Roll, open-ended (no crit)
      case "block": { // Open-Ended from Various Rolls
        attacktable = item.system.notes;

        // display the d100 roll
        flavour += `<p>${game.i18n.localize(`local.rolls.d100roll`)}: [<b>${firstRoll}</b>] - `;
        flavour += `<a class="open-journal" data-table="${attacktable}">${game.i18n.localize(`local.rolls.table`)}</a></p>`;

        const openEnded = await this.checkOpenEnded(roll, openEndedRange);
        if (openEnded) {
          roll = openEnded.roll;
          flavour += `<p class=${openEnded.explodeUp ? "critsuccess" : "critfail"}>${rollType.toUpperCase()} ${game.i18n.localize(`local.rolls.roll`)}: [<b>${roll.dice[1].total}</b>]</p>`;
        }
        break;
      }
      // Not sure which ones are only OEH
      case "parry": { // Open-Ended High from Various Rolls
        attacktable = item.system.notes;

        // display the d100 roll
        flavour += `<p>${game.i18n.localize(`local.rolls.d100roll`)}: [<b>${firstRoll}</b>] - `;
        flavour += `<a class="open-journal" data-table="${attacktable}">${game.i18n.localize(`local.rolls.table`)}</a></p>`;

        const openEnded = await this.checkOpenEnded(roll, openEndedRange, false); // OEH
        if (openEnded) {
          roll = openEnded.roll;
          flavour += `<p class=${openEnded.explodeUp ? "critsuccess" : "critfail"}>${rollType.toUpperCase()} ${game.i18n.localize(`local.rolls.roll`)}: [<b>${roll.dice[1].total}</b>]</p>`;
        }
        break;
      }
      case "reaction": { // For Melee or Missile Defensive Bonus "rolls"
        // For Melee or Missile Defensive Bonus "rolls"
        // the calculations are made in prepchardata so keep this here for output to chat only
        // FIXME: I don't think we have reaction rolls any more, they will be made under techniques
        break;
      }
      // A skill roll, open-ended (no crit)
      case "stat":
      case "skill": {
        let journal = item.name;
        attacktable = `${game.i18n.localize(`local.rolls.skill.mm`)}`;
        crittable = `${game.i18n.localize(`local.rolls.skill.sm`)}`;

        // display the d100 roll
        flavour += `<p>${game.i18n.localize(`local.rolls.d100roll`)}: [<b>${firstRoll}</b>] - `;
        flavour += `<a class="open-journal" data-table="${journal}">${game.i18n.localize(`local.rolls.skill.skill`)}</a>, `;
        flavour += `<a class="open-journal" data-table="${attacktable}">${attacktable}</a>, `;
        flavour += `<a class="open-journal" data-table="${crittable}">${crittable}</a></p>`;

        const openEnded = await this.checkOpenEnded(roll, openEndedRange);
        if (openEnded) {
          roll = openEnded.roll;
          flavour += `<p class=${openEnded.explodeUp ? "critsuccess" : "critfail"}>${rollType.toUpperCase()} ${game.i18n.localize(`local.rolls.roll`)}: [<b>${roll.dice[1].total}</b>]</p>`;;
        }
        break;
      }
      // A spell roll, open-ended (no crit)
      case "spell": {
        let journal = item.name.split(":")[1].trim();
        attacktable = "Spell Casting Table";

        // display the d100 roll
        flavour += `<p>${game.i18n.localize(`local.rolls.d100roll`)}: [<b>${firstRoll}</b>] - `;
        flavour += `<a class="open-journal" data-table="${journal}">${game.i18n.localize(`local.rolls.spell.lore`)}</a>, `;
        flavour += `<a class="open-journal" data-table="${attacktable}">${game.i18n.localize(`local.rolls.spell.sct`)}</a></p>`;

        const openEnded = await this.checkOpenEnded(roll, openEndedRange);
        if (openEnded) {
          roll = openEnded.roll;
          flavour += `<p class=${openEnded.explodeUp ? "critsuccess" : "critfail"}>${rollType.toUpperCase()} ${game.i18n.localize(`local.rolls.roll`)}: [<b>${roll.dice[1].total}</b>]</p>`;;
        }

        try {
          Journal._showEntry(game.journal.getName(attacktable).uuid, "text", true);
        } catch (err) {
          if (attacktable)
            ui.notifications.error(`There is no matching Journal Entry for ${attacktable}. Have you imported it from the compendium?`);
        }
        break;
      }
      // An attack roll, open-ended (crit)
      case "attack": { // Open-Ended
        unmodified = item.system.armourDiv;
        if (firstRoll <= unmodified) {
          // fumble has occurred
          // render roll and critRoll with fumble message then return
          flavour += `<p class="critfail">${game.i18n.localize(`local.rolls.d100roll`)}: [<b>${firstRoll} - ${game.i18n.localize(`local.rolls.critfail`)}</b>]`;
          switch (rolldata.name.split(":")[0]) {
            case "Blunt":
            case "Blades":
            case "Brawl":
            case "Polearms":
              flavour += ` <i class="fas fa-scroll rollmacro" data-category="dodge" data-target="Fumble (Me"></i></p>`;
              break;
            case "Ranged":
              flavour += ` <i class="fas fa-scroll rollmacro" data-category="dodge" data-target="Fumble"></i></p>`;
              break;
            default: // a spell attack
              flavour += ` <i class="fas fa-scroll rollmacro" data-category="dodge" data-target="Failure"></i></p>`;
              break;
          }
          if (actor.system.autoCrit) {
            flavour += `<p>${game.i18n.localize(`local.rolls.attack.d100critroll`)}: [<b>${critRoll.total}</b>]</p>`;
          }
          roll.toMessage({
            speaker: ChatMessage.getSpeaker({ actor: actor }),
            flavor: flavour,
          });
          actor.update({ ["data.gmod.value"]: 0 });
          actor.resetModVars(false);
          return;
        }

        attacktable = item.system.damage;
        crittable = item.system.damageType;
        sectable = "";
        let maxresult = item.type == "Melee-Attack" ? item.system.weight : item.system.accuracy;

        // display the d100 roll
        flavour += `<p>${game.i18n.localize(`local.rolls.d100roll`)}: [<b>${firstRoll}</b>] - `;
        flavour += `<a class="open-journal" data-table="${attacktable}">${game.i18n.localize(`local.rolls.attack.attack`)}</a></p>`;

        const openEnded = await this.checkOpenEnded(roll, openEndedRange);
        if (openEnded) {
          roll = openEnded.roll;
          flavour += `<p class=${openEnded.explodeUp ? "critsuccess" : "critfail"}>${rollType.toUpperCase()} ${game.i18n.localize(`local.rolls.roll`)}: [<b>${roll.dice[1].total}</b>]</p>`;;
        }

        // display the automated crit roll if desired
        if (actor.system.autoCrit) {
          flavour += `<p>${game.i18n.localize(`local.rolls.attack.d100critroll`)}: [<b>${critRoll.total}</b>] - `;
          flavour += `<a class="open-journal" data-table="${crittable}">${game.i18n.localize(`local.rolls.attack.critical`)}</a></p>`;
        }
        try {
          Journal._showEntry(game.journal.getName(attacktable).uuid, "text", true);
        } catch (err) {
          if (attacktable)
            ui.notifications.error(`There is no matching Journal Entry for ${attacktable}. Have you imported it from the compendium?`);
        }
        if (roll.total > maxresult) {
          flavour += `<p class="critfail">${game.i18n.localize(`local.rolls.attack.maxresult`)}: [<b>${maxresult}</b>]</p>`;
        }
        break;
      }
      case "modlist": { // all the work has been done above. Maybe move it in here to tidy up?
        flavour += `<hr><p>${roll.result} = <b>${roll.total}</b></p>`;
        new Dialog({
          title: actor.name,
          content: flavour,
          buttons: {
            close: {
              label: "Close"
            },
          },
          default: "close"
        }).render(true)
        return;
      }
      default: { // the D100 roll and skill value/level/bonus
        // unsupported roll types
        ui.notifications.info(`The ${rolldata.type} roll is not yet supported.`);
        break;
      }
    }

    // prepare the flavor in advance based on which type of die is in use.
    roll.toMessage({
      speaker: ChatMessage.getSpeaker({ actor: actor }),
      flavor: flavour,
    });
    actor.update({ ["data.gmod.value"]: 0 });
    actor.resetModVars(false);
  }

  async checkOpenEnded(roll, openEndedRange, oe = true) {
    const result = {};
    const firstRoll = roll.terms[0].total;
    const topEnd = 100 - openEndedRange;
    // does it explode
    if (firstRoll <= openEndedRange && oe) {// explodes downward
      result.explodeUp = false;
    } else if (firstRoll > topEnd) {// explodes upward
      result.explodeUp = true;
    } else {
      return null;
    }
    result.roll = roll.clone();
    result.roll.terms[0] = new Die({ number: 1, faces: 100, results: [{ result: firstRoll, active: true }] });
    result.roll.terms.push(new OperatorTerm({ operator: (result.explodeUp) ? '+' : '-' }));
    result.roll.terms.push(new Die({ number: 1, faces: 100, modifiers: ['x>' + topEnd] }));
    await result.roll.evaluate({ async: true });
    result.roll._formula = result.roll.formula;
    return result;
  }

}