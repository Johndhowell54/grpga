import { system } from "../config.js";

/**
 * A utility class defining behaviour for ruleset-specific actors
 */
export class Actor3D6 {

  constructor(actor) {
    this.actor = actor;
    this.compendiumName = "Base Items";
  }

  prepareBaseData() {
    const actordata = this.actor.system;
  }

  prepareAdditionalData() {
    const actor = this.actor;
    const actordata = actor.system;
    if (CONFIG.system.testMode) console.debug("entering prepareDerivedData()\n", [actor, actordata]);

    // for counting the ranks stored in Variable Items for some systems
    let actorranks = {};
    let rankitems = [];

    // group the items for efficient iteration
    const actormods = [];
    const nonmods = [];
    const attackmods = [];
    const primods = [];
    const valuemods = [];
    const rollables = [];

    // store references to primary attribute and pool items
    let previousLayer = [];
    let nextLayer = [];

    // write the items into the model
    for (const item of actordata.items) {
      const itemdata = item.system;
      let itemID = actor.slugify(item.name);
      // collect non-modifier items with formula
      if (itemdata.formula) nonmods.push(item);

      switch (item.type) {
        case "Primary-Attribute": {
          let itemID = actor.slugify(itemdata.abbr);
          if (itemID) {
            // only process primaries with abbreviations
            itemdata.moddedvalue = Number(itemdata.attr);
            itemdata.value = itemdata.moddedvalue;
            // store a reference in the data structure
            actordata.dynamic[itemID] = item;
            // add this to the processed references
            nextLayer.push(itemID);
          }
          break;
        }
        case "Trait": {
          // store a reference in the data structure
          actordata.traits[itemID] = item;
          switch (item.name) {
            case "Keen Senses":
            case "Night Sight":
            case "Dark Sight":
            case "Darkvision":
            case "Star Sight": {
              actordata.dynamic.visionType = item.name;
            }
          }
          break;
        }
        case "Melee-Attack":
        case "Ranged-Attack":
        case "Advantage":
        case "Power":
        case "Defence":
        case "Rollable": {
          // If the formula is numeric, store it in the array of valid references
          if (Number.isNumeric(itemdata.formula)) {
            nextLayer.push(itemID);
            itemdata.value = itemdata.moddedvalue = Number(itemdata.formula);
          } else {
            itemdata.moddedformula = "";
            itemdata.value = itemdata.moddedvalue = 0;
          }
          // store a reference in the data structure
          actordata.dynamic[itemID] = item;
          // store a reference in a temporary array
          if (itemdata.category == "value") values.push(item);
          else if (itemdata.category == "combatval") values.push(item);
          else if (itemdata.category == "defence") values.push(item);
          else rollables.push(item);
          break;
        }
        case "Hit-Location": {
          break;
        }
        case "Pool": {
          itemID = actor.slugify(itemdata.abbr);
          if (itemID) {
            itemdata.moddedvalue = itemdata.value;
            nextLayer.push(itemID);
            // store a reference in the data structure for resources
            actordata.tracked[itemID] = item;
            nonmods.push(item);
          }
          break;
        }
        case "Variable": {
          // If the formula is numeric, store it in the array of valid references
          if (Number.isNumeric(itemdata.formula)) {
            itemdata.value = itemdata.moddedvalue = Number(itemdata.formula);
            nextLayer.push(itemID);
          }
          // store a reference in the data structure
          actordata.dynamic[itemID] = item;
          break;
        }
        case "Modifier": {
          itemdata.tempName = item.name;
          // store a reference in the data structure array of modifiers
          actordata.modifiers[itemID] = item;
          // if it modifies ability stats, add it to a temporary array
          if (itemdata.primary) primods.push(item);
          if (itemdata.attack) attackmods.push(item);
          if (itemdata.defence || itemdata.combatval || itemdata.value) valuemods.push(item);
          actormods.push(item);
          break;
        }
        default: { // Traits that define a vision type for the actor
          actordata.dynamic[itemID] = item;
        }
      }
    }

    // Primary Attribute modifiers first
    for (const item of primods) {
      const itemdata = item.system;
      // the modifier has a primod and is in effect
      if (itemdata.primary && itemdata.inEffect) {
        for (const entry of itemdata.entries) {
          // this entry is a primod
          if (entry.category == "primary") {
            // process primods formulae before the mods are applied
            actor.processModifierFormula(entry, item);
            const cases = entry.targets.split(",").map(word => word.trim().toLowerCase());
            for (const target of cases) {
              // we have found the target in dynamic
              if (actordata.dynamic[target]?.type == "Primary-Attribute") {
                const targetdata = actordata.dynamic[target].system;
                targetdata.value = targetdata.moddedvalue = Number(targetdata.moddedvalue) + entry.value;
              }
            }
          }
        }
      }
    }

    // this will store all valid modifier entries for later processing efficiency
    const activeModEntries = [];

    // calculate the remaining formulae one layer at a time
    while (nextLayer.length !== 0) {
      previousLayer = nextLayer;
      nextLayer = [];

      for (const item of nonmods) {
        const itemdata = item.system;

        if (item.type == "Pool") { // new pool formula functionality
          const dynID = actor.slugify(itemdata.abbr);
          //ignore numeric formulae
          if (itemdata.hasmax && itemdata.hasmin) continue;
          if (!itemdata.hasmax) { // formula has not yet been calculated
            for (const target of previousLayer) {
              // only need to process the terms once
              if (itemdata.hasmax) continue;
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.maxForm.includes(target)) {
                const formData = actor._replaceData(itemdata.maxForm);
                try {
                  itemdata.max = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, '')) * 100) / 100;
                } catch (err) {
                  itemdata.max = 0; // eliminate any old values
                  console.warn("Pool-max formula evaluation error:\n", [actor.name, item.name, itemdata.maxForm]);
                }
                itemdata.hasmax = true;
              }
            }
          }
          if (!itemdata.hasmin) { // formula has not yet been calculated
            for (const target of previousLayer) {
              // only need to process the terms once
              if (itemdata.hasmin) continue;
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.minForm.includes(target)) {
                const formData = actor._replaceData(itemdata.minForm);
                try {
                  itemdata.min = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, '')) * 100) / 100;
                } catch (err) {
                  itemdata.min = 0; // eliminate any old values
                  console.warn("Pool-min formula evaluation error:\n", [actor.name, item.name, itemdata.minForm]);
                }
                itemdata.hasmin = true;
              }
            }
          }
          if (itemdata.hasmax && itemdata.value > itemdata.max) itemdata.value = itemdata.max;
          if (itemdata.hasmin && itemdata.value < itemdata.min) itemdata.value = itemdata.min;
          itemdata.ratio = Math.trunc(itemdata.value / itemdata.max * 100);
          if (itemdata.hasmax && itemdata.hasmin)
            nextLayer.push(dynID);
        } else {
          //ignore items with no formula or whose formula has already been processed
          if (!itemdata.formula || Number.isNumeric(itemdata.formula) || itemdata.formula.startsWith("#") || (Number(itemdata.moddedformula) == itemdata.value && itemdata.moddedformula != "")) continue;
          const dynID = actor.slugify(item.name);
          if (previousLayer.includes(dynID)) continue;

          // the formula is a reference
          if (itemdata.formula.includes("@")) {
            for (const target of previousLayer) {
              // only need to process the terms once
              if (Number(itemdata.moddedformula) == itemdata.value && itemdata.moddedformula != "") continue;
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.formula.includes(target)) {
                const formData = actor._replaceData(itemdata.formula);
                try {
                  itemdata.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, '')) * 100) / 100;
                  itemdata.moddedvalue = itemdata.value; // default reference is now moddedvalue so set it now
                  itemdata.moddedformula = "" + itemdata.value; // store the result as a String
                } catch (err) {
                  // store the formula ready to be rolled
                  itemdata.moddedformula = formData.value;
                  itemdata.value = itemdata.moddedvalue = 0; // eliminate any old values
                  console.warn("Non-Modifier formula evaluation incomplete:\n", [actor.name, item.name, itemdata.moddedformula]);
                }
                // put this item into the next layer
                nextLayer.push(dynID);
              }
            }
          } else { // the formula might be a dice expression
            if (itemdata.formula.toLowerCase().includes("d")) {
              itemdata.moddedformula = itemdata.formula;
              itemdata.value = itemdata.moddedvalue = 0; // eliminate any old values
              console.debug("Encountered a Dice Expression:\n", [actor.name, item.name, itemdata.moddedformula]);
            }
          }
        }
      }
    }

    // process all modifiers again after the non-modifiers have been processed
    for (const item of actormods) {
      if (!item.system.inEffect) continue;
      for (const entry of item.system.entries) {
        actor.processModifierFormula(entry, item);
        if (activeModEntries.includes(entry) || entry.value == 0) continue;
        activeModEntries.push(entry);
      }
    }

    // calculate the modified value of the item
    for (const item of rollables) {
      const itemdata = item.system;
      // if there is no modified formula, make it the value
      if (itemdata.moddedformula == undefined) itemdata.moddedformula = "" + itemdata.value;
      switch (item.type) {
        case "Melee-Attack":
        case "Ranged-Attack": {
          itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "attack", activeModEntries);
          break;
        }
        case "Defence": {
          itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "defence", activeModEntries);
          break;
        }
        case "Rollable": {
          switch (itemdata.category) {
            case "check": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "check", activeModEntries);
              break;
            }
            case "technique":
            case "skill": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "skill", activeModEntries);
              break;
            }
            case "rms":
            case "spell": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "spell", activeModEntries);
              break;
            }
          }
          break;
        }
      }
    }

    // this is the end of prepareDerivedData
  }

  /**
   * rolldata consists of:
   * - name: the name of the roll
   * - type: the type of roll (used for filtering modifiers)
   * - roll: the value being processed by this function
   * - modtype: if the type is "modlist" then this is the type of the roll
   * - id: the _id of the item being rolled, so we can access all it's raw data
   */
  async roll(rolldata) {

    let flavour = rolldata.flavour || "";
    const actor = rolldata.actor;
    const actordata = actor.system;
    const item = await actor.items.get(rolldata.id);

    const useFAHR = actordata.useFAHR;
    const hideTotal = true;
    let isDamageOrReaction = false;
    let isModList = false;

    var formula = "";
    switch (rolldata.type) {
      case "modlist": // this will render a dialog with the modifiers, not a chat message
        formula = rolldata.roll;
        isModList = true;
        break;
      case "damage": {
        formula = rolldata.roll;
        flavour += ` [<b>${rolldata.roll} ${item.system.damageType} (/${item.system.armourDiv})</b>]`;
        isDamageOrReaction = true;
        break;
      };
      case "check": {
        if (item.name == "Reaction") {
          formula = "3d6";
          isDamageOrReaction = true;
        } else {
          formula = rolldata.roll;
          flavour += ` [<b>${rolldata.roll}</b>]`;
        }
        break;
      };
      case "attack": {
        formula = rolldata.roll;
        const attack = item;
        const attackdata = attack.system;
        const ammopool = actordata.tracked[attackdata.shots?.toLowerCase()];
        if (ammopool) {
          // has ammunition
          const ammoused = Math.min(attackdata.rof, ammopool.system.value);
          // Decrement Pool by the Rate of Fire value
          await actor.updateEmbeddedDocuments("Item", [{
            _id: ammopool._id,
            system: { value: Math.max(0, ammopool.system.value - ammoused) }
          }]);
          // Get remaining ammo count post-attack to display in output
          const ammo_left = ammopool.system.value;
          flavour += `<hr><b>Range:</b> ${attackdata.range}<br>`;
          if (ammoused == 0) {
            flavour += `<b>Ammo used:</b> ${ammoused} <span class="critfail">Click! Attack failed! Reload!</span><br>`;
          } else if (ammo_left == 0) {
            flavour += `<b>Ammo used:</b> ${ammoused} (Remaining: ${ammo_left}) <span class="critfail">Reload!</span><br>`;
          } else {
            flavour += `<b>Ammo used:</b> ${ammoused} (Remaining: ${ammo_left})<br>`;
          }
          flavour += `<b>Damage:</b><span class="rollable" data-item-id="{{item._id}}" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage"> ${attackdata.damage} - ${attackdata.damageType}</span><hr><b>Strike Modifiers:</b><br>Base:  `;
        } else {
          if (attack.type == "Ranged-Attack") {
            flavour += `<hr><b>Range:</b> ${attackdata.range}<br><b>Damage:</b><span class="rollable" data-item-id="{{item._id}}" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage"> ${attackdata.damage} - ${attackdata.damageType}</span><hr><b>Strike Modifiers:</b><br>Base:  `;
          } else {
            // a melee attack
            flavour += `<hr><b>Damage:</b><span class="rollable" data-item-id="${item._id}" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage"> ${attackdata.damage} - ${attackdata.damageType}</span><hr><b>Strike Modifiers:</b><br>Base:  `;
          }
        }
        flavour += ` [<b>${rolldata.roll}</b>]`;
        break;
      }
      default: {
        formula = rolldata.roll;
        flavour += ` [<b>${rolldata.roll}</b>]`;
        break;
      }
    }
    // add the item-id to the flavour
    flavour += `<span data-item-id="${item.id}"></span>`;

    const modList = actor.sheet.fetchRollModifiers(actor, { type: rolldata.type, modtype: rolldata.modtype, name: rolldata.name });

    var hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      flavour += `<p class="chatmod">`;
      for (const mod of modList) {
        sign = typeof mod.modifier === 'string' ? "" : mod.modifier > -1 ? "+" : "";
        formula += `${sign}${mod.modifier}`;
        flavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    const target = await new Roll(formula).evaluate({ async: true });

    if (isModList) { // render the modlist dialog and return
      flavour += `<hr><p>${target.result} = <b>${target.total}</b></p>`;
      new Dialog({
        title: actor.name,
        content: flavour,
        buttons: {
          close: {
            label: "Close"
          },
        },
        default: "close"
      }).render(true)
      return;
    }

    if (hasMods || isDamageOrReaction) {
      flavour += `<hr><p class="">${target.formula} = [<b>${target.total}</b>]`;
    }

    const r3d6 = await new Roll("3d6").evaluate({ async: true });

    // a Success roll
    if (!isDamageOrReaction) {
      const measureofsuccess = target.total - r3d6.total;
      let success = measureofsuccess > -1;
      let critfail = false;
      let critsuccess = false;
      switch (r3d6.total) {
        case 3:
        case 4: {
          critsuccess = true;
          break;
        }
        case 5:
        case 6: {
          critsuccess = measureofsuccess > 9;
          break;
        }
        case 17: {
          success = false;
          critfail = measureofsuccess < -1;
          break;
        }
        case 18: {
          success = !(critfail = true);
          break;
        }
      }
      if (!hasMods) {
        flavour += `<hr><p class="">`;
      } else {
        flavour += `<br>`;
      }
      flavour += `3D6 Roll: [<b>${r3d6.total}</b>] <i class="fas fa-arrow-right"></i> <span class="`;
      if (critsuccess) flavour += `critsuccess">Critical Success`;
      else if (critfail) flavour += `critfail">Critical Failure`;
      else if (success) flavour += `success">Success by ${measureofsuccess}`;
      else flavour += `failure">Failure by ${-measureofsuccess}`;
      flavour += `</span></p>`
    }

    target.toMessage({
      speaker: ChatMessage.getSpeaker({ actor: actor }),
      flavor: flavour,
      flags: { hideMessageContent: hideTotal }
    });
    actor.resetModVars(false);
    actor.update({ ["system.gmod.value"]: 0 });
  }
}