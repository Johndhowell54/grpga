import { system } from "../config.js";
import { baseActorSheet } from "./baseActor-sheet.js";
import { xmlToJson, parseXML, json2xml, xml2json } from "../utility.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {baseActorSheet}
 */
export class ActorMnMSheet extends baseActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["mnm", "grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorMnM-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-nav", contentSelector: ".sheet-body", initial: "skill" }]
    });
  }

  /** @override */
  getData(options) {
    if (CONFIG.system.testMode) console.debug("entering getData() in ActorMnM-sheet");

    const context = super.getData();
    // My shortcuts
    const actordata = context.system;
    const traits = actordata.traits;
    const dynamic = actordata.dynamic;
    const tracked = actordata.tracked;
    const headinfo = actordata.headerinfo = {};

    headinfo.level = dynamic.level;
    headinfo.perception = dynamic.perception;
    headinfo.initiative = dynamic.initiative;
    headinfo.dodge = dynamic.dodge;
    headinfo.xp = dynamic.xp;

    for (let item of actordata.advantages) {
      switch (item?.name) {
        case "Character Class": headinfo.characterclass = item; break;
        case "Specific Information": headinfo.specificinfo = item; break;
        case "Additional Information": headinfo.additionalinfo = item; break;
        case "Alignment": headinfo.alignment = item; break;
        case "Secret Identity": headinfo.secretname = item; break;
      }
    }
    
    actordata.primaryattributes = [];
    for (let varname of system[context.mode].primaryAttributes) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.primaryattributes.push(dynamic[varname]);
    }

    // group and sort skills correctly for display
    let tempskills = [...actordata.skills];
    let tempspells = [...actordata.spells];
    actordata.skills = [];
    actordata.techniques = [];
    for (let item of tempskills) {
      if (item.system.category == "technique") {
        actordata.techniques.push(item);
      } else {
        item.system.stepvalue = item.system.moddedvalue - item.system.value;
        actordata.skills.push(item);
      }
    }
    actordata.rms = [];
    for (let item of tempspells) {
      if (item.system.category == "rms") {
        actordata.rms.push(item);
      } else {
        actordata.skills.push(item);
      }
    }
    for (let item of actordata.checks) {
      actordata.skills.push(item);
    }
    delete actordata.spells;
    delete actordata.checks;
    actordata.skills = this.sort(actordata.skills);

    // group and sort defences correctly for display
    let tempdefences = [...actordata.defences];
    actordata.defences = [];
    actordata.blocks = [];
    for (const item of tempdefences) {
      if (item.system.category == "block") {
        actordata.blocks.push(item);
      } else {
        item.system.stepvalue = item.system.moddedvalue - item.system.value;
        actordata.defences.push(item);
      }
    }
    actordata.attackVariables = [];
    for (let varname of system.mnm.attackVariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.attackVariables.push(dynamic[varname]);
      else if (tracked[varname]) actordata.attackVariables.push(tracked[varname]);
    }
    actordata.defenceVariables = [];
    for (let varname of system.mnm.defenceVariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.defenceVariables.push(dynamic[varname]);
      else if (tracked[varname]) actordata.defenceVariables.push(tracked[varname]);
    }
    actordata.skillVariables = [];
    for (let varname of system.mnm.skillVariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.skillVariables.push(dynamic[varname]);
      else if (tracked[varname]) actordata.skillVariables.push(tracked[varname]);
    }
    actordata.spellVariables = [];
    for (let varname of system.mnm.spellVariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.spellVariables.push(dynamic[varname]);
      else if (tracked[varname]) actordata.spellVariables.push(tracked[varname]);
    }

    // group and sort skill mods
    actordata.checkSkillSpellMods = this.sort(Array.from(new Set(actordata.checkmods.concat(actordata.skillmods, actordata.spellmods))));

    delete actordata.skillmods;
    delete actordata.checkmods;
    delete actordata.spellmods;

    // check to see if specific modifiers are toggled so we can set the flags
    // the modifier must affect Variable Formulae, D20 skills or D100 skills to work as a toggle (a modifier of zero is ok)
    // these were designed for D6 Pool ruleset but could be used by others
    for (let item of actordata.checkSkillSpellMods) {
      switch (item?.name) {
        case "Rushed": this.actor.setFlag("grpga", "rushed", item.system.inEffect); break;
        case "Using Edge": this.actor.setFlag("grpga", "usingedge", item.system.inEffect); break;
      }
    }

    return context;
  }

  sort(items) {
    return items.sort((a, b) => {
      if (a.sort < b.sort) return -1;
      if (a.sort > b.sort) return 1;
      return 0;
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.importText').click(this._onImportTextData.bind(this));
    html.find('.importXML').click(this._onImportXMLData.bind(this));
    html.find('.importHeroLab').click(this._onImportHeroLabData.bind(this));
  }

  async _processJSONData(itemdata) {
    const chardata = itemdata.document.public.character;

    const updates = [];

    // attributes
    let elements = chardata.attributes?.attribute || [];
    let elms = elements instanceof Array ? elements : [elements];
    for (let e of elms) {
      let itemdata = {
        name: e.name,
        type: "Primary-Attribute",
        system: {
          chartype: "CharacterMnM",
          attr: Number(e.modified),
          cost: Number(e.cost.value)
        }
      }
      switch (itemdata.name) {
        case "Strength": itemdata.system.abbr = "STR"; break;
        case "Stamina": itemdata.system.abbr = "STA"; break;
        case "Agility": itemdata.system.abbr = "AGL"; break;
        case "Dexterity": itemdata.system.abbr = "DEX"; break;
        case "Fighting": itemdata.system.abbr = "FGT"; break;
        case "Intellect": itemdata.system.abbr = "INT"; break;
        case "Awareness": itemdata.system.abbr = "AWE"; break;
        case "Presence": itemdata.system.abbr = "PRE"; break;
      }
      updates.push(itemdata);
    }

    // Power Points
    let element = chardata.powerpoints;
    let edata = {
      name: "XP",
      type: "Rollable",
      system: {
        chartype: "CharacterMnM",
        category: "rms",
        formula: element.value || "0",
        group: "Reference",
        notes: `<h3>${element.text}</h3>`
      }
    }
    updates.push(edata);

    // Power Level
    element = chardata.powerlevel;
    edata = {
      name: "Level",
      type: "Rollable",
      system: {
        chartype: "CharacterMnM",
        category: "rms",
        formula: element.value || "0",
        group: "Reference",
        notes: `<h3>${element.text}</h3>`
      }
    }
    updates.push(edata);

    // Initiative
    element = chardata.initiative;
    edata = {
      name: "Initiative",
      type: "Defence",
      system: {
        chartype: "CharacterMnM",
        category: "dodge",
        formula: element.total || "0",
        group: "Defences",
        notes: `<h3>Imported Initiative Value</h3>`
      }
    }
    updates.push(edata);

    // Header Items
    elms = ["Character Class", "Specific Information", "Additional Information", "Alignment", "Secret Identity"];
    for (let e of elms) {
      let itemdata = {
        name: e,
        type: "Trait",
        system: {
          chartype: "CharacterMnM",
          category: "advantage",
          group: "Header Info",
          notes: ""
        }
      }
      updates.push(itemdata);
    }

    // Attacks
    elements = chardata.attacks?.attack || [];
    elms = elements instanceof Array ? elements : [elements];
    for (let e of elms) {
      let itemdata = {}
      if (e.description.includes("ranged")) {
        itemdata = {
          name: e.name,
          type: "Ranged-Attack",
          system: {
            chartype: "CharacterMnM",
            group: "Attacks",
            formula: e.attack || "0",
            damage: e.dc || "DC",
            damageType: e.descriptor || "Dmg Type",
            armourDiv: e.crit || 20,
            minST: "",
            range: "",
            notes: `<h3>${e.name}</h3><hr><p>${e.description}</p>`
          }
        }
      } else {
        itemdata = {
          name: e.name,
          type: "Melee-Attack",
          system: {
            chartype: "CharacterMnM",
            group: "Attacks",
            formula: e.attack || "0",
            damage: e.dc || "DC",
            damageType: e.descriptor || "Dmg Type",
            armourDiv: e.crit || 20,
            minST: "",
            reach: "",
            notes: `<h3>${e.name}</h3><hr><p>${e.description}</p>`
          }
        }
      }
      updates.push(itemdata);
    }

    // advantages
    elements = chardata.advantages?.advantage || [];
    elms = elements instanceof Array ? elements : [elements];
    for (let e of elms) {
      let itemdata = {
        name: e.name,
        type: "Advantage",
        system: {
          chartype: "CharacterMnM",
          category: e.categorytext,
          ppcost: Number(e.cost.value),
          formula: e.ranks || "0",
          group: "Advantage",
          notes: `<h3>${e.categorytext}</h3><hr><p>${e.description}</p>`
        }
      }
      updates.push(itemdata);
    }

    // powers
    elements = chardata.powers?.power || [];
    elms = elements instanceof Array ? elements : [elements];
    for (let e of elms) {
      let itemdata = {
        name: e.name,
        type: "Power",
        system: {
          chartype: "CharacterMnM",
          category: "Power",
          ppcost: Number(e.cost.value),
          formula: e.ranks || "0",
          group: "Power",
          notes: `<hr><p>${e.description}</p><p>${e.summary}</p><p>${e.cost.text}</p>`
        }
      }
      updates.push(itemdata);
      // Check for alternate powers
      if (e.alternatepowers) {
        let altpowers = e.alternatepowers.power instanceof Array ? e.alternatepowers.power : [e.alternatepowers.power];
        for (let alt of altpowers) {
          let itemdata = {
            name: alt.name,
            type: "Power",
            system: {
              chartype: "CharacterMnM",
              category: "Power",
              ppcost: Number(alt.cost.value),
              formula: alt.ranks || "0",
              group: "Alternate Power",
              notes: `<hr>Under: ${e.name}<hr><p>${alt.description}</p><p>${alt.summary}</p><p>${alt.cost.text}</p>`
            }
          }
          updates.push(itemdata);
        }
      }
      // Check for other powers
      if (e.otherpowers) {
        let othpowers = e.otherpowers.power instanceof Array ? e.otherpowers.power : [e.otherpowers.power];
        for (let oth of othpowers) {
          let itemdata = {
            name: oth.name,
            type: "Power",
            system: {
              chartype: "CharacterMnM",
              category: "Power",
              ppcost: Number(oth.cost.value),
              formula: oth.ranks || "0",
              group: "Other Power",
              notes: `<hr>Under: ${e.name}<hr><p>${oth.description}</p><p>${oth.summary}</p><p>${oth.cost.text}</p>`
            }
          }
          updates.push(itemdata);
        }
      }
    }

    // complications
    elements = chardata.complications?.complication || [];
    elms = elements instanceof Array ? elements : [elements];
    for (let e of elms) {
      let itemdata = {
        name: e.name,
        type: "Trait",
        system: {
          chartype: "CharacterMnM",
          category: "disadvantage",
          notes: `<hr><p>${e.description}</p>`
        }
      }
      updates.push(itemdata);
    }
    // languages
    elements = chardata.languages?.language || {};
    elms = elements instanceof Array ? elements : [elements];
    for (let e of elms) {
      let itemdata = {
        name: `Language: ${e.name}`,
        type: "Trait",
        system: {
          chartype: "CharacterMnM",
          category: "disadvantage",
          notes: ""
        }
      }
      updates.push(itemdata);
    }
    // defenses
    elements = chardata.defenses?.defense || [];
    elms = elements instanceof Array ? elements : [elements];
    let skillrankdata = {
      name: "Ranks: Defenses",
      type: "Variable",
      system: {
        chartype: "CharacterMnM",
        entries: {
          0: {
            value: 0,
            formula: "",
            label: ""
          }
        }
      }
    }
    let nextIndex = 0;
    for (let e of elms) {
      skillrankdata.system.entries[nextIndex++] = {
        formula: Number(e.modified),
        label: e.name
      }
      let skilldata = {
        name: e.name,
        type: "Defence",
        system: {
          chartype: "CharacterMnM",
          category: "dodge",
          formula: e.modified,
          group: "Defences",
          impervious: Number(e.impervious),
          cost: Number(e.cost.value),
          abbr: e.abbr
        }
      }
      updates.push(skilldata);
    }
    skillrankdata.system.entries[nextIndex++] = {
      formula: chardata.initiative.total,
      label: "Initiative"
    }
    updates.push(skillrankdata);

    // skills
    elements = chardata.skills?.skill || [];
    elms = elements instanceof Array ? elements : [elements];
    skillrankdata = {
      name: "Ranks: Skills",
      type: "Variable",
      system: {
        chartype: "CharacterMnM",
        entries: {
          0: {
            value: 0,
            formula: "",
            label: ""
          }
        }
      }
    }
    nextIndex = 0;
    for (let e of elms) {
      let trainedonly = e.trainedonly == "yes";
      skillrankdata.system.entries[nextIndex++] = {
        formula: (e.value != "-") ? e.value : "-99",
        label: e.name
      }
      let skilldata = {
        name: e.name,
        type: "Rollable",
        system: {
          chartype: "CharacterMnM",
          category: "skill",
          formula: (e.value != "-") ? e.value : "-99",
          trainedonly: trainedonly,
          cost: Number(e.cost.value),
          notes: e.description
        }
      }
      updates.push(skilldata);
    }
    updates.push(skillrankdata);
    await this._updateOrCreateItems(updates);

    // personal and other basic data
    const persdata = {
      age: chardata.personal?.age || "Unk",
      gender: chardata.personal?.gender || "Unk",
      hair: chardata.personal?.hair || "Unk",
      eyes: chardata.personal?.eyes || "Unk",
      description: chardata.personal?.description || "Unk",
      height: chardata.personal?.charheight.text || "Unk",
      weight: chardata.personal?.charweight.text || "Unk"
    }
    await this.actor.update({
      'name': chardata.name,
      'prototypeToken.name': chardata.name,
      'prototypeToken.actorLink': true,
      'prototypeToken.displayBars': 40,
      'prototypeToken.displayName': 50,
      'prototypeToken.disposition': chardata.relationship == "ally" ? 1 : -1,
      'prototypeToken.sight.enabled': true,
      'system.personal': persdata
    });
  }

  /**
   * For imported and augmented data, detemine if the item exists and:
   * - if so, update the item
   * - if not, create the item
   */
  async _updateOrCreateItems(updates) {
    // make sure it is an array
    const arrayOfData = updates instanceof Array ? updates : [updates];

    const updateItems = [];
    const createItems = [];
    for (const itemdata of arrayOfData) {
      const item = this.actor.system.items.find(i => i.name === itemdata.name && i.type == itemdata.type);
      if (item) {
        itemdata._id = item.id;
        updateItems.push(itemdata);
      } else {
        createItems.push(itemdata);
      }
    }
    await this.actor.updateEmbeddedDocuments("Item", updateItems);
    await this.actor.createEmbeddedDocuments('Item', createItems);
  }

  /**
   * An XML data importer needs:
   * - to read the XML file from the itemscript field
   * - an xml to json converter
   * - a json reader to convert to GRPGA Items
   */
  async _onImportXMLData(event) {
    event.preventDefault();
    let temp = this.actor.system.itemscript.replace(/&quot;/g, '#quot;');
    if (temp[0] == "\"") { // remove the wrapping doublequotes
      temp = temp.substr(1, temp.length - 2);
    }
    let itemdata = JSON.parse(xml2json(parseXML(temp), "\t"));
    console.warn("Imported JSON character data\n", itemdata.document.public.character);
    await this.actor.update({
      'system.bm.sprint': itemdata.document.public.character,
      'system.itemscript': ""
    });
    const items = await this.actor.items.contents.reduce((arr, c) => {
      arr.push(c.id);
      return arr;
    }, []);
    await this.actor.deleteEmbeddedDocuments("Item", items);
    this._processJSONData(itemdata);
  };

  /**
   * A Hero Lab file importer needs:
   * - a file picker to select the .por file
   * - an unzipper to read the xml file in the archive
   * - an xml to json converter
   * - a json reader to convert to GRPGA Items
   */
  async _onImportHeroLabData(event) { };

  async _onImportTextData(event) {
    event.preventDefault();
    let temp = this.actor.system.itemscript;
    if (temp[0] == "\"") { // remove the wrapping doublequotes
      temp = temp.substr(1, temp.length - 2);
    }
    const scriptdata = temp.split(/\r?\n/).map(word => word.trim());

    let biography = this.actor.system.biography;

    let ammoIndex = 0;
    for (let entry of scriptdata) {
      let regex = new RegExp(/([^:]+):([^\r|\n]+)/gi);
      const line = regex.exec(entry);

      switch (line[1].trim()) {
        case "Name": {
          // Name: {{name}}
          await this.actor.update({ 'name': line[2].trim() });
          break;
        }
        case "Attribute": {
          // Attribute: {{name}} ### {{abbr || slugify(name)}} ### {{attr || 0}} ### {{sort || 0}} ### {{notes || empty}}
          // Attribute: Intelligence Quotient ### IQ ### 12 ### 1 ### Your ability to reason
          let itemdata = {
            type: "Primary-Attribute",
            system: {
              chartype: "CharacterMnM",
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.abbr = result[1] || this.actor.slugify(result[0]);
          itemdata.system.attr = Number(result[2]) || 0;
          itemdata.system.sort = Number(result[3]) || 50;
          itemdata.system.notes = result[4] || "";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Skill": {
          // Skill: {{name}} ### {{"VarForm" || "D20" || "D100"}} ### {{formula || "0"}} ### {{sort || 50}} ### {{notes || empty}}
          // Skill: Athletics ### D100 ### 45 ### 3 ### Training in vigorous exertion for competition
          let itemdata = {
            type: "Rollable",
            system: {
              chartype: "CharacterMnM"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          if (!result[1]) {
            itemdata.system.category = "spell";
          } else {
            switch (result[1].toLowerCase()) {
              case "varform":
                itemdata.system.category = "check";
                break;
              case "d20":
                itemdata.system.category = "skill";
                break;
              default:
                itemdata.system.category = "spell";
            }
          }
          itemdata.system.formula = result[2] || "0";
          itemdata.system.sort = Number(result[3]) || 50;
          itemdata.system.notes = result[4] || "";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Information": {
          // Information: {{name}} ### {{formula || "0"}} ### {{sort || 50}} ### {{notes || empty}}
          let itemdata = {
            type: "Rollable",
            system: {
              chartype: "CharacterMnM",
              category: "technique"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = result[1] || "0";
          itemdata.system.sort = Number(result[2]) || 50;
          itemdata.system.notes = result[3] || "You should be putting html formatted content here to be displayed in the chat log.";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Reference": {
          // Reference: {{name}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let itemdata = {
            type: "Rollable",
            system: {
              chartype: "CharacterMnM",
              category: "rms"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = result[1] || "0";
          itemdata.system.sort = Number(result[2]) || 50;
          itemdata.system.notes = result[3] || "";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Defence": {
          // Defence: {{name}} ### {{"D20" || "D100"}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let itemdata = {
            type: "Defence",
            system: {
              chartype: "CharacterMnM"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          if (!result[1]) {
            itemdata.system.category = "parry";
          } else {
            switch (result[1].toLowerCase()) {
              case "d20":
                itemdata.system.category = "dodge";
                break;
              default:
                itemdata.system.category = "parry";
            }
          }
          itemdata.system.formula = result[2] || "0";
          itemdata.system.sort = Number(result[3]) || 50;
          itemdata.system.notes = result[4] || "";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Trait": {
          // Trait: {{name}} ### {{category}} ### {{notes}} ### {{sort || 0}}
          let itemdata = {
            type: "Trait",
            system: {
              chartype: "CharacterMnM"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.category = result[1].toLowerCase();
          itemdata.system.sort = Number(result[2]) || 50;
          itemdata.system.notes = result[3] || "";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "SharedValue": {
          // SharedValue: {{name}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let itemdata = {
            type: "Defence",
            system: {
              chartype: "CharacterMnM",
              category: "block"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = result[1] || "0";
          itemdata.system.sort = Number(result[2]) || 50;
          itemdata.system.notes = result[3] || "";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Pool": {
          // Pool: {{name}} ### {{abbr}} ### {{max}} ### {{value}} ### {{min}} ### {{sort || 50}} ### {{notes || empty}}
          let itemdata = {
            type: "Pool",
            system: {
              chartype: "CharacterMnM"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.abbr = result[1];
          itemdata.system.max = result[2];
          itemdata.system.value = result[3];
          itemdata.system.min = result[4];
          itemdata.system.sort = Number(result[5]) || 50;
          itemdata.system.notes = result[6] || "";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Attack": {
          // Attack: {{name}} ### {{sort || 50}} ### {{range}} ### {{damage}} ### {{rof}} ### {{payload}} ### {{ammo}} ### {{damage class}}
          let itemdata = {
            system: {
              chartype: "CharacterMnM",
              armourDiv: 20,
              minST: "x2"
            }
          }
          let result = line[2].split("###").map(word => word.trim());
          itemdata.system.sort = Number(result[1]) || 50;
          itemdata.system.damage = result[3];
          itemdata.system.damageType = result[7];

          if (result[2] == "0") { // melee
            itemdata.type = "Melee-Attack";
            itemdata.name = `Melee: ${result[0]}`;
            itemdata.system.formula = "@strike-bonus";
          } else { // ranged
            itemdata.type = "Ranged-Attack";
            itemdata.name = `Ranged: ${result[0]}`;
            itemdata.system.formula = "0";
            itemdata.system.accuracy = result[4];
            itemdata.system.range = result[2];
            if (result[5] != "0") { // ammunition pool required
              let ammodata = {
                name: `${result[0]} Ammunition`,
                type: "Pool",
                system: {
                  abbr: `Ammo${++ammoIndex}`,
                  chartype: "CharacterMnM",
                  min: 0,
                  value: Number(result[5]),
                  max: Number(result[5]),
                  notes: `Reloads: ${Number(result[6])}`
                }
              }
              const item = this.actor.system.items.find(i => i.name === ammodata.name);
              if (item) {
                // do not override the existing saved ammunition tracking
              } else {
                await this.actor.createEmbeddedDocuments('Item', [ammodata]);
              }
            }
          }
          const item = this.actor.system.items.find(i => i.name === itemdata.name);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Armor Type": { // Rollable: check
          let num = line[2].trim();
          const item = this.actor.system.items.find(i => i.name === "Armor Type");
          const update = { _id: item.id, 'system.formula': num, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Level": { // Rollable: check
          let num = line[2].trim();
          const item = this.actor.system.items.find(i => i.name.startsWith("Level-Ranks"));
          const update = { _id: item.id, 'system.formula': num, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Race": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.system.items.find(i => i.name.startsWith("Race"));
          const update = { _id: item.id, 'name': `Race: ${str}`, 'system.formula': 0, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "CharacterClass": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.system.items.find(i => i.name.startsWith("Culture"));
          const update = { _id: item.id, 'name': `Culture: ${str}`, 'system.formula': 0, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Occupation": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.system.items.find(i => i.name.startsWith("Profession"));
          const update = { _id: item.id, 'name': `Profession: ${str}`, 'system.formula': 0, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Psionics": {
          // Psionics: {{name}} ### {{sort || 50}} ### {{ispcost}} ### {{range}} ### {{duration}} ### {{effects}}
          let itemdata = {
            type: "Rollable",
            system: {
              chartype: "CharacterMnM",
              category: "technique"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = "0";
          itemdata.system.sort = Number(result[1]) || 50;
          itemdata.system.notes = `<hr><div><b>ISP Cost:</b> ${result[2]}</div><div><b>Range:</b> ${result[3]}</div><div><b>Duration:</b> ${result[4]}</div><div><b>Effects:</b> ${result[5]}</div>`;
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Magic": {
          // Magic: {{name}} ### {{sort || 50}} ### {{ppecost}} ### {{range}} ### {{duration}} ### {{saves}} ### {{effects}}
          let itemdata = {
            type: "Rollable",
            system: {
              chartype: "CharacterMnM",
              category: "technique"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = "0";
          itemdata.system.sort = Number(result[1]) || 50;
          itemdata.system.notes = `<hr><div><b>PPE Cost:</b> ${result[2]}</div><div><b>Range:</b> ${result[3]}</div><div><b>Duration:</b> ${result[4]}</div><div><b>Saves:</b> ${result[5]}</div><div><b>Effects:</b> ${result[6]}</div>`;
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Melee": {
          // Melee: {{name}} ### {{sort}} ### {{initiative}} ### {{actions}} ### {{strike}}  ### {{parry}}  ### {{dodge}}  ### {{autododge}}  ### {{disarm}}  ### {{entangle}}  ### {{damage || "0"}}  ### {{pullpunch}}  ### {{rollwithpunch}}  ### {{notes  || empty}}  ### {{level}}
          let result = line[2].split("###").map(word => word.trim());
          const name = `[M]-${result[0]}`;
          const sort = result[1];
          const initiative = result[2];
          const actions = result[3];
          const strike = result[4];
          const parry = result[5];
          const dodge = result[6];
          const adodge = result[7];
          const disarm = result[8];
          const entangle = result[9];
          const damage = result[10] || "0";
          const pullpunch = result[11];
          const rollpunch = result[12];
          const notes = `${result[13]}\nLevel: ${result[14]}`;

          let nextEntry = 0;
          let itemdata = {
            name: name,
            type: "Modifier",
            system: {
              chartype: "CharacterMnM",
              notes: notes,
              sort: Number(sort),
              inEffect: false,
              alwaysOn: false,
              attack: false,
              defence: false,
              skill: false,
              spell: false,
              check: false,
              reaction: false,
              damage: false,
              primary: false,
              entries: {
                0: {
                  value: 0,
                  formula: "",
                  category: "attack",
                  targets: ""
                }
              }
            }
          };

          if (initiative != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(initiative),
              formula: initiative,
              category: "defence",
              targets: "Initiative"
            }
            itemdata.system.defence = true;
          }
          if (actions != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(actions),
              formula: actions,
              category: "primary",
              targets: "APR"
            }
            itemdata.system.primary = true;
          }
          if (strike != 0) {
            itemdata.system.entries[nextEntry++] = {
              value: Number(strike),
              formula: strike,
              category: "attack",
              targets: "Melee"
            }
            itemdata.system.attack = true;
          }
          if (parry != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(parry),
              formula: parry,
              category: "defence",
              targets: "Parry"
            }
            itemdata.system.defence = true;
          }
          if (dodge != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(dodge),
              formula: dodge,
              category: "defence",
              targets: "Dodge"
            }
            itemdata.system.defence = true;
          }
          if (adodge != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(adodge),
              formula: adodge,
              category: "defence",
              targets: "Auto Dodge"
            }
            itemdata.system.defence = true;
          }
          if (disarm != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(disarm),
              formula: disarm,
              category: "defence",
              targets: "Disarm"
            }
            itemdata.system.defence = true;
          }
          if (entangle != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(entangle),
              formula: entangle,
              category: "defence",
              targets: "Entangle"
            }
            itemdata.system.defence = true;
          }
          if (damage != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(damage),
              formula: damage,
              category: "damage",
              targets: ""
            }
            itemdata.system.damage = true;
          }
          if (pullpunch != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(pullpunch),
              formula: pullpunch,
              category: "defence",
              targets: "Pull"
            }
            itemdata.system.defence = true;
          }
          if (rollpunch != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(rollpunch),
              formula: rollpunch,
              category: "defence",
              targets: "Roll"
            }
            itemdata.system.defence = true;
          }

          if (!itemdata.system.entries[0]) break;
          const item = this.actor.system.items.find(i => i.name === name);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Ranged": {
          // Ranged: {{name}} ### {{sort}} ### {{initiative}} ### {{actions}} ### {{strike}}  ### {{burst}}  ### {{called}}  ### {{disarm}}  ### {{entangle}}  ### {{damage || "0"}}  ### {{level}}
          let result = line[2].split("###").map(word => word.trim());
          const name = `[R]-${result[0]}`;
          const sort = result[1];
          const initiative = result[2];
          const actions = result[3];
          // the value for strike is derived from the Aim total
          const strike = (result[4] != "0") ? Number(result[4]) - 2 : 0;
          const burst = result[5];
          const called = result[6];
          const disarm = result[7];
          const entangle = result[8];
          const damage = result[9] || "0";
          const notes = `Level: ${result[10]}`;

          let nextEntry = 0;
          let itemdata = {
            name: name,
            type: "Modifier",
            system: {
              chartype: "CharacterMnM",
              notes: notes,
              sort: Number(sort),
              inEffect: false,
              alwaysOn: false,
              attack: false,
              defence: false,
              skill: false,
              spell: false,
              check: false,
              reaction: false,
              damage: false,
              primary: false,
              entries: {
                0: {
                  value: 0,
                  formula: "",
                  category: "attack",
                  targets: ""
                }
              }
            }
          };
          if (initiative != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(initiative),
              formula: initiative,
              category: "defence",
              targets: "Initiative"
            }
            itemdata.system.defence = true;
          }
          if (actions != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(actions),
              formula: actions,
              category: "primary",
              targets: "APR"
            }
            itemdata.system.primary = true;
          }
          if (strike != 0) {
            itemdata.system.entries[nextEntry++] = {
              value: Number(strike),
              formula: strike,
              category: "attack",
              targets: "Ranged"
            }
            itemdata.system.attack = true;
          }
          if (burst != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(burst),
              formula: burst,
              category: "primary",
              targets: "Burst"
            }
            itemdata.system.primary = true;
          }
          if (called != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(called),
              formula: called,
              category: "primary",
              targets: "Called"
            }
            itemdata.system.primary = true;
          }
          if (disarm != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(disarm),
              formula: disarm,
              category: "defence",
              targets: "Disarm"
            }
            itemdata.system.defence = true;
          }
          if (entangle != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(entangle),
              formula: entangle,
              category: "defence",
              targets: "Entangle"
            }
            itemdata.system.defence = true;
          }
          if (damage != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(damage),
              formula: damage,
              category: "damage",
              targets: ""
            }
            itemdata.system.damage = true;
          }

          if (!itemdata.system.entries[0]) break;
          const item = this.actor.system.items.find(i => i.name === name);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
      }
    }
    await this.actor.update({ 'system.biography': biography });
    await this.actor.update({ 'system.itemscript': '' });
    ui.notifications.info("Stat block import complete");
    return;
  }

  /**
   * Fetches the formatted modifiers for a roll.
   */
  fetchRollModifiers(actordata, dataset) {
    let tempMods = [];
    const actormods = actordata.items.filter(i => (i.type == "Modifier"));
    const type = (dataset.type == "modlist") ? dataset.modtype : dataset.type;

    // preload gmod
    tempMods.push({ modifier: Number(actordata.system.gmod.value) || 0, description: 'global modifier' });

    // iterate through the modifiers in effect, picking the appropriate ones for the roll
    for (const mod of actormods) {
      const moddata = mod.system;

      if (moddata.inEffect) {

        for (const entry of moddata.entries) {
          // check to see if this entry applies to this type of roll
          switch (type) {
            case "technique":
              if (entry.category != "skill") continue;
              break;
            case "rms":
              if (entry.category != "spell") continue;
              break;
            case "dodge":
            case "block":
            case "parry":
              if (entry.category != "defence") continue;
              break;
            default:
              if (entry.category != type) continue;
          }
          if (Number(entry.moddedformula) == 0) continue;

          // what is the target of this modifier?
          let hasRelevantTarget = entry.targets.trim();
          // does this modifier have a target matching the item being rolled?
          if (hasRelevantTarget != "") {
            hasRelevantTarget = false;
            // get the array of targets to which it applies
            const cases = entry.targets.split(",").map(word => word.trim());
            for (const target of cases) {
              // test the target against the beginning of dataset.name for a match
              if (dataset.name.startsWith(target)) {
                hasRelevantTarget = true;
                continue;
              }
            }
          } else {
            // a general modifier
            hasRelevantTarget = true;
          }

          if (hasRelevantTarget) {
            let clean = entry.moddedformula?.replace(/\(([-+][\d]+)\)/g, "$1");
            tempMods.push({
              modifier: (clean == undefined) ? entry.value : clean[0] == "+" || clean[0] == "-" ? clean : "+" + clean,
              description: moddata.tempName
            });
          }
        }
      }
    }

    const prepareModList = mods => mods.map(mod => ({ ...mod, modifier: mod.modifier })).filter(mod => mod.modifier !== 0);

    return prepareModList(tempMods);
  }
}
