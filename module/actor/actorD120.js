import { system } from "../config.js";

/**
 * A utility class defining behaviour for ruleset-specific actors
 */
export class ActorD120 {

  constructor(actor) {
    this.actor = actor;
    this.compendiumName = "Base Items";
  }

  prepareBaseData() {
    const actordata = this.actor.system;
    actordata.hasStatBonusTable = system.hasStatBonusTable();
  }

  prepareAdditionalData() {
    const actor = this.actor;
    const actordata = actor.system;
    if (CONFIG.system.testMode) console.debug("entering prepareDerivedData()\n", [actor, actordata]);

    // for counting the ranks stored in Variable Items for some systems
    let actorranks = {};
    let rankitems = [];

    // group the items for efficient iteration
    const actormods = [];
    const nonmods = [];
    const attackmods = [];
    const primods = [];
    const valuemods = [];
    const values = [];
    const skillmods = [];
    const skills = [];
    const reactionmods = [];
    const rollables = [];
    const primaries = [];

    // store references to primary attribute and pool items
    let previousLayer = [];
    let nextLayer = [];

    // write the items into the model
    for (const item of actordata.items) {
      const itemdata = item.system;
      let itemID = actor.slugify(item.name);
      // collect non-modifier items with formula
      if (itemdata.formula) nonmods.push(item);

      switch (item.type) {
        case "Primary-Attribute": {
          let itemID = actor.slugify(itemdata.abbr);
          // only process primaries with abbreviations
          if (itemID) {
            itemdata.moddedvalue = itemdata.value = Number(itemdata.attr);
            // store a reference to the item in the data structure
            actordata.dynamic[itemID] = item;
            // add this to the processed references
            nextLayer.push(itemID);
            primaries.push(item);
          }
          break;
        }
        case "Rank-Progression": {
          actordata.rankitem = item;
          break;
        }
        case "Trait": {
          // store a reference in the data structure
          actordata.traits[itemID] = item;
          switch (item.name) {
            case "Keen Senses":
            case "Night Sight":
            case "Dark Sight":
            case "Darkvision":
            case "Star Sight": {
              actordata.dynamic.visionType = item.name;
            }
          }
          break;
        }
        case "Melee-Attack":
        case "Ranged-Attack":
        case "Advantage":
        case "Power":
        case "Defence":
        case "Rollable": {
          // If the formula is numeric, store it in the array of valid references
          if (Number.isNumeric(itemdata.formula)) {
            nextLayer.push(itemID);
            itemdata.value = itemdata.moddedvalue = Number(itemdata.formula);
          } else {
            itemdata.moddedformula = "";
            itemdata.value = itemdata.moddedvalue = 0;
          }
          // store a reference in the data structure
          actordata.dynamic[itemID] = item;
          // store a reference in a temporary array
          if (itemdata.category == "value") values.push(item);
          else if (itemdata.category == "combatval") values.push(item);
          else if (itemdata.category == "defence") values.push(item);
          else rollables.push(item);
          break;
        }
        case "Hit-Location": {
          break;
        }
        case "Pool": {
          itemID = actor.slugify(itemdata.abbr);
          if (itemID) {
            itemdata.moddedvalue = itemdata.value;
            nextLayer.push(itemID);
            // store a reference in the data structure for resources
            actordata.tracked[itemID] = item;
            nonmods.push(item);
          }
          break;
        }
        case "Variable": {
          // If the formula is numeric, store it in the array of valid references
          if (Number.isNumeric(itemdata.formula)) {
            itemdata.value = itemdata.moddedvalue = Number(itemdata.formula);
            nextLayer.push(itemID);
          }
          // store a reference in the data structure
          actordata.dynamic[itemID] = item;

          // add any items whose name starts with "Ranks:" to the list to be polled next
          if (item.name.startsWith("Ranks:")) rankitems.push(item);

          break;
        }
        case "Modifier": {
          itemdata.tempName = item.name;
          // store a reference in the data structure array of modifiers
          actordata.modifiers[itemID] = item;
          // if it modifies ability stats, add it to a temporary array
          if (itemdata.primary) primods.push(item);
          if (itemdata.attack) attackmods.push(item);
          if (itemdata.defence || itemdata.combatval || itemdata.value) valuemods.push(item);
          if (itemdata.reaction) reactionmods.push(item);
          actormods.push(item);
          break;
        }
        default: { // Traits that define a vision type for the actor
          actordata.dynamic[itemID] = item;
        }
      }
    }

    /**
     * Only count ranks for actors of the current ruleset and possessing a rankitem.
     * 
     * Calculate the ranks, then process number formulae
     */
    if (rankitems.length != 0 && (actordata.ruleset == actordata.mode)) {
      if (CONFIG.system.testMode) console.debug("Rankitems to be polled:\n", [this, rankitems]);

      // count the rank standard skill items
      for (const item of rankitems) {
        for (const entry of item.system.entries) {
          let target = actor.slugify(entry.label);
          // if the name is not blank
          if (target) {
            entry.value = Number(entry.formula);
            // the target may be one that does not exist on the actor so capture it anyway
            actorranks[target] = (actorranks[target]) ? actorranks[target] + entry.value : entry.value;
          }
        }
      }

      if (CONFIG.system.testMode) console.debug("Rankdata after polling:\n", [this, actorranks]);

      let rankprog = actordata.rankitem?.system.standard || "0:1:1:1:1";
      // assign the ranks to the targets in dynamic
      for (const [name, ranks] of Object.entries(actorranks)) {
        if (actordata.dynamic[name]) {
          const item = actordata.dynamic[name];
          const itemdata = item.system;
          // calculate rank bonus to assign to value and moddedvalue
          const value = actor.rankCalculator(ranks, rankprog);
          if (item.type == "Primary-Attribute") {
            itemdata.attr = `#${ranks}`;
            itemdata.ranks = ranks;
            itemdata.value = value;
            itemdata.moddedvalue = value;
          } else {
            itemdata.formula = `#${ranks}`;
            itemdata.ranks = ranks;
            itemdata.value = value;
            itemdata.moddedvalue = value;
          }
          nextLayer.push(name);
        } else {
          if (CONFIG.system.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
        }
      }
    }

    // Modify Primary Attribute values first
    for (const item of primods) {
      if (item.system.inEffect) {
        for (const entry of item.system.entries) {
          // this entry is a primod
          if (entry.category == "primary") {
            // process primods formulae before the mods are applied
            actor.processModifierFormula(entry, item);
            const cases = entry.targets.split(",").map(word => word.trim().toLowerCase());
            for (const key of cases) {
              const splitkey = key.split(".");
              if (splitkey[1] == "value") {
                const target = actordata.dynamic[splitkey[0]];
                if (target?.type == "Primary-Attribute") {
                  // modify the value and moddedvalue together
                  target.system.value = target.system.moddedvalue += entry.value;
                }
              }
            }
          }
        }
      }
    }
    // Set the moddedvalues for proper Primary Attributes
    for (const item of primaries) {
      const itemdata = item.system;
      let itemID = actor.slugify(itemdata.abbr);
      // only process primaries with abbreviations
      if (itemID) {
        if (actordata.hasStatBonusTable && system[actordata.mode].primaryAttributes.includes(itemdata.abbr.toLowerCase())) {
          // this makes the stat modifiers available as a reference
          itemdata.moddedvalue = system.sbs(itemdata.value);
        }
      }
    }
    // Modify Primary Attribute moddedvalues second
    for (const item of primods) {
      if (item.system.inEffect) {
        for (const entry of item.system.entries) {
          // this entry is a primod
          if (entry.category == "primary") {
            const cases = entry.targets.split(",").map(word => word.trim().toLowerCase());
            for (const key of cases) {
              const target = actordata.dynamic[key];
              if (target?.type == "Primary-Attribute") {
                target.system.moddedvalue += entry.value;
                if (actordata.hasStatBonusTable && system[actordata.mode].primaryAttributes.includes(target.system.abbr.toLowerCase())) {
                  /**
                   * This is a primary attribute and 
                   * there is a stat bonus table in effect so 
                   * the value and moddedvalue must remain separate.
                   */
                } else {
                  // this makes the "stat" behave normally
                  target.system.value = target.system.moddedvalue;
                }
              }
            }
          }
        }
      }
    }

    // this will store all valid modifier entries for later processing efficiency
    const activeModEntries = [];

    // calculate the remaining formulae one layer at a time
    /**
     * Calculate every item with a formula, one layer at a time, starting with
     * those whose references have been calculated. Once an item has been
     * processed, add it to the layer of those available as references.
     *
     * TODO: This section needs another look to see if there is a way to
     * avoid the requirement to have all reference depths the same.
     * 
     * I think I have it now. The previousLayer needs to end up being all processed items
     */
    while (nextLayer.length !== 0) {
      previousLayer = previousLayer.concat(nextLayer);
      nextLayer = [];

      for (const item of nonmods) {
        const itemdata = item.system;
        if (item.type == "Pool") {
          // new pool formula functionality
          const dynID = actor.slugify(itemdata.abbr);
          //ignore numeric formulae
          if (itemdata.hasmax && itemdata.hasmin) continue;

          if (!itemdata.hasmax) {
            // formula has not yet been calculated
            for (const target of previousLayer) {
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.maxForm.includes(target)) {
                const formData = actor._replaceData(itemdata.maxForm);
                try {
                  itemdata.max = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, '')) * 100) / 100;
                  itemdata.moddedvalue = itemdata.value = Math.min(itemdata.value, itemdata.max);
                } catch (err) {
                  itemdata.max = 0; // eliminate any old values
                  console.warn("Pool-max formula evaluation error:\n", [actor.name, item.name, itemdata.maxForm]);
                }
                itemdata.hasmax = true;
              }
            }
          }
          if (!itemdata.hasmin) {
            // formula has not yet been calculated
            for (const target of previousLayer) {
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.minForm.includes(target)) {
                const formData = actor._replaceData(itemdata.minForm);
                try {
                  itemdata.min = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, '')) * 100) / 100;
                  itemdata.moddedvalue = itemdata.value = Math.max(itemdata.value, itemdata.min);
                } catch (err) {
                  itemdata.min = 0; // eliminate any old values
                  console.warn("Pool-min formula evaluation error:\n", [actor.name, item.name, itemdata.minForm]);
                }
                itemdata.hasmin = true;
              }
            }
          }
          if (itemdata.hasmax && itemdata.value > itemdata.max) itemdata.value = itemdata.max;
          if (itemdata.hasmin && itemdata.value < itemdata.min) itemdata.value = itemdata.min;
          itemdata.ratio = Math.trunc(itemdata.value / itemdata.max * 100);
          if (itemdata.hasmax && itemdata.hasmin)
            nextLayer.push(dynID);
        } else {
          //ignore items with no formula or whose formula has already been processed
          if (!itemdata.formula || Number.isNumeric(itemdata.formula) || itemdata.formula.startsWith("#") || (Number(itemdata.moddedformula) == itemdata.value && itemdata.moddedformula != "")) continue;
          const dynID = actor.slugify(item.name);
          if (previousLayer.includes(dynID)) continue;

          // the formula is a reference
          if (itemdata.formula.includes("@")) {
            const regex = /@([\w]+)/g;
            let foundAllRefs = true;
            let match;
            while ((match = regex.exec(itemdata.formula)) && foundAllRefs) {
              foundAllRefs = previousLayer.includes(match[1]);
            }
            if (foundAllRefs) {
              let formData = actor._replaceData(itemdata.formula);
              try {
                itemdata.moddedvalue = itemdata.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, "")) * 100) / 100;
                itemdata.moddedformula = "" + itemdata.value; // store the result as a String
              } catch (err) { // TODO: this may not be reachable now that we are checking all the refs first
                // store the formula ready to be rolled
                itemdata.moddedformula = formData.value;
                itemdata.moddedvalue = itemdata.value = 0; // eliminate any old values
                console.warn("Non-Modifier formula evaluation incomplete:\n", [actor.name, item.name, itemdata.moddedformula]);
              }
              // put this item into the next layer and stop looking for matches
              nextLayer.push(dynID);
            }
          } else {
            // the formula might be a dice expression
            if (itemdata.formula.toLowerCase().includes("d")) {
              itemdata.moddedformula = itemdata.formula;
              itemdata.moddedvalue = itemdata.value = 0; // eliminate any old values
            }
          }
        }
      }
    }

    // process all modifiers again after the non-modifiers have been processed
    for (const item of actormods) {

      // check to see if specific modifiers are toggled so we can set the flags
      // the modifier must affect Variable Formulae, D20 skills or D100 skills to work as a toggle (a modifier of zero is ok)
      // these were designed for D6 Pool ruleset but could be used by others
      switch (item.name) {
        case "Rushed": actor.setFlag("grpga", "rushed", item.system.inEffect); break;
        case "Using Edge": actor.setFlag("grpga", "usingedge", item.system.inEffect); break;
        case "Energized": actor.setFlag("grpga", "usingedge", item.system.inEffect); break;
      }

      if (!item.system.inEffect) continue;
      for (const entry of item.system.entries) {
        actor.processModifierFormula(entry, item);
        if (activeModEntries.includes(entry) || entry.value == 0) continue;
        activeModEntries.push(entry);
      }
    }

    // calculate the modified value of the item
    for (const item of rollables) {
      const itemdata = item.system;
      // TODO: confirm that the following is now redundant
      // itemdata.moddedvalue = itemdata.value;
      // if there is no modified formula, make it the value
      if (itemdata.moddedformula == undefined) itemdata.moddedformula = "" + itemdata.value;
      switch (item.type) {
        case "Melee-Attack":
        case "Ranged-Attack": {
          itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "attack", activeModEntries);
          break;
        }
        case "Defence": {
          itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "defence", activeModEntries);
          break;
        }
        case "Rollable": {
          switch (itemdata.category) {
            case "check": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "reaction", activeModEntries);
              break;
            }
            case "technique":
            case "skill": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "skill", activeModEntries);
              break;
            }
            case "rms":
            case "spell": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "spell", activeModEntries);
              break;
            }
          }
          break;
        }
      }
    }
    // this is the end of prepareDerivedData
  }

  /**
   * rolldata consists of:
   * - name: the name of the roll
   * - type: the type of roll (used for filtering modifiers)
   * - roll: the value being processed by this function
   * - modtype: if the type is "modlist" then this is the type of the roll
   * - id: the _id of the item being rolled, so we can access all it's raw data
   */
  async roll(rolldata) {

    let flavour = rolldata.flavour || "";
    const actor = rolldata.actor;
    const actordata = actor.system;
    const item = await actor.items.get(rolldata.id);
    const threat = item?.system.armourDiv || 20;
    const damageType = item?.system.damageType || "";

    let fail = 1;
    let canCrit = false, isModList = false, isPercent = false;
    const useFAHR = actordata.useFAHR;
    let usesStepMods = true; // for Alternity rolls that use Steps

    const withAdvantage = actordata.dynamic.withadvantage?.system.formula || false;
    const woundLevel = actordata.dynamic.wl?.system.value || 0;
    const advantageVariable = await actor.getNamedItem("dynamic.withadvantage");

    const rolledInitiative = rolldata.name == "Initiative";
    let hideTotal = false;

    const usingedge = actor.getFlag("grpga", "usingedge");
    const rushed = actor.getFlag("grpga", "rushed");

    switch (rolldata.type) {
      case "tochat":
      case "technique": { // send the notes to the chat and return
        const isplaintext = rolldata.roll.replace(/<[^>]*>?/gm, '') == rolldata.roll;
        ChatMessage.create({
          speaker: ChatMessage.getSpeaker({ actor: actor }),
          flavor: `<b>${rolldata.name}</b><hr>`,
          content: isplaintext ? rolldata.roll.replace(/(?:\r\n|\r|\n)/g, '<br>') : rolldata.roll,
        });
        return;
      }
      case "rms": { // no roll, just storing a value
        return;
      }
      case "attack": {
        const attack = item;
        const attackdata = attack.system;
        const ammopool = actordata.tracked[attackdata.minST.toLowerCase()];
        if (ammopool) {
          // has ammunition
          const ammoused = Math.min(attackdata.accuracy, ammopool.system.value);
          // Decrement Pool by the Rate of Fire value
          await actor.updateEmbeddedDocuments("Item", [{
            _id: ammopool._id,
            system: { value: Math.max(0, ammopool.system.value - ammoused) }
          }]);
          // Get remaining ammo count post-attack to display in output
          const ammo_left = ammopool.system.value;
          flavour += `<hr><b>Range:</b> ${attackdata.range}<br>`;
          if (ammoused == 0) {
            flavour += `<b>Ammo used:</b> ${ammoused} <span class="critfail">Click! Attack failed! Reload!</span><br>`;
          } else if (ammo_left == 0) {
            flavour += `<b>Ammo used:</b> ${ammoused} (Remaining: ${ammo_left}) <span class="critfail">Reload!</span><br>`;
          } else {
            flavour += `<b>Ammo used:</b> ${ammoused} (Remaining: ${ammo_left})<br>`;
          }
          flavour += `<b>Damage:</b><span class="rollable" data-item-id="{{item._id}}" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage"> ${attackdata.damage} - ${attackdata.damageType}</span><hr><b>Strike Modifiers:</b><br>Base:  `;
        } else {
          if (attack.type == "Ranged-Attack") {
            flavour += `<hr><b>Range:</b> ${attackdata.range}<br><b>Damage:</b><span class="rollable" data-item-id="{{item._id}}" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage"> ${attackdata.damage} - ${attackdata.damageType}</span><hr><b>Strike Modifiers:</b><br>Base:  `;
          } else {
            // a melee attack
            flavour += `<hr><b>Damage:</b><span class="rollable" data-item-id="${item._id}" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage"> ${attackdata.damage} - ${attackdata.damageType}</span><hr><b>Strike Modifiers:</b><br>Base:  `;
          }
        }
        break;
      }
    }
    // add the item-id to the flavour
    flavour += `<span data-item-id="${item?.id}"></span>`;

    // get the modifiers
    const modList = actor.sheet.fetchRollModifiers(actor, { type: rolldata.type, modtype: rolldata.modtype, name: rolldata.name });
    // process the modifiers
    let modformula = "";
    flavour += (rolldata.type != "modlist") ? ((rolldata.type == "damage") ? ` [<b>${rolldata.roll} ${damageType}</b>]` : ` [<b>${rolldata.roll}</b>]`) : `<p class="chatmod">[<b>${rolldata.roll}</b>]: ${rolldata.name}<br>`;
    const hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      flavour += (rolldata.type != "modlist") ? `<p class="chatmod">` : "";
      for (const mod of modList) {
        sign = typeof mod.modifier === 'string' ? "" : mod.modifier > -1 ? "+" : "";
        modformula += `${sign}${mod.modifier}`;
        flavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    var formula = "";
    switch (rolldata.type) {
      case "modlist": // this will render a dialog with the modifiers, not a chat message
        isModList = true;
      case "block": // send the value to the chat as a modifiable roll
      case "damage": {// roll damage using a valid dice expression
        formula = rolldata.roll;
        usesStepMods = false;
        break;
      }
      case "check": { // a valid dice expression
        formula = rolldata.roll;
        isPercent = formula.includes("1d100");
        break;
      }
      case "parry": // a d100 roll less than or equal to a target number
      case "spell": {
        formula = rolldata.roll + " - 1d100";
        usesStepMods = false;
        isPercent = true;
        break;
      }
      case "attack": // attacks are subject to critical results
      case "dodge": // a d20 roll to exceed a target number
      case "skill": {
        switch (CONFIG.system.hijack) {
          case "UseD6Pool": { // use d6 pools where successes are counted on a 6
            if (rolledInitiative) {
              formula = `${rolldata.roll} + (${rolldata.roll + modformula})d6cs>4`;
              if (woundLevel < 0) {
                formula += woundLevel;
                flavour += `<p class="chatmod"><b>${woundLevel}</b>: Wound Modifiers<br>`;
              }
            } else if (usingedge) {
              formula = `(${rolldata.roll + modformula})d6x6cs>4`;
            } else {
              formula = `(${rolldata.roll + modformula})d6cs>4`;
            }
            modformula = "";
            break;
          }
          case "UsePnP": { // use d6 pools where successes are counted on a 4,5,(6x2)
            formula = (usingedge) ? `(${rolldata.roll + modformula})d6x6` : `(${rolldata.roll + modformula})d6`;
            modformula = "";
            break;
          }
          case "UseNovus": { // a hijack to roll an exploding 2d10 instead
            formula = "1d10x10 + 1d10x10 + " + rolldata.roll;
            break;
          }
          case "UseBlackNight": { // a hijack to roll an open-ended 2d10 instead
            formula = "1d10x10 + 1d10x10 + 1d10x10 + 1d10x10 + " + rolldata.roll;
            break;
          }
          case "Use2D10": { // a hijack to roll 2d10 instead
            formula = (withAdvantage) ? `${withAdvantage} + ` + rolldata.roll : "2d10 + " + rolldata.roll;
            fail = 2;
            break;
          }
          case "UseD10": { // a hijack to roll d10 instead
            formula = (withAdvantage) ? `${withAdvantage} + ` + rolldata.roll : "1d10 + " + rolldata.roll;
            break;
          }
          case "Use3D6": { // a hijack to roll 3d6 instead
            formula = (withAdvantage) ? `${withAdvantage} + ` + rolldata.roll : "3d6 + " + rolldata.roll;
            break;
          }
          case "Use2D6": { // a hijack to roll 2d6 instead
            if (CONFIG.system.useSineNomine && rolldata.type == "attack") { // WWN/SWN attacks
              formula = "1d20 + " + rolldata.roll;
            } else if (CONFIG.system.useSineNomine && rolldata.type == "dodge") { // WWN/SWN Save
              formula = "1d20 - " + rolldata.roll;
            } else { // normal 2d6 or WWN/SWN skills
              formula = (withAdvantage) ? `${withAdvantage} + ` + rolldata.roll : "2d6 + " + rolldata.roll;
            }
            break;
          }
          case "UsePercent": { // a hijack to roll percent instead
            if (useFAHR && rolledInitiative) { // jnov36 initiative roll
              formula = "1d10 + " + rolldata.roll;
            } else { // normal percentile roll
              formula = rolldata.roll + " - 1d100";
            }
            isPercent = true;
            break;
          }
          case "UseAlternity": { // a hijack to roll Control and Situation dice less than a target number
            formula = rolldata.roll + " - (1d20";
            let steps = eval(modformula) || 0;
            switch (steps) {
              case -4: formula += " - 1d12)"; break;
              case -3: formula += " - 1d8)"; break;
              case -2: formula += " - 1d6)"; break;
              case -1: formula += " - 1d4)"; break;
              case 0: formula += " + 0d4)"; break;
              case 1: formula += " + 1d4)"; break;
              case 2: formula += " + 1d6)"; break;
              case 3: formula += " + 1d8)"; break;
              case 4: formula += " + 1d12)"; break;
              case 5: formula += " + 1d20)"; break;
              case 6: formula += " + 2d20)"; break;
              case 7: formula += " + 3d20)"; break;
              case 8: formula += " + 4d20)"; break;
              case 9: formula += " + 5d20)"; break;
              case 10: formula += " + 6d20)"; break;
              case 11: formula += " + 7d20)"; break;
              case 12: formula += " + 8d20)"; break;
              case 13: formula += " + 9d20)"; break;
              default: formula += " - 1d20)"; break;
            }
            break;
          }
          default: { // standard Palladium behaviour
            canCrit = true;
            if (CONFIG.system.useSineNomine && rolldata.type == "attack") { // Godbound THAC0
              formula = "20 - 1d20 - " + rolldata.roll;
            } else if (CONFIG.system.useSineNomine && rolldata.type == "dodge") { // Godbound Save
              formula = "1d20 - " + rolldata.roll;
              formula = `1d20 - (${rolldata.roll}`;
            } else if (useFAHR && rolledInitiative) { // FAHR initiative roll
              formula = "3d6 + " + rolldata.roll;
            } else { // normal d20 attacks
              formula = (withAdvantage) ? `${withAdvantage} + ` + rolldata.roll : "1d20 + " + rolldata.roll;
            }
          }
        }
        break;
      }
      default: { // there should not be any of these
        console.error("Failed to process rolldata roll type: " + rolldata.type);
      }
    }

    if (CONFIG.system.hijack == "UseAlternity") {
      modformula = "";
    } else if (CONFIG.system.useSineNomine && rolldata.type == "dodge") {
      modformula += ")";
    }
    // process the modified roll
    let roll = await new Roll(formula + modformula).evaluate({ async: true });

    if (isModList) { // render the modlist dialog and return
      flavour += `<hr><p>${roll.result} = <b>${roll.total}</b></p>`;
      new Dialog({
        title: actor.name,
        content: flavour,
        buttons: {
          close: {
            label: "Close"
          },
        },
        default: "close"
      }).render(true)
      return;
    }
    if (rolldata.type == "block") { // send the modified value to the chat and return
      flavour += ` <hr>Total: [<b>${roll.total}</b>]`;
      ChatMessage.create({
        speaker: ChatMessage.getSpeaker({ actor: actor }),
        content: flavour
      });
      return;
    }

    if (CONFIG.system.hijack == "UseD6Pool") {
      let number = roll.dice[0].number;
      let glitch = (rushed) ? 3 : 2;
      let glitches = 0;
      for (let i = 0; i < number; i++) {
        if (roll.dice[0].values[i] < glitch) glitches++;
      }
      if (glitches >= number / 2) {
        flavour += ` <p><i class="fas fa-arrow-right"></i> <span class="critfail">Glitched</span></p>`;
      }
    } else if (CONFIG.system.hijack == "UsePnP") { // a roll in Prowlers & Paragons RPG
      hideTotal = true;
      const dice = roll.dice[0];
      const value = [0, 0, 0, 0, 0, 0, 0];
      for (let i = 0; i < dice.values.length; i++) {
        value[dice.values[i]]++;
        switch (dice.values[i]) {
          case 6:
            value[0]++;
          case 5:
          case 4:
            value[0]++
        }
      }
      flavour += ` <p>${roll.formula} Roll:</p> 
      <table><tr><td><span style="color:red;">1-3: </span> ${value[1] + value[2] + value[3]}</td>
      <td><span style="color:green;">4-5: </span> ${value[4] + value[5]}</td> 
      <td><span class="critsuccess">6: </span> ${value[6]}</td></tr></table>
      Successes: [<b>${value[0]}</b>]`;
    } else if (CONFIG.system.useSineNomine) { // a roll in Godbound or WWN/SWN
      hideTotal = true;
      const isGodbound = CONFIG.system.hijack != "Use2D6";
      const isAttack = rolldata.type == "attack";
      // did a critical success or failure occur?
      let critfail = false;
      let critsuccess = false;
      if (((isGodbound && isAttack) ? roll.terms[2].total : roll.terms[0].total) <= fail) {
        critfail = true;
      } else if (((isGodbound && isAttack) ? roll.terms[2].total : roll.terms[0].total) >= threat) {
        critsuccess = true;
      }
      // display the die roll result
      if (!isGodbound && rolldata.type == "skill") {// WWN/SWN skill roll
        flavour += ` <p>2D6 Roll: [<b>${roll.terms[0].total}</b>]`;
      } else if (isGodbound && isAttack) { // godbound THAC0 attack
        flavour += ` <p>D20 Roll: [<b>${roll.terms[2].total}</b>]`;
      } else if (isGodbound && rolldata.type == "damage") {
        const damagetaken = roll.terms[0].results
        // process the damage taken values
        for (const result of damagetaken) {
          result.dt = this.godboundDamageTaken(result.result);
        }
        const hasmod = roll.terms.length > 1;
        console.log([roll, damagetaken]);
        if (hasmod) {
          let double = false; // whether there is a double increase
          let single = -1; // the index of a single increase
          const mod = roll.terms[2].total;
          for (let i = 0; i < damagetaken.length && !double; i++) {
            if (damagetaken[i].result < 10 && damagetaken[i].result + mod >= 10) {
              damagetaken[i].dt += 2;
              double = true;
            } else if (this.godboundDamageTaken(damagetaken[i].result + mod) > damagetaken[i].dt) {
              single = i;
            }
          }
          if (!double && single != -1) damagetaken[single].dt += 1;
        }
        // get ready to display the results
        let dt = damagetaken[0].dt;
        let rolled = ` <p>Damage Rolled: ${damagetaken[0].result}`;
        let taken = ` <p>Damage Taken: ${dt}`;
        for (let i = 1; i < damagetaken.length; i++) {
          rolled += ` + ${damagetaken[i].result}`;
          taken += ` + ${damagetaken[i].dt}`;
          dt += damagetaken[i].dt;
        }
        if (hasmod) rolled += ` + ${roll.terms[2].total}`;
        rolled += ` = [<b>${roll.total}</b>]</p>`;
        taken += ` = [<b>${dt}</b>]`;
        flavour += rolled + taken;
      } else { // all other rolls
        switch (rolldata.type) {
          case "attack":
          case "dodge":
          case "skill": {
            flavour += ` <p>D20 Roll: [<b>${roll.terms[0].total}</b>]`;
            break;
          }
          default: {
            flavour += ` <p>Result: [<b>${roll.total}</b>]`;
          }
        }
      }
      switch (rolldata.type) {
        case "dodge": { // all
          if (critsuccess) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
          else if (critfail) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
          else if (roll.total >= 0) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Success</span>`;
          else flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Failure</span>`;
          break;
        }
        case "skill": {
          if (!isGodbound) {
            flavour += ` </p><p>Result: [<b>${roll.total}</b>]`
          } else {
            if (critsuccess) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
            else if (critfail) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
            else if (roll.total >= 21) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Success</span>`;
            else flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Failure</span>`;
          }
          break;
        }
        case "attack": {
          if (critsuccess) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
          else if (critfail) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
          if (!isGodbound) {
            flavour += ` </p><p>Result: [<b>${roll.total}</b>]`
          } else {
            flavour += ` </p><p>Hits AC: [<b>${roll.total}</b>]`
          }
        }
      }
      flavour += `</p>`
    } else if (CONFIG.system.hijack == "UseNovus" && rolldata.type != "damage") { // a roll in Novus 2e RPG
      hideTotal = false;
      let mainrollobject = roll.toJSON();
      console.log("mainrollobject", [roll, mainrollobject]);
      mainrollobject.evaluated = false;
      const firstdie = mainrollobject.terms.shift();
      mainrollobject.terms.shift();
      const newterm = mainrollobject.terms[0];
      newterm.results = newterm.results.concat(firstdie.results);
      mainrollobject.formula = mainrollobject.formula.replace("1d10x10 + 1", "2");
      newterm.number = 2;
      const die1 = roll.dice[0].values[0];
      const die2 = roll.dice[1].values[0];
      const ultimate = die1 + die2;
      switch (ultimate) {
        case 2: {
          flavour += ` <p>2d10 Roll: [<b>1 & 1</b>] <i class="fas fa-arrow-right"></i> <span class="critfail">Botched</span>`;
          flavour += ` <hr>Total: [<b>2</b>]`;
          hideTotal = true;
          break;
        }
        case 11: {
          if (die1 == 1) {
            flavour += ` <p>2d10 Roll: [<b>1 & 10</b>] <i class="fas fa-arrow-right"></i> <span class="critsuccess">Nova</span>`;
            flavour += ` <hr>Total: [<b>11</b>]`;
            hideTotal = true;
          }
          else if (die1 == 10) {
            flavour += ` <p>2d10 Roll: [<b>10 & 1</b>] <i class="fas fa-arrow-right"></i> <span class="critsuccess">Nova</span>`;
            flavour += ` <hr>Total: [<b>11</b>]`;
            hideTotal = true;
          } else {
            flavour += ` <p>2d10x Roll: [<b>${roll.dice[0].total + roll.dice[1].total}</b>]`;
          }
          break;
        }
        case 20: {
          flavour += ` <p>2d10 Roll: [<b>10 & 10</b>] <i class="fas fa-arrow-right"></i> <span class="critsuccess">Super Nova</span>`;
          flavour += ` <hr>Total: [<b>40</b>]`;
          hideTotal = true;
          break;
        }
        default: {
          flavour += ` <p>2d10x Roll: [<b>${roll.dice[0].total + roll.dice[1].total}</b>]`;
        }
      }
      // re-evaluate the expression with the selected options
      roll = await Roll.fromData(mainrollobject).evaluate({ async: true });
      console.log("mainrollobject", [roll, mainrollobject]);
    } else if (CONFIG.system.hijack == "UseBlackNight" && rolldata.type != "damage") { // a roll in Black Night RPG
      hideTotal = false;
      let mainrollobject = roll.toJSON();
      console.log("mainrollobject", [roll, mainrollobject]);
      mainrollobject.evaluated = false;
      mainrollobject.formula = mainrollobject.formula.replace("1d10x10 + 1d10x10 + 1d10x10 + 1d10x10", "2d10oe");
      const firstdie = mainrollobject.terms.shift();
      mainrollobject.terms.shift(); // an operator
      if (roll.dice[0].total > 1) {
        // a normal result so get rid of next die
        mainrollobject.terms.shift();
      } else {
        // the results of the second die must be subracted from the total
        firstdie.results[0].exploded = true;
        const dive = mainrollobject.terms.shift();
        for (const result of dive.results) {
          result.result *= -1;
          firstdie.results.push(result);
        }
      }
      mainrollobject.terms.shift(); // an operator
      const nextdie = mainrollobject.terms.shift();
      mainrollobject.terms.shift(); // an operator
      if (roll.dice[2].total > 1) {
        // a normal result so get rid of next die
        mainrollobject.terms.shift();
      } else {
        // the results of the fourth die must be subracted from the total
        nextdie.results[0].exploded = true;
        const dive = mainrollobject.terms.shift();
        for (const result of dive.results) {
          result.result *= -1;
          nextdie.results.push(result);
        }
      }
      firstdie.results = firstdie.results.concat(nextdie.results);
      mainrollobject.terms.unshift(firstdie);




      // re-evaluate the expression with the selected options
      roll = await Roll.fromData(mainrollobject).evaluate({ async: true });
      console.log("mainrollobject", [roll, mainrollobject]);
      roll.toMessage(
        {
          speaker: ChatMessage.getSpeaker({ actor: actor }),
          flavor: flavour,
          flags: { hideMessageContent: hideTotal }
        }
      );
      return;
      mainrollobject.formula = mainrollobject.formula.replace("1d10x10 + 1", "2");
      newterm.number = 2;
      const die1 = roll.dice[0].values[0];
      const die2 = roll.dice[1].values[0];
      const ultimate = die1 + die2;
      switch (ultimate) {
        case 2: {
          flavour += ` <p>2d10 Roll: [<b>1 & 1</b>] <i class="fas fa-arrow-right"></i> <span class="critfail">Botched</span>`;
          flavour += ` <hr>Total: [<b>2</b>]`;
          hideTotal = true;
          break;
        }
        case 11: {
          if (die1 == 1) {
            flavour += ` <p>2d10 Roll: [<b>1 & 10</b>] <i class="fas fa-arrow-right"></i> <span class="critsuccess">Nova</span>`;
            flavour += ` <hr>Total: [<b>11</b>]`;
            hideTotal = true;
          }
          else if (die1 == 10) {
            flavour += ` <p>2d10 Roll: [<b>10 & 1</b>] <i class="fas fa-arrow-right"></i> <span class="critsuccess">Nova</span>`;
            flavour += ` <hr>Total: [<b>11</b>]`;
            hideTotal = true;
          } else {
            flavour += ` <p>2d10x Roll: [<b>${roll.dice[0].total + roll.dice[1].total}</b>]`;
          }
          break;
        }
        case 20: {
          flavour += ` <p>2d10 Roll: [<b>10 & 10</b>] <i class="fas fa-arrow-right"></i> <span class="critsuccess">Super Nova</span>`;
          flavour += ` <hr>Total: [<b>40</b>]`;
          hideTotal = true;
          break;
        }
        default: {
          flavour += ` <p>2d10x Roll: [<b>${roll.dice[0].total + roll.dice[1].total}</b>]`;
        }
      }
      // re-evaluate the expression with the selected options
      roll = await Roll.fromData(mainrollobject).evaluate({ async: true });
      console.log("mainrollobject", [roll, mainrollobject]);
    } else if (CONFIG.system.use616dice) { // a roll in Marvel Multiverse RPG
      hideTotal = true;
      const fantastic = roll.dice[0].values[1];
      const ultimate = roll.dice[0].total;
      flavour += ` <p>d616 Roll: [<b>${roll.dice[0].values[0]} <span style="color:lightcoral;">${roll.dice[0].values[1]}</span> ${roll.dice[0].values[2]}</b>]`;
      if (fantastic == 1) {
        switch (ultimate) {
          case 3: {
            flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Botched</span>`;
            break;
          }
          case 13: {
            flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Ultimate Fantastic</span>`;
            break;
          }
          default: {
            flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Fantastic</span>`;
            break;
          }
        }
      }
      flavour += ` <hr>Total: [<b>${roll.total}</b>]`;
    } else if (CONFIG.system.hijack == "UseAlternity" && usesStepMods) { // a roll in Alternity RPG
      hideTotal = true;
      const control = roll.dice[0].total;
      flavour += ` <p>Control Roll: [<b>${control}</b>]`;
      if (control == 1) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
      else if (control == 20) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
      flavour += `</p>`

      let result = roll.total;
      let sflavour;
      flavour += ` <p>Situation Roll: [<b>${roll.dice[1].total}</b>]<hr>Total: [<b>${roll.terms[2].total}</b>]`;
      if (result >= 0) {
        // ordinary
        sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Ordinary Success</span>`;
        let total = roll.terms[2].total;
        if (result >= total) {
          // good
          sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Good Success</span>`;
          if (result >= total * 3) {
            //amazing
            sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Amazing Success</span>`;
          }
        }
      } else {
        // failure or marginal
        sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critfail">Failure/Marginal</span>`;
      }
      flavour += sflavour + `</p>`
    } else if (canCrit) { // an attack or defence roll in D120
      let critfail = false;
      let critsuccess = false;
      if (roll.terms[0].total <= fail) {
        critfail = true;
      } else if (roll.terms[0].total >= threat) {
        critsuccess = true;
      }
      flavour += ` <p>D20 Roll: [<b>${roll.terms[0].total}</b>]`;
      if (critsuccess) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Critical Success</span>`;
      else if (critfail) flavour += ` <i class="fas fa-arrow-right"></i> <span class="critfail">Critical Failure</span>`;
      flavour += `</p>`
    } else if (isPercent && !rolledInitiative) { // a skill roll in D120
      let result = roll.total;
      let sflavour;
      flavour += ` <p>D100 Roll: [<b>${roll.dice[0].values[0]}</b>]`;
      if (result >= 0) {
        // success
        sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Success</span>`;
        if (useFAHR) {
          let total = roll.dice[0].values[0];
          if (result >= total) {
            // heroic
            sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Heroic Success</span>`;
            if (result >= total * 3) {
              //legendary
              sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Legendary Success</span>`;
            }
          }
        }
      } else {
        // failure
        sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critfail">Failure</span>`;
      }
      flavour += sflavour + `</p>`
    }
    if (advantageVariable) {
      let state = advantageVariable.system.label;
      switch (state) {
        case "Major Advantage":
          flavour += ` <p class="critsuccess">With ${state}</p>`;
          break;
        case "Advantage":
          flavour += ` <p class="critsuccess">With ${state}</p>`;
          break;
        case "Disadvantage":
          flavour += ` <p class="critfail">With ${state}</p>`;
          break;
        case "Major Disadvantage":
          flavour += ` <p class="critfail">With ${state}</p>`;
          break;
      }

    }

    // prepare the flavor in advance based on which type of die is in use.
    roll.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: actor }),
        flavor: flavour,
        flags: { hideMessageContent: hideTotal }
      }
    );
    let updates = { ["system.gmod.value"]: 0 };
    if (rolledInitiative) {
      updates["system.bs.value"] = roll.total;
    }
    actor.resetModVars(false);
    actor.update(updates);
  }

  godboundDamageTaken(rolled) {
    if (rolled < 2) return 0;
    if (rolled < 6) return 1;
    if (rolled < 10) return 2;
    return 4;
  }
}
