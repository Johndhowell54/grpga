import { baseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {baseActorSheet}
 */
export class Actor3D6Sheet extends baseActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actor3d6-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "combat" }]
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.importGCS').click(this._onImportGCSData.bind(this));
  }

  async _onImportGCSData(event) {
    event.preventDefault();
    const scriptdata = this.actor.system.itemscript.split(/\r?\n/).map(word => word.trim());
    const useFAHR = this.actor.system.useFAHR;
    let race = "Human";

    for (let entry of scriptdata) {
      let regex = new RegExp(/([A-z- ]+):([^\r|\n]+)/gi);
      let line = regex.exec(entry);
      switch (line[1].trim()) {
        case "Name": {
          await this.actor.update({ 'name': line[2].trim() });
          break;
        }
        case "Spell":
        case "Skill": {
          let itemdata = {
            type: "Rollable",
            system: {
              chartype: "Character3D6",
              category: line[1].toLowerCase(),
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          let regex = new RegExp(/([^ +-]+)?([+|-][0-9]+)/gi);
          let rsl = regex.exec(result[1]);
          if (!rsl) {
            itemdata.name = result[0];
          } else if (rsl[1]) {
            itemdata.name = result[0];
            itemdata.system.formula = `@${this.actor.slugify(rsl[1])} ${rsl[2]}`;
          } else { // technique or rms
            let diff = rsl[2];
            regex = new RegExp(/([A-Z ]+) \(([^\)]+\)*)\)/gi);
            rsl = regex.exec(result[0]);
            itemdata.name = rsl[0];
            itemdata.system.formula = `@${this.actor.slugify(rsl[2])} ${diff}`;
          }
          itemdata.system.notes = result[2];
          await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          break;
        }
        case "Primary-Attribute": {
          let itemdata = {
            type: line[1],
            system: {
              chartype: "Character3D6"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.abbr = result[1];
          itemdata.system.attr = result[2];
          // look to see if we already have an attribute with this name
          const item = this.actor.system.items.find(i => i.name == result[0] && i.type == "Primary-Attribute");
          if (item?.id) {
            await this.actor.updateEmbeddedDocuments("Item", [{ _id: item._id, 'system.attr': result[2] }]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Pool": {
          let itemdata = {
            type: line[1],
            system: {
              chartype: "Character3D6"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.abbr = result[1];
          itemdata.system.max = result[2];
          itemdata.system.value = result[2];
          // look to see if we already have a pool with this name
          const item = this.actor.system.items.find(i => i.name == result[0]);
          if (item?.id) {
            await this.actor.updateEmbeddedDocuments("Item", [{ _id: item._id, 'system.max': result[2], 'system.value': result[2] }]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Basic Speed": {
          await this.actor.update({ 'system.bs.value': Number(line[2]) });
          break;
        }
        case "Basic Move": {
          await this.actor.update({ 'system.bm.move': Number(line[2]) });
          break;
        }
        case "Check": {
          let itemdata = {
            type: "Rollable",
            system: {
              chartype: "Character3D6",
              category: line[1].toLowerCase(),
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = result[1];
          await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          break;
        }
        case "Melee-Attack": {
          let itemdata = {
            type: line[1],
            system: {
              chartype: "Character3D6",
              armourDiv: 1
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = result[1];
          let damage = result[2].split(" ").map(word => word.trim());
          itemdata.system.damage = damage[0].replace('d', 'd6');
          itemdata.system.damageType = damage[1];
          itemdata.system.minST = result[3];
          itemdata.system.weight = result[4];
          itemdata.system.reach = result[5];
          itemdata.system.notes = result[6];
          await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          break;
        }
        case "Defence": {
          let itemdata = {
            type: line[1],
            system: {
              chartype: "Character3D6",
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = ((result[1] == "No") ? "Block - " : "Parry - ") + result[0];
          itemdata.system.formula = ((result[1] == "No") ? result[2] : result[1].split("U")[0]);
          itemdata.system.weight = result[3];
          itemdata.system.notes = result[4];
          await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          break;
        }
        case "Ranged-Attack": {
          let itemdata = {
            type: line[1],
            system: {
              chartype: "Character3D6",
              armourDiv: 1
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = result[1];
          let damage = result[2].split(" ").map(word => word.trim());
          itemdata.system.damage = damage[0].replace('d', 'd6');
          itemdata.system.damageType = damage[1];
          itemdata.system.minST = result[3];
          itemdata.system.accuracy = result[4];
          itemdata.system.range = result[5];
          itemdata.system.rof = result[6];
          itemdata.system.shots = result[7];
          itemdata.system.bulk = result[8];
          itemdata.system.recoil = result[9];
          itemdata.system.notes = result[10];
          await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          break;
        }
        case "Advantage":
        case "Disadvantage":
        case "Perk":
        case "Quirk": {
          let itemdata = {
            type: "Trait",
            system: {
              chartype: "Character3D6",
              category: line[1].toLowerCase(),
              notes: ""
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          if (result[1]) itemdata.system.notes += 'Modifier: ' + result[1] + '\r\n';
          itemdata.system.notes += result[2];
          if (result[3] == "RACE") race = result[0];
          await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          break;
        }
        case "Hit-Location": {
          if (!useFAHR) {
            let itemdata = {
              type: line[1],
              system: {
                chartype: "Character3D6",
              }
            };
            let result = line[2].split("###").map(word => word.trim());
            itemdata.name = result[0];
            itemdata.system.damageResistance = result[1];
            itemdata.system.toHitPenalty = result[2];
            itemdata.system.notes = result[3];
            itemdata.system.toHitRoll18 = result[4];
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Equipment": {
          let result = line[2].split("###").map(word => word.trim());
          switch (result[4].trim()) {
            case "Body Armour": {
              let itemdata = {
                type: "Hit-Location",
                system: {
                  chartype: "Character3D6",
                  hp: this.actor.system.tracked.hp.value,
                  notes: result[0] + "; " + result[7]
                }
              };
              let regex = new RegExp(/^DR:([\d]+)/gi);
              itemdata.system.damageResistance = Number(regex.exec(result[7])[1]);
              regex = new RegExp(/locn:([\w ]+)/gi);
              let locations = Array.from(result[6].matchAll(regex));
              for (let i = 0; i < locations.length; i++) {
                switch (locations[i][1].trim()) {
                  case "fullbody": {
                    await this.head(itemdata);
                    await this.neck(itemdata);
                    await this.torso(itemdata);
                    await this.arms(itemdata);
                    await this.legs(itemdata);
                    await this.hands(itemdata);
                    await this.feet(itemdata);
                    break;
                  }
                  case "head": {
                    await this.head(itemdata);
                    break;
                  }
                  case "skull": {
                    await this.skull(itemdata);
                    break;
                  }
                  case "face": {
                    await this.face(itemdata);
                    break;
                  }
                  case "neck": {
                    await this.neck(itemdata);
                    break;
                  }
                  case "torso": {
                    await this.torso(itemdata);
                    break;
                  }
                  case "chest": {
                    await this.chest(itemdata);
                    break;
                  }
                  case "abdomen": {
                    await this.abdomen(itemdata);
                    break;
                  }
                  case "vitals": {
                    await this.vitals(itemdata);
                    break;
                  }
                  case "arms": {
                    await this.arms(itemdata);
                    break;
                  }
                  case "shoulders": {
                    await this.shoulders(itemdata);
                    break;
                  }
                  case "upperarms": {
                    await this.upperarms(itemdata);
                    break;
                  }
                  case "elbows": {
                    await this.elbows(itemdata);
                    break;
                  }
                  case "forearms": {
                    await this.forearms(itemdata);
                    break;
                  }
                  case "hands": {
                    await this.hands(itemdata);
                    break;
                  }
                  case "legs": {
                    await this.legs(itemdata);
                    break;
                  }
                  case "thighs": {
                    await this.thighs(itemdata);
                    break;
                  }
                  case "knees": {
                    await this.knees(itemdata);
                    break;
                  }
                  case "shins": {
                    await this.shins(itemdata);
                    break;
                  }
                  case "feet": {
                    await this.feet(itemdata);
                    break;
                  }
                  case "eyes": {
                    await this.eyes(itemdata);
                    break;
                  }
                  case "groin": {
                    await this.groin(itemdata);
                    break;
                  }
                }
              }
            }
          }
          break;
        }
        default: {
          // append the entry to notes
        }
      }
    }
    await this.actor.update({ 'system.itemscript': '' });
    ui.notifications.info("GCS import complete");
  }

  /**
   * Fetches the formatted modifiers for a roll.
   */
  fetchRollModifiers(actordata, dataset) {
    let tempMods = [];
    const actormods = actordata.items.filter(i => (i.type == "Modifier"));
    const type = (dataset.type == "modlist") ? dataset.modtype : dataset.type;

    // preload gmod
    tempMods.push({ modifier: Number(actordata.system.gmod.value) || 0, description: 'global modifier' });

    // iterate through the modifiers in effect, picking the appropriate ones for the roll
    for (const mod of actormods) {
      const moddata = mod.system;

      if (moddata.inEffect) {

        for (const entry of moddata.entries) {
          // check to see if this entry applies to this type of roll
          switch (type) {
            case "technique":
              if (entry.category != "skill") continue;
              break;
            case "rms":
              if (entry.category != "spell") continue;
              break;
            case "dodge":
            case "block":
            case "parry":
              if (entry.category != "defence") continue;
              break;
            default:
              if (entry.category != type) continue;
          }
          if (Number(entry.moddedformula) == 0) continue;

          // what is the target of this modifier?
          let hasRelevantTarget = entry.targets.trim();
          // does this modifier have a target matching the item being rolled?
          if (hasRelevantTarget != "") {
            hasRelevantTarget = false;
            // get the array of targets to which it applies
            const cases = entry.targets.split(",").map(word => word.trim());
            for (const target of cases) {
              // test the target against the beginning of dataset.name for a match
              if (dataset.name.startsWith(target)) {
                hasRelevantTarget = true;
                continue;
              }
            }
          } else {
            // a general modifier
            hasRelevantTarget = true;
          }

          if (hasRelevantTarget) {
            let clean = entry.moddedformula?.replace(/\(([-+][\d]+)\)/g, "$1");
            tempMods.push({
              modifier: (clean == undefined) ? entry.value : clean[0] == "+" || clean[0] == "-" ? clean : "+" + clean,
              description: moddata.tempName
            });
          }
        }
      }
    }

    const prepareModList = mods => mods.map(mod => ({ ...mod, modifier: mod.modifier })).filter(mod => mod.modifier !== 0);

    return prepareModList(tempMods);
  }

  async head(itemdata) {
    this.skull(itemdata);
    this.face(itemdata);
    this.eyes(itemdata);
  }
  async skull(itemdata) {
    itemdata.name = "Skull";
    itemdata.system.toHitRoll18 = "3,4";
    itemdata.system.toHitRoll6 = "All";
    itemdata.system.toHitPenalty = -7;
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async face(itemdata) {
    itemdata.name = "Face";
    itemdata.system.toHitRoll18 = "5";
    itemdata.system.toHitRoll6 = "All";
    itemdata.system.toHitPenalty = -5;
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async eyes(itemdata) {
    itemdata.name = "Eyes";
    itemdata.system.toHitRoll18 = "-";
    itemdata.system.toHitRoll6 = "-";
    itemdata.system.toHitPenalty = -9;
    itemdata.system.toCripple = Number(itemdata.system.hp / 10);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async neck(itemdata) {
    itemdata.name = "Neck";
    itemdata.system.toHitRoll18 = "17,18";
    itemdata.system.toHitRoll6 = "All";
    itemdata.system.toHitPenalty = -5;
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async torso(itemdata) {
    this.chest(itemdata);
    this.abdomen(itemdata);
    this.vitals(itemdata);
  }
  async chest(itemdata) {
    itemdata.name = "Chest";
    itemdata.system.toHitRoll18 = "9,10";
    itemdata.system.toHitRoll6 = "2,3,4,5,6";
    itemdata.system.toHitPenalty = -0;
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async abdomen(itemdata) {
    itemdata.name = "Abdomen";
    itemdata.system.toHitRoll18 = "11";
    itemdata.system.toHitRoll6 = "2,3,4,5,6";
    itemdata.system.toHitPenalty = -2;
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async vitals(itemdata) {
    itemdata.name = "Vitals";
    itemdata.system.toHitRoll18 = "9,10,11";
    itemdata.system.toHitRoll6 = "1";
    itemdata.system.toHitPenalty = -3;
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async groin(itemdata) {
    itemdata.name = "Groin";
    itemdata.system.toHitRoll18 = "-";
    itemdata.system.toHitRoll6 = "-";
    itemdata.system.toHitPenalty = -3;
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async arms(itemdata) {
    this.shoulders(itemdata);
    this.upperarms(itemdata);
    this.elbows(itemdata);
    this.forearms(itemdata);
  }
  async shoulders(itemdata) {
    itemdata.name = "Right Shoulder";
    itemdata.system.toHitRoll18 = "8";
    itemdata.system.toHitRoll6 = "6";
    itemdata.system.toHitPenalty = -2;
    itemdata.system.toCripple = Number(itemdata.system.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
    itemdata.name = "Left Shoulder";
    itemdata.system.toHitRoll18 = "12";
    itemdata.system.toHitRoll6 = "6";
    itemdata.system.toHitPenalty = -2;
    itemdata.system.toCripple = Number(itemdata.system.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async upperarms(itemdata) {
    itemdata.name = "Right Upper Arm";
    itemdata.system.toHitRoll18 = "8";
    itemdata.system.toHitRoll6 = "5";
    itemdata.system.toHitPenalty = -2;
    itemdata.system.toCripple = Number(itemdata.system.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
    itemdata.name = "Left Upper Arm";
    itemdata.system.toHitRoll18 = "12";
    itemdata.system.toHitRoll6 = "5";
    itemdata.system.toHitPenalty = -2;
    itemdata.system.toCripple = Number(itemdata.system.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async elbows(itemdata) {
    itemdata.name = "Right Elbow";
    itemdata.system.toHitRoll18 = "8";
    itemdata.system.toHitRoll6 = "4";
    itemdata.system.toHitPenalty = -2;
    itemdata.system.toCripple = Number(itemdata.system.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
    itemdata.name = "Left Elbow";
    itemdata.system.toHitRoll18 = "12";
    itemdata.system.toHitRoll6 = "4";
    itemdata.system.toHitPenalty = -2;
    itemdata.system.toCripple = Number(itemdata.system.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async forearms(itemdata) {
    itemdata.name = "Right Forearm";
    itemdata.system.toHitRoll18 = "8";
    itemdata.system.toHitRoll6 = "1,2,3";
    itemdata.system.toHitPenalty = -2;
    itemdata.system.toCripple = Number(itemdata.system.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
    itemdata.name = "Left Forearm";
    itemdata.system.toHitRoll18 = "12";
    itemdata.system.toHitRoll6 = "1,2,3";
    itemdata.system.toHitPenalty = -2;
    itemdata.system.toCripple = Number(itemdata.system.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async hands(itemdata) {
    itemdata.name = "Right Hand";
    itemdata.system.toHitRoll18 = "15";
    itemdata.system.toHitRoll6 = "1,3,5";
    itemdata.system.toHitPenalty = -4;
    itemdata.system.toCripple = Number(itemdata.system.hp / 3);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
    itemdata.name = "Left Hand";
    itemdata.system.toHitRoll18 = "15";
    itemdata.system.toHitRoll6 = "2,4,6";
    itemdata.system.toHitPenalty = -4;
    itemdata.system.toCripple = Number(itemdata.system.hp / 3);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async legs(itemdata) {
    this.thighs(itemdata);
    this.knees(itemdata);
    this.shins(itemdata);
  }
  async thighs(itemdata) {
    itemdata.name = "Right Thigh";
    itemdata.system.toHitRoll18 = "6,7";
    itemdata.system.toHitRoll6 = "5,6";
    itemdata.system.toHitPenalty = -2;
    itemdata.system.toCripple = Number(itemdata.system.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
    itemdata.name = "Left Thigh";
    itemdata.system.toHitRoll18 = "13,14";
    itemdata.system.toHitRoll6 = "5,6";
    itemdata.system.toHitPenalty = -2;
    itemdata.system.toCripple = Number(itemdata.system.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async knees(itemdata) {
    itemdata.name = "Right Knee";
    itemdata.system.toHitRoll18 = "6,7";
    itemdata.system.toHitRoll6 = "4";
    itemdata.system.toHitPenalty = -2;
    itemdata.system.toCripple = Number(itemdata.system.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
    itemdata.name = "Left Knee";
    itemdata.system.toHitRoll18 = "13,14";
    itemdata.system.toHitRoll6 = "4";
    itemdata.system.toHitPenalty = -2;
    itemdata.system.toCripple = Number(itemdata.system.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async shins(itemdata) {
    itemdata.name = "Right Shin";
    itemdata.system.toHitRoll18 = "6,7";
    itemdata.system.toHitRoll6 = "1,2,3";
    itemdata.system.toHitPenalty = -2;
    itemdata.system.toCripple = Number(itemdata.system.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
    itemdata.name = "Left Shin";
    itemdata.system.toHitRoll18 = "13,14";
    itemdata.system.toHitRoll6 = "1,2,3";
    itemdata.system.toHitPenalty = -2;
    itemdata.system.toCripple = Number(itemdata.system.hp / 2);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
  async feet(itemdata) {
    itemdata.name = "Right Foot";
    itemdata.system.toHitRoll18 = "16";
    itemdata.system.toHitRoll6 = "1,3,5";
    itemdata.system.toHitPenalty = -4;
    itemdata.system.toCripple = Number(itemdata.system.hp / 3);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
    itemdata.name = "Left Foot";
    itemdata.system.toHitRoll18 = "16";
    itemdata.system.toHitRoll6 = "2,4,6";
    itemdata.system.toHitPenalty = -4;
    itemdata.system.toCripple = Number(itemdata.system.hp / 3);
    await this.actor.createEmbeddedDocuments('Item', [itemdata]);
  }
}
