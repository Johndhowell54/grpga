import { system } from "../config.js";
import { baseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {baseActorSheet}
 */
export class ActorD120Sheet extends baseActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorD120-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-nav", contentSelector: ".sheet-body", initial: "combat" }]
    });
  }

  /** @override */
  getData(options) {
    if (CONFIG.system.testMode) console.debug("entering getData() in ActorD120-sheet");

    const context = super.getData();
    // My shortcuts
    const actordata = context.system;
    const traits = actordata.traits;
    const dynamic = actordata.dynamic;
    const tracked = actordata.tracked;
    const headinfo = actordata.headerinfo = {};

    headinfo.level = dynamic.level;
    if (headinfo.level)
      headinfo.level.system.label = system.d120.headerVariables[2];
    headinfo.perception = dynamic.perception;
    headinfo.initiative = dynamic.initiative;
    headinfo.dodge = dynamic.dodge;
    headinfo.xp = dynamic.xp;
    if (headinfo.xp)
      headinfo.xp.system.label = system.d120.headerVariables[1];
    // for d20
    headinfo.hitpoints = tracked.hp;
    headinfo.apr = dynamic.apr;
    headinfo.mpa = dynamic.mpa;
    headinfo.pace = dynamic.pace;
    headinfo.mr = tracked.mr;

    headinfo.race = traits.race;
    headinfo.characterclass = traits.character_class;
    headinfo.specificinfo = traits.specific_information;
    headinfo.additionalinfo = traits.additional_information;
    headinfo.alignment = traits.alignment;

    if (headinfo.alignment)
      headinfo.alignment.system.label = system.d120.headerVariables[0];

      actordata.primaryattributes = [];
      for (let varname of system[context.mode].primaryAttributes) {
        if (!varname.trim()) continue;
        if (dynamic[varname]) actordata.primaryattributes.push(dynamic[varname]);
      }
  
    if (actordata.useAlternity) {
      actordata.allskills = this.sort(actordata.checks.concat(actordata.skills, actordata.spells));
      for (let item of actordata.allskills) {
        item.system.stepvalue = item.system.moddedvalue - item.system.value;
      }
      actordata.defences = this.sort(actordata.dodges.concat(actordata.parrys));
      for (let item of actordata.defences) {
        item.system.stepvalue = item.system.moddedvalue - item.system.value;
      }
    } else if (actordata.useSineNomine) {
      actordata.defences = this.sort(actordata.dodges.concat(actordata.parrys));
      for (let item of actordata.defences) {
        item.system.value = 16 - item.system.value;
        item.system.moddedvalue = 16 - item.system.moddedvalue;
      }
    }

    // group and sort skill mods
    actordata.checkSkillSpellMods = this.sort(Array.from(new Set(actordata.checkmods.concat(actordata.skillmods, actordata.spellmods))));

    return context;
  }

  sort(items) {
    return items.sort((a, b) => {
      if (a.sort < b.sort) return -1;
      if (a.sort > b.sort) return 1;
      return 0;
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.importPAL').click(this._onImportPalData.bind(this));
  }

  async dropData(dragItem) {
    if (CONFIG.system.testMode) console.debug(`processing ${dragItem.type}\n`, dragItem);
    let itemdata = {};
    switch (dragItem.type) {
      case "dynamic": {
        const chatmessage = await game.collections.get("ChatMessage").get(dragItem.id);
        const itemdata = chatmessage.getFlag("grpga", "itemdata");
        const item = await this.actor.createEmbeddedDocuments("Item", [itemdata]);
        const baseitem = await this.actor.items.get(item[0].id);
        baseitem.sheet.render(true);
        break;
      }
      case "critical": {
        data = {
          name: `${dragItem.condition} [${dragItem.effect}]`,
          type: "Defence",
          system: {
            chartype: this.actor.type,
            category: "block",
            notes: `${dragItem.woundtitle}\n${dragItem.message}`
          }
        }
        let item = await this.actor.createEmbeddedDocuments("Item", [itemdata]);
        const baseitem = await this.actor.items.get(item[0].id);
        baseitem.sheet.render(true);
        break;
      }
      case "damage": { // apply damage so it will update status and bar
        await this.actor.modifyTokenAttribute("tracked.hp", -dragItem.hits, true);
        break;
      }
    }
  }

  async _onImportPalData(event) {
    event.preventDefault();
    let temp = this.actor.system.itemscript;
    if (temp[0] == "\"") { // remove the wrapping doublequotes
      temp = temp.substr(1, temp.length - 2);
    }
    const scriptdata = temp.split(/\r?\n/).map(word => word.trim());

    let biography = this.actor.system.biography;

    let ammoIndex = 0;
    for (let entry of scriptdata) {
      let regex = new RegExp(/([^:]+):([^\r|\n]+)/gi);
      const line = regex.exec(entry);

      switch (line[1].trim()) {
        case "Name": {
          // Name: {{name}}
          await this.actor.update({ 'name': line[2].trim() });
          break;
        }
        case "Attribute": {
          // Attribute: {{name}} ### {{abbr || slugify(name)}} ### {{attr || 0}} ### {{sort || 0}} ### {{notes || empty}}
          // Attribute: Intelligence Quotient ### IQ ### 12 ### 1 ### Your ability to reason
          let itemdata = {
            type: "Primary-Attribute",
            system: {
              chartype: "CharacterD120",
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.abbr = result[1] || this.actor.slugify(result[0]);
          itemdata.system.attr = Number(result[2]) || 0;
          itemdata.system.sort = Number(result[3]) || 50;
          itemdata.system.notes = result[4] || "";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Skill": {
          // Skill: {{name}} ### {{"VarForm" || "D20" || "D100"}} ### {{formula || "0"}} ### {{sort || 50}} ### {{notes || empty}}
          // Skill: Athletics ### D100 ### 45 ### 3 ### Training in vigorous exertion for competition
          let itemdata = {
            type: "Rollable",
            system: {
              chartype: "CharacterD120"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          if (!result[1]) {
            itemdata.system.category = "spell";
          } else {
            switch (result[1].toLowerCase()) {
              case "varform":
                itemdata.system.category = "check";
                break;
              case "d20":
                itemdata.system.category = "skill";
                break;
              default:
                itemdata.system.category = "spell";
            }
          }
          itemdata.system.formula = result[2] || "0";
          itemdata.system.sort = Number(result[3]) || 50;
          itemdata.system.notes = result[4] || "";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Information": {
          // Information: {{name}} ### {{formula || "0"}} ### {{sort || 50}} ### {{notes || empty}}
          let itemdata = {
            type: "Rollable",
            system: {
              chartype: "CharacterD120",
              category: "technique"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = result[1] || "0";
          itemdata.system.sort = Number(result[2]) || 50;
          itemdata.system.notes = result[3] || "You should be putting html formatted content here to be displayed in the chat log.";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Reference": {
          // Reference: {{name}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let itemdata = {
            type: "Rollable",
            system: {
              chartype: "CharacterD120",
              category: "rms"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = result[1] || "0";
          itemdata.system.sort = Number(result[2]) || 50;
          itemdata.system.notes = result[3] || "";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Defence": {
          // Defence: {{name}} ### {{"D20" || "D100"}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let itemdata = {
            type: "Defence",
            system: {
              chartype: "CharacterD120"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          if (!result[1]) {
            itemdata.system.category = "parry";
          } else {
            switch (result[1].toLowerCase()) {
              case "d20":
                itemdata.system.category = "dodge";
                break;
              default:
                itemdata.system.category = "parry";
            }
          }
          itemdata.system.formula = result[2] || "0";
          itemdata.system.sort = Number(result[3]) || 50;
          itemdata.system.notes = result[4] || "";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Trait": {
          // Trait: {{name}} ### {{category}} ### {{notes}} ### {{sort || 0}}
          let itemdata = {
            type: "Trait",
            system: {
              chartype: "CharacterD120"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.category = result[1].toLowerCase();
          itemdata.system.sort = Number(result[2]) || 50;
          itemdata.system.notes = result[3] || "";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "SharedValue": {
          // SharedValue: {{name}} ### {{formula || "0"}} ### {{sort || 0}} ### {{notes || empty}}
          let itemdata = {
            type: "Defence",
            system: {
              chartype: "CharacterD120",
              category: "block"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = result[1] || "0";
          itemdata.system.sort = Number(result[2]) || 50;
          itemdata.system.notes = result[3] || "";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Pool": {
          // Pool: {{name}} ### {{abbr}} ### {{max}} ### {{value}} ### {{min}} ### {{sort || 50}} ### {{notes || empty}}
          let itemdata = {
            type: "Pool",
            system: {
              chartype: "CharacterD120"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.abbr = result[1];
          itemdata.system.maxForm = result[2];
          itemdata.system.value = Number(result[3]);
          itemdata.system.minForm = result[4];
          itemdata.system.sort = Number(result[5]) || 50;
          itemdata.system.notes = result[6] || "";
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Attack": {
          // Attack: {{name}} ### {{sort || 50}} ### {{range}} ### {{damage}} ### {{rof}} ### {{payload}} ### {{ammo}} ### {{damage class}}
          let itemdata = {
            system: {
              chartype: "CharacterD120",
              armourDiv: 20,
              minST: "x2"
            }
          }
          let result = line[2].split("###").map(word => word.trim());
          itemdata.system.sort = Number(result[1]) || 50;
          itemdata.system.damage = result[3];
          itemdata.system.damageType = result[7];

          if (result[2] == "0") { // melee
            itemdata.type = "Melee-Attack";
            itemdata.name = `Melee: ${result[0]}`;
            itemdata.system.formula = "@strike-bonus";
          } else { // ranged
            itemdata.type = "Ranged-Attack";
            itemdata.name = `Ranged: ${result[0]}`;
            itemdata.system.formula = "0";
            itemdata.system.accuracy = result[4];
            itemdata.system.range = result[2];
            if (result[5] != "0") { // ammunition pool required
              let ammodata = {
                name: `${result[0]} Ammunition`,
                type: "Pool",
                system: {
                  abbr: `Ammo${++ammoIndex}`,
                  chartype: "CharacterD120",
                  minForm: "0",
                  value: Number(result[5]),
                  maxForm: result[5],
                  notes: `Reloads: ${result[6]}`
                }
              }
              const item = this.actor.system.items.find(i => i.name === ammodata.name);
              if (item) {
                // do not override the existing saved ammunition tracking
              } else {
                await this.actor.createEmbeddedDocuments('Item', [ammodata]);
              }
            }
          }
          const item = this.actor.system.items.find(i => i.name === itemdata.name);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Armor Type": { // Rollable: check
          let num = line[2].trim();
          const item = this.actor.system.items.find(i => i.name === "Armor Type");
          const update = { _id: item.id, 'system.formula': num, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Level": { // Rollable: check
          let num = line[2].trim();
          const item = this.actor.system.items.find(i => i.name.startsWith("Level-Ranks"));
          const update = { _id: item.id, 'system.formula': num, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Race": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.system.items.find(i => i.name.startsWith("Race"));
          const update = { _id: item.id, 'name': `Race: ${str}`, 'system.formula': 0, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "CharacterClass": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.system.items.find(i => i.name.startsWith("Culture"));
          const update = { _id: item.id, 'name': `Culture: ${str}`, 'system.formula': 0, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Occupation": { // Rollable: check
          let str = line[2].trim();
          const item = this.actor.system.items.find(i => i.name.startsWith("Profession"));
          const update = { _id: item.id, 'name': `Profession: ${str}`, 'system.formula': 0, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Psionics": {
          // Psionics: {{name}} ### {{sort || 50}} ### {{ispcost}} ### {{range}} ### {{duration}} ### {{effects}}
          let itemdata = {
            type: "Rollable",
            system: {
              chartype: "CharacterD120",
              category: "technique"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = "0";
          itemdata.system.sort = Number(result[1]) || 50;
          itemdata.system.notes = `<hr><div><b>ISP Cost:</b> ${result[2]}</div><div><b>Range:</b> ${result[3]}</div><div><b>Duration:</b> ${result[4]}</div><div><b>Effects:</b> ${result[5]}</div>`;
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Magic": {
          // Magic: {{name}} ### {{sort || 50}} ### {{ppecost}} ### {{range}} ### {{duration}} ### {{saves}} ### {{effects}}
          let itemdata = {
            type: "Rollable",
            system: {
              chartype: "CharacterD120",
              category: "technique"
            }
          };
          let result = line[2].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = "0";
          itemdata.system.sort = Number(result[1]) || 50;
          itemdata.system.notes = `<hr><div><b>PPE Cost:</b> ${result[2]}</div><div><b>Range:</b> ${result[3]}</div><div><b>Duration:</b> ${result[4]}</div><div><b>Saves:</b> ${result[5]}</div><div><b>Effects:</b> ${result[6]}</div>`;
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Melee": {
          // Melee: {{name}} ### {{sort}} ### {{initiative}} ### {{actions}} ### {{strike}}  ### {{parry}}  ### {{dodge}}  ### {{autododge}}  ### {{disarm}}  ### {{entangle}}  ### {{damage || "0"}}  ### {{pullpunch}}  ### {{rollwithpunch}}  ### {{notes  || empty}}  ### {{level}}
          let result = line[2].split("###").map(word => word.trim());
          const name = `[M]-${result[0]}`;
          const sort = result[1];
          const initiative = result[2];
          const actions = result[3];
          const strike = result[4];
          const parry = result[5];
          const dodge = result[6];
          const adodge = result[7];
          const disarm = result[8];
          const entangle = result[9];
          const damage = result[10] || "0";
          const pullpunch = result[11];
          const rollpunch = result[12];
          const notes = `${result[13]}\nLevel: ${result[14]}`;

          let nextEntry = 0;
          let itemdata = {
            name: name,
            type: "Modifier",
            system: {
              chartype: "CharacterD120",
              notes: notes,
              sort: Number(sort),
              inEffect: false,
              alwaysOn: false,
              attack: false,
              defence: false,
              skill: false,
              spell: false,
              check: false,
              reaction: false,
              damage: false,
              primary: false,
              entries: {
                0: {
                  value: 0,
                  formula: "",
                  category: "attack",
                  targets: ""
                }
              }
            }
          };

          if (initiative != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(initiative),
              formula: initiative,
              category: "defence",
              targets: "Initiative"
            }
            itemdata.system.defence = true;
          }
          if (actions != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(actions),
              formula: actions,
              category: "primary",
              targets: "APR"
            }
            itemdata.system.primary = true;
          }
          if (strike != 0) {
            itemdata.system.entries[nextEntry++] = {
              value: Number(strike),
              formula: strike,
              category: "attack",
              targets: "Melee"
            }
            itemdata.system.attack = true;
          }
          if (parry != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(parry),
              formula: parry,
              category: "defence",
              targets: "Parry"
            }
            itemdata.system.defence = true;
          }
          if (dodge != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(dodge),
              formula: dodge,
              category: "defence",
              targets: "Dodge"
            }
            itemdata.system.defence = true;
          }
          if (adodge != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(adodge),
              formula: adodge,
              category: "defence",
              targets: "Auto Dodge"
            }
            itemdata.system.defence = true;
          }
          if (disarm != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(disarm),
              formula: disarm,
              category: "defence",
              targets: "Disarm"
            }
            itemdata.system.defence = true;
          }
          if (entangle != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(entangle),
              formula: entangle,
              category: "defence",
              targets: "Entangle"
            }
            itemdata.system.defence = true;
          }
          if (damage != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(damage),
              formula: damage,
              category: "damage",
              targets: ""
            }
            itemdata.system.damage = true;
          }
          if (pullpunch != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(pullpunch),
              formula: pullpunch,
              category: "defence",
              targets: "Pull"
            }
            itemdata.system.defence = true;
          }
          if (rollpunch != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(rollpunch),
              formula: rollpunch,
              category: "defence",
              targets: "Roll"
            }
            itemdata.system.defence = true;
          }

          if (!itemdata.system.entries[0]) break;
          const item = this.actor.system.items.find(i => i.name === name);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Ranged": {
          // Ranged: {{name}} ### {{sort}} ### {{initiative}} ### {{actions}} ### {{strike}}  ### {{burst}}  ### {{called}}  ### {{disarm}}  ### {{entangle}}  ### {{damage || "0"}}  ### {{level}}
          let result = line[2].split("###").map(word => word.trim());
          const name = `[R]-${result[0]}`;
          const sort = result[1];
          const initiative = result[2];
          const actions = result[3];
          // the value for strike is derived from the Aim total
          const strike = (result[4] != "0") ? Number(result[4]) - 2 : 0;
          const burst = result[5];
          const called = result[6];
          const disarm = result[7];
          const entangle = result[8];
          const damage = result[9] || "0";
          const notes = `Level: ${result[10]}`;

          let nextEntry = 1;
          let itemdata = {
            name: name,
            type: "Modifier",
            system: {
              chartype: "CharacterD120",
              notes: notes,
              sort: Number(sort),
              inEffect: false,
              alwaysOn: false,
              attack: false,
              defence: false,
              skill: false,
              spell: false,
              check: false,
              reaction: false,
              damage: false,
              primary: false,
              entries: {
                0: {
                  value: 0,
                  formula: "",
                  category: "attack",
                  targets: ""
                }
              }
            }
          };
          if (initiative != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(initiative),
              formula: initiative,
              category: "defence",
              targets: "Initiative"
            }
            itemdata.system.defence = true;
          }
          if (actions != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(actions),
              formula: actions,
              category: "primary",
              targets: "APR"
            }
            itemdata.system.primary = true;
          }
          if (strike != 0) {
            itemdata.system.entries[nextEntry++] = {
              value: Number(strike),
              formula: strike,
              category: "attack",
              targets: "Ranged"
            }
            itemdata.system.attack = true;
          }
          if (burst != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(burst),
              formula: burst,
              category: "primary",
              targets: "Burst"
            }
            itemdata.system.primary = true;
          }
          if (called != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(called),
              formula: called,
              category: "primary",
              targets: "Called"
            }
            itemdata.system.primary = true;
          }
          if (disarm != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(disarm),
              formula: disarm,
              category: "defence",
              targets: "Disarm"
            }
            itemdata.system.defence = true;
          }
          if (entangle != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(entangle),
              formula: entangle,
              category: "defence",
              targets: "Entangle"
            }
            itemdata.system.defence = true;
          }
          if (damage != "0") {
            itemdata.system.entries[nextEntry++] = {
              value: Number(damage),
              formula: damage,
              category: "damage",
              targets: ""
            }
            itemdata.system.damage = true;
          }

          if (!itemdata.system.entries[0]) break;
          const item = this.actor.system.items.find(i => i.name === name);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
      }
    }
    await this.actor.update({ 'system.biography': biography });
    await this.actor.update({ 'system.itemscript': '' });
    ui.notifications.info("Stat block import complete");
    return;
  }

  /**
   * Fetches the formatted modifiers for a roll.
   */
  fetchRollModifiers(actordata, dataset) {
    let tempMods = [];
    const actormods = actordata.items.filter(i => (i.type == "Modifier"));
    const type = (dataset.type == "modlist") ? dataset.modtype : dataset.type;

    // preload gmod
    tempMods.push({ modifier: Number(actordata.system.gmod.value) || 0, description: 'global modifier' });

    // iterate through the modifiers in effect, picking the appropriate ones for the roll
    for (const mod of actormods) {
      const moddata = mod.system;

      if (moddata.inEffect) {

        for (const entry of moddata.entries) {
          // check to see if this entry applies to this type of roll
          switch (type) {
            case "technique":
              if (entry.category != "skill") continue;
              break;
            case "rms":
              if (entry.category != "spell") continue;
              break;
            case "dodge":
            case "block":
            case "parry":
              if (entry.category != "defence") continue;
              break;
            default:
              if (entry.category != type) continue;
          }
          if (Number(entry.moddedformula) == 0) continue;

          // what is the target of this modifier?
          let hasRelevantTarget = entry.targets.trim();
          // does this modifier have a target matching the item being rolled?
          if (hasRelevantTarget != "") {
            hasRelevantTarget = false;
            // get the array of targets to which it applies
            const cases = entry.targets.split(",").map(word => word.trim());
            for (const target of cases) {
              // test the target against the beginning of dataset.name for a match
              if (dataset.name.startsWith(target)) {
                hasRelevantTarget = true;
                continue;
              }
            }
          } else {
            // a general modifier
            hasRelevantTarget = true;
          }

          if (hasRelevantTarget) {
            let clean = entry.moddedformula?.replace(/\(([-+][\d]+)\)/g, "$1");
            tempMods.push({
              modifier: (clean == undefined) ? entry.value : clean[0] == "+" || clean[0] == "-" ? clean : "+" + clean,
              description: moddata.tempName
            });
          }
        }
      }
    }

    const prepareModList = mods => mods.map(mod => ({ ...mod, modifier: mod.modifier })).filter(mod => mod.modifier !== 0);

    return prepareModList(tempMods);
  }
}
