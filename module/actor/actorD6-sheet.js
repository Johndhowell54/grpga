import { system } from "../config.js";
import { baseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {baseActorSheet}
 */
export class ActorD6Sheet extends baseActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorD6-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-nav", contentSelector: ".sheet-body", initial: "combat" }]
    });
  }

  /** @override */
  getData(options) {
    if (CONFIG.system.testMode) console.debug("entering getData() in ActorD6-sheet");

    const context = super.getData();
    // My shortcuts
    const actordata = context.system;
    const traits = actordata.traits;
    const dynamic = actordata.dynamic;
    const tracked = actordata.tracked;
    const headinfo = actordata.headerinfo = {};

    for (let item of actordata.disadvantages) {
      switch (item?.name) {
        case "Race": headinfo.race = item; break;
        case "Quote": headinfo.quote = item; break;
        case "Personality": headinfo.personality = item; break;
        case "Description": headinfo.description = item; break;
      }
    }
    if (headinfo.race)
      headinfo.race.system.label = system.d6.headerVariables[0];
      if (headinfo.personality)
      headinfo.personality.system.label = system.d6.headerVariables[1];
      if (headinfo.quote)
      headinfo.quote.system.label = system.d6.headerVariables[2];
    headinfo.cp = dynamic.cp;
    if (headinfo.cp)
      headinfo.cp.system.label = system.d6.headerVariables[3];
    headinfo.hp = dynamic.hp;
    if (headinfo.hp)
      headinfo.hp.system.label = system.d6.headerVariables[4];
    if (headinfo.description)
      headinfo.description.system.label = system.d6.headerVariables[5];

    actordata.primaryattributes = [];
    for (let varname of system[context.mode].primaryAttributes) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) {
        actordata.primaryattributes.push(dynamic[varname]);
        dynamic[varname].system.diceandpips = this.actor.toDiceAndPips(dynamic[varname].system.moddedvalue, false);
      }
    }

    for (let skill of actordata.skills) {
      const group = skill.system.group.trim().toLowerCase();
      if (dynamic[group]) {
        skill.system.diceandpips = this.actor.toDiceAndPips(skill.system.moddedvalue + dynamic[group].system.moddedvalue, false);
        skill.system.value += dynamic[group].system.moddedvalue;
        if (dynamic[group].system.skills) {
          if (!dynamic[group].system.skills.includes(skill)) dynamic[group].system.skills.push(skill);
        } else {
          dynamic[group].system.skills = [skill];
        }
      } else {
        // no such primary found
      }
    }
    for (let defence of actordata.defences) {
      defence.system.diceandpips = this.actor.toDiceAndPips(defence.system.moddedvalue, false);
    }
    // group and sort skill mods
    actordata.checkSkillSpellMods = this.sort(Array.from(new Set(actordata.checkmods.concat(actordata.skillmods, actordata.spellmods))));

    // check to see if specific modifiers are toggled so we can set the flags
    // the modifier must affect Variable Formulae, D20 skills or D100 skills to work as a toggle (a modifier of zero is ok)
    // these were designed for D6 Pool ruleset but could be used by others
    for (let item of actordata.checkSkillSpellMods) {
      switch (item?.name) {
        case "Rushed": this.actor.setFlag("grpga", "rushed", item.system.inEffect); break;
        case "Using Edge": this.actor.setFlag("grpga", "usingedge", item.system.inEffect); break;
      }
    }

    return context;
  }

  sort(items) {
    return items.sort((a, b) => {
      if (a.sort < b.sort) return -1;
      if (a.sort > b.sort) return 1;
      return 0;
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
  }

  async dropData(dragItem) {
    if (CONFIG.system.testMode) console.debug(`processing ${dragItem.type}\n`, dragItem);
    let itemdata = {};
    switch (dragItem.type) {
      case "dynamic": {
        const chatmessage = await game.collections.get("ChatMessage").get(dragItem.id);
        const itemdata = chatmessage.getFlag("grpga", "itemdata");
        const item = await this.actor.createEmbeddedDocuments("Item", [itemdata]);
        const baseitem = await this.actor.items.get(item[0].id);
        baseitem.sheet.render(true);
        break;
      }
      case "critical": {
        data = {
          name: `${dragItem.condition} [${dragItem.effect}]`,
          type: "Defence",
          system: {
            chartype: this.actor.type,
            category: "block",
            notes: `${dragItem.woundtitle}\n${dragItem.message}`
          }
        }
        let item = await this.actor.createEmbeddedDocuments("Item", [itemdata]);
        const baseitem = await this.actor.items.get(item[0].id);
        baseitem.sheet.render(true);
        break;
      }
      case "damage": { // apply damage so it will update status and bar
        await this.actor.modifyTokenAttribute("tracked.hp", -dragItem.hits, true);
        break;
      }
    }
  }

  /**
   * Fetches the formatted modifiers for a roll.
   */
  fetchRollModifiers(actordata, dataset) {
    let tempMods = [];
    const actormods = actordata.items.filter(i => (i.type == "Modifier"));
    const type = (dataset.type == "modlist") ? dataset.modtype : dataset.type;

    // preload gmod
    tempMods.push({ modifier: Number(actordata.system.gmod.value) || 0, description: 'global modifier' });

    // iterate through the modifiers in effect, picking the appropriate ones for the roll
    for (const mod of actormods) {
      const moddata = mod.system;

      if (moddata.inEffect) {

        for (const entry of moddata.entries) {
          // check to see if this entry applies to this type of roll
          switch (type) {
            case "technique":
              if (entry.category != "skill") continue;
              break;
            case "rms":
              if (entry.category != "spell") continue;
              break;
            case "dodge":
            case "block":
            case "parry":
              if (entry.category != "defence") continue;
              break;
            default:
              if (entry.category != type) continue;
          }
          if (Number(entry.moddedformula) == 0) continue;

          // what is the target of this modifier?
          let hasRelevantTarget = entry.targets.trim();
          // does this modifier have a target matching the item being rolled?
          if (hasRelevantTarget != "") {
            hasRelevantTarget = false;
            // get the array of targets to which it applies
            const cases = entry.targets.split(",").map(word => word.trim());
            for (const target of cases) {
              // test the target against the beginning of dataset.name for a match
              if (dataset.name.startsWith(target)) {
                hasRelevantTarget = true;
                continue;
              }
            }
          } else {
            // a general modifier
            hasRelevantTarget = true;
          }

          if (hasRelevantTarget) {
            let clean = entry.moddedformula?.replace(/\(([-+][\d]+)\)/g, "$1");
            tempMods.push({
              modifier: (clean == undefined) ? entry.value : clean[0] == "+" || clean[0] == "-" ? clean : "+" + clean,
              description: moddata.tempName
            });
          }
        }
      }
    }

    const prepareModList = mods => mods.map(mod => ({ ...mod, modifier: mod.modifier })).filter(mod => mod.modifier !== 0);

    return prepareModList(tempMods);
  }
}
