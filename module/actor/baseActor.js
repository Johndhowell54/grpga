import { ActorMnM } from "./actorMnM.js";
import { ActorD120 } from "./actorD120.js";
import { ActorD100 } from "./actorD100.js";
import { Actor3D6 } from "./actor3D6.js";
import { ActorBBF } from "./actorBBF.js";
import { ActorD6 } from "./actorD6.js";
import { MyDialog } from "../dialog.js";

/**
 * Extend the base Actor document by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class BaseActor extends Actor {
  /**
   * Fetch and sort all the items, even though they will be prepared internally after this stage.
   */
  prepareBaseData() {
    const actordata = this.system;
    if (CONFIG.system.testMode)
      console.debug("entering prepareBaseData()\n", [this, actordata]);

    actordata.items = Array.from(this.items.values()).sort((a, b) => {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });

    /**
     * Reference data structure to mimic static systems
     */
    actordata.dynamic = {};
    actordata.traits = {};
    actordata.tracked = {};
    actordata.modifiers = {};

    actordata.defence = {};
    actordata.rulesetActor = null;

    // set some basic values
    actordata.dynamic.scale = {
      value: game.scenes?.current?.grid.distance || 5,
    };
    actordata.dynamic.visionType = "normal";
    actordata.ruleset = game.settings.get("grpga", "rulesetChoice"); // 3d6, d120, d100, mnm, bbf
    actordata.useFAHR = game.settings.get("grpga", "useFAHR");
    actordata.autoCrit = game.settings.get("grpga", "autoCriticalRolls");

    switch (this.type) {
      case "Character3D6": {
        actordata.mode = "3d6";
        actordata.rulesetActor = new Actor3D6(this);
        break;
      }
      case "CharacterD120": {
        actordata.mode = "d120";
        actordata.rulesetActor = new ActorD120(this);
        break;
      }
      case "CharacterBBF": {
        actordata.mode = "bbf";
        actordata.rulesetActor = new ActorBBF(this);
        break;
      }
      case "CharacterD6": {
        actordata.mode = "d6";
        actordata.rulesetActor = new ActorD6(this);
        break;
      }
      case "CharacterMnM": {
        actordata.mode = "mnm";
        actordata.rulesetActor = new ActorMnM(this);
        break;
      }
      case "CharacterD100": {
        actordata.mode = "d100";
        actordata.rulesetActor = new ActorD100(this);
        break;
      }
    }
    actordata.rulesetActor.prepareBaseData();
  }

  /**
   * Augment the basic actor data with additional dynamic data.
   *
   * Changes made here to item seem to stick.
   */
  prepareDerivedData() {
    // delegate the preparation to each ruleset
    this.system.rulesetActor.prepareAdditionalData();
  }

  processModifierFormula(entry, item) {
    if (!Number.isNumeric(entry.formula) && entry.formula != "") {
      const formData = this._replaceData(entry.formula);
      try {
        entry.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, "")));
        entry.moddedformula = "" + entry.value; // store the result as a String
      } catch (err) {
        // store the formula ready to be rolled
        entry.moddedformula = formData.value;
        entry.value = 0; // eliminate any old values
        console.warn("Modifier formula evaluation error:\n", [this.name, item.name, entry.moddedformula,]);
      }
      if (!item.system.tempName.includes(formData.label)) item.system.tempName += formData.label;
    }
  }

  getNamedItem(name) {
    let parts = name.split(".");
    if (parts.length != 2) return undefined;
    return this.system[parts[0]][parts[1]];
  }

  slugify(text) {
    return text
      .toString() // Cast to string
      .toLowerCase() // Convert the string to lowercase letters
      .normalize("NFD") // The normalize() method returns the Unicode Normalization Form of a given string.
      .trim() // Remove whitespace from both sides of a string
      .replace(/[\s-]+/g, "_") // Replace spaces with underscore
      .replace(/[^\w]+/g, "") // Remove all non-word chars
      .replace(/\_\_+/g, "_"); // Replace multiple underscores with a single one
  }

  rankCalculator(ranks, progression = "0:1:1:1:1") {
    if (progression == "0:1:1:1:1")
      return ranks;
    const tiers = progression.split(":");
    const base = Number(tiers[0]);
    const t0 = Number(tiers[1]);
    const t10 = Number(tiers[2]);
    const t20 = Number(tiers[3]);
    const t30 = Number(tiers[4]);

    if (ranks > 30) {
      return 10 * (t0 + t10 + t20) + (ranks - 30) * t30;
    } else if (ranks > 20) {
      return 10 * (t0 + t10) + (ranks - 20) * t20;
    } else if (ranks > 10) {
      return 10 * t0 + (ranks - 10) * t10;
    } else if (ranks > 0) {
      return ranks * t0;
    } else {
      return base;
    }
  }

  /**
   * This function converts an integer into a Dice + Pips string
   */
  toDiceAndPips(pips, seefaces = true, faces = 6, pipsize = 3) {
    return `${~~(pips / pipsize)}D${(seefaces) ? faces : ''}${(pips % pipsize) == 0 ? `` : `+${pips % pipsize}`}`;
  }

  /**
   * Replace data references in the formula of the syntax `@attr[.fieldname =.moddedvalue]` with the corresponding key from
   * the provided `data` object.
   * @param {String} formula    The original formula within which to replace
   * @private
   */
  _replaceData(formula) {
    const dataRgx = /[@]([\w.]+)/gi;
    const dynamic = this.system.dynamic;
    const tracked = this.system.tracked;
    let tempName = "";
    const findTerms = (match, term) => {
      const fields = term.split(".");
      const attribute = fields[0];
      // default to the field "moddedvalue" if none is specified
      const field = fields[1] || "moddedvalue";
      if (dynamic[attribute]) {
        const value = dynamic[attribute].system[field];
        const label = dynamic[attribute].system.label;
        tempName += (label) ? ` (${label})` : "";
        return (value) ? String(value).trim() : "0";
      } else if (tracked[attribute]) {
        const value = tracked[attribute].system[field];
        return (value) ? String(value).trim() : "0";
      } else {
        return "0";
      }
    };
    const replyData = formula.replace(dataRgx, findTerms).replace(/(min)|(max)|(floor)|(ceil)|(round)|(abs)|(pow)/g, "Math.$&");
    return { value: replyData, label: tempName };
  }

  async setConditions(newValue, attrName) {
    if (CONFIG.system.testMode) console.debug("entering setConditions()\n", [newValue, attrName]);

    const tracked = this.system.tracked;
    const attr = attrName.split('.')[2];
    const item = tracked[attr];
    const itemdata = item.system;
    let attrValue = itemdata.value;
    let attrMax = itemdata.max;
    let attrState = itemdata.state;
    let attrMin = itemdata.min;

    // Assign the variables
    if (attrName.includes('.maxForm')) {
      attrMax = Math.round(eval(this._replaceData(newValue).value.replace(CONFIG.system.dataRgx, '')));
    } else if (attrName.includes('.minForm')) {
      attrMin = Math.round(eval(this._replaceData(newValue).value.replace(CONFIG.system.dataRgx, '')));
    } else {
      attrValue = newValue;
    }
    const ratio = attrValue / attrMax;

    switch (attr) {
      case "end": { // Homebrew
        if (attrValue < 1) attrState = '[EXHSTD]';
        else if (ratio < 0.25) attrState = '[BUSHED]';
        else if (ratio < 0.5) attrState = '[TIRED]';
        else if (ratio < 0.75) attrState = '[WINDED]';
        else attrState = '[FIT]';
        break;
      }
      case "vit": { // Star Wars D20
        if (attrValue < 1) attrState = '[EXHSTD]';
        else if (ratio < 0.25) attrState = '[BUSHED]';
        else if (ratio < 0.5) attrState = '[TIRED]';
        else if (ratio < 0.75) attrState = '[WINDED]';
        else attrState = '[FIT]';
        break;
      }
      case "wnd": { // Star Wars D20
        if (attrValue < -9) attrState = '[DEAD]';
        else if (attrValue < 1) attrState = '[UNC]';
        else if (ratio < 0.25) attrState = '[WND]';
        else if (ratio < 0.5) attrState = '[BLD]';
        else if (ratio < 0.75) attrState = '[BRU]';
        else attrState = '[FIT]';
        break;
      }
      case "hp": {
        switch (this.type) {
          case "CharacterD20": { // health state for D20 system
            if (attrValue < -9) attrState = '[DEAD]';
            else if (attrValue < 1) attrState = '[UNC]';
            else if (ratio < 0.25) attrState = '[WND]';
            else if (ratio < 0.5) attrState = '[BLD]';
            else if (ratio < 0.75) attrState = '[BRU]';
            else attrState = '[FIT]';
            break;
          }
          case "CharacterVsD": { // health state for Against the Darkmaster
            if (this.system.useFAHR) { // House rules
              switch (Math.trunc(ratio * 8)) {
                case 1: {
                  attrState = '[1/8 BRU]';
                  break;
                }
                case 2: {
                  attrState = '[1/4 BRU]';
                  break;
                }
                case 3: {
                  attrState = '[3/8 BRU]';
                  break;
                }
                case 4: {
                  attrState = '[1/2]';
                  break;
                }
                case 5: {
                  attrState = '[5/8]';
                  break;
                }
                case 6: {
                  attrState = '[3/4]';
                  break;
                }
                case 7: {
                  attrState = '[7/8]';
                  break;
                }
                case 8: {
                  attrState = '[FIT]';
                  break;
                }
                default: { // bad shape
                  if (ratio <= -1) {
                    attrState = '[DEAD]';
                  } else if (ratio <= -0.5) {
                    attrState = '[DYING]';
                  } else if (ratio <= 0) {
                    attrState = '[INC]';
                  } else {
                    attrState = '[< 1/8 BRU]';
                  }
                }
              }
            } else { // RAW
              if (attrValue < -49) attrState = '[DYING]';
              else if (attrValue < 1) attrState = '[INC]';
              else if (ratio < 0.5) attrState = '[BRU]';
              else attrState = '[FIT]';
            }
            break;
          }
          case "Character3D6": { // health state for 3D6 ruleset
            switch (Math.trunc(ratio)) {
              case 0: {
                if (ratio <= 0) { // collapse
                  attrState = '[C]';
                  break;
                } else if (attrValue < (attrMax / 3)) { // reeling
                  attrState = '[R]';
                  break;
                }
                // healthy, no break
              }
              case 1: { // healthy
                attrState = '[H]';
                break;
              }
              case -1: { // death check at -1
                attrState = '[-X]';
                break;
              }
              case -2: { // death check at -2
                attrState = '[-2X]';
                break;
              }
              case -3: { // death check at -3
                attrState = '[-3X]';
                break;
              }
              case -4: { // death check at -4
                attrState = '[-4X]';
                break;
              }
              default: { // dead
                attrState = '[DEAD]';
                break;
              }
            }
            break;
          }
          default: { // use eighths and let the user set the minimum
            switch (Math.trunc(ratio * 8)) {
              case 1: {
                attrState = '[1/8]';
                break;
              }
              case 2: {
                attrState = '[1/4]';
                break;
              }
              case 3: {
                attrState = '[3/8]';
                break;
              }
              case 4: {
                attrState = '[1/2]';
                break;
              }
              case 5: {
                attrState = '[5/8]';
                break;
              }
              case 6: {
                attrState = '[3/4]';
                break;
              }
              case 7: {
                attrState = '[7/8]';
                break;
              }
              case 8: {
                attrState = '[FIT]';
                break;
              }
              default: { // bad shape
                if (attrValue <= attrMin) {
                  attrState = '[DEAD]';
                } else if (attrValue <= 0) {
                  attrState = '[UNC]';
                } else {
                  attrState = '[< 1/8]';
                }
              }
            }
            break;
          }
        }
        break;
      }
      case "fp": {
        // set the limits
        switch (Math.trunc(ratio)) {
          case 0: {
            if (ratio <= 0) { // collapse
              attrState = '[C]';
              break;
            } else if (attrValue < (attrMax / 3)) { // tired
              attrState = '[T]';
              break;
            }
            // fresh, no break
          }
          case 1: { // fresh
            attrState = '[F]';
            break;
          }
          default: { // unconscious
            attrState = '[UNC]';
            break;
          }
        }
        break;
      }
      default: { // assume that the state is measured in eighths
        switch (Math.trunc(ratio * 8)) {
          case 1: {
            attrState = '[1/8]';
            break;
          }
          case 2: {
            attrState = '[1/4]';
            break;
          }
          case 3: {
            attrState = '[3/8]';
            break;
          }
          case 4: {
            attrState = '[1/2]';
            break;
          }
          case 5: {
            attrState = '[5/8]';
            break;
          }
          case 6: {
            attrState = '[3/4]';
            break;
          }
          case 7: {
            attrState = '[7/8]';
            break;
          }
          case 8: {
            attrState = '[Full]';
            break;
          }
          default: { // dead
            if (ratio <= 0) { // empty
              attrState = '[Empty]';
            } else {
              attrState = '[< 1/8]';
            }
          }
        }
      }
    }
    itemdata.min = attrMin;
    itemdata.value = attrValue;
    itemdata.max = attrMax;
    itemdata.state = attrState;

    return await item.update({ system: itemdata });
  }

  /**
   * Handle how changes to a Token attribute bar are applied to the Actor.
   * Logic for pools to be edited by plus-minus and text input (on Actor and Item) is also redirected here.
   * @param {string} attribute    The attribute path
   * @param {number} value        The target attribute value
   * @param {boolean} isDelta     Whether the number represents a relative change (true) or an absolute change (false)
   * @param {boolean} isBar       Whether the new value is part of an attribute bar, or just a direct value
   * @return {Promise}
   */
  async modifyTokenAttribute(attribute, value, isDelta = false, isBar = true) {
    if (CONFIG.system.testMode) console.debug("entering modifyTokenAttribute()\n", [attribute, value, isDelta, isBar]);

    if (isBar && !attribute.endsWith(".system"))
      attribute += ".system";
    // fetch the pool if the attribute changing is 'value', or the attribute itelf if not
    const current = getProperty(this.system, attribute);
    // isBar is true for the value and must be clamped
    // isBar is false for the min or max
    value = isBar
      ? Math.clamped(current.min, isDelta ? Number(current.value) + Number(value) : Number(value), current.max)
      : isDelta ? Number(current) + Number(value) : value;

    // redirect updates to setConditions
    return await this.setConditions(value, `system.${attribute}`);
  }

  /**
   * Fetches the total modifier value for a rollable item
   */
  fetchDisplayModifiers(name, type, activeModEntries) {

    let mods = 0;

    const modEntries = activeModEntries.filter((ame) => ame.category == type);

    for (const entry of modEntries) {
      // what is the target of this modifier?
      let hasRelevantTarget = entry.targets.trim();
      // does this modifier have a target matching the item being rolled?
      if (hasRelevantTarget != "") {
        hasRelevantTarget = false;
        // get the array of targets to which it applies
        const cases = entry.targets.split(",").map((word) => word.trim());
        for (const target of cases) {
          // test the target against the beginning of dataset.name for a match
          if (name.startsWith(target)) {
            hasRelevantTarget = true;
            continue;
          }
        }
      } else {
        // a general modifier
        hasRelevantTarget = true;
      }
      mods += hasRelevantTarget ? entry.value : 0;
    }
    return mods;
  }

  /**
   * Resets all temporary Variables and Modifiers
   */
  resetModVars(temporary = true) {
    const items = this.items.filter(function (item) {
      if (temporary) {
        return item.system.temporary;
      } else {
        return item.system.once;
      }
    });
    let updates = [];

    for (const item of items) {
      switch (item.type) {
        case "Modifier": {
          updates.push({ _id: item.id, ["system.inEffect"]: false, });
          break;
        }
        case "Variable": {
          updates.push({
            _id: item.id,
            ["system.value"]: item.system.entries[0].value,
            ["system.formula"]: item.system.entries[0].formula,
            ["system.label"]: item.system.entries[0].label,
          });
          break;
        }
      }
    }
    this.updateEmbeddedDocuments("Item", updates);
  }

  /**
   * A method to dispatch non-event-driven rolls.
   */
  async roll(rolldata) {
    this.system.rulesetActor.roll(rolldata);
  }

  /**
   * Pass a filtering object to this method in the form:
   * {
   *  type: "",
   *  category: "",
   *  group: "",
   *  target: "",
   *  position: {
   *   width: 300,
   *   top: 0,
   *   left: 0
   *  }
   * }
   * where the values shown are the defaults and may be omitted
   * if not required.
   *
   * Each field is a comma-separated list of the things you want
   * to include in the result. It matches against the text you
   * enter, so a category of "s" will match skill and rms.
   *
   * type: may be any of {Ability, Melee, Ranged,
   * Attack, Rollable}
   *
   * category: applies only to type:
   *     Rollable: {skill, combatval, defence} and
   * and may be used without specifying the type.
   *
   * group: is a user-defined field for use in filtering
   * this result.
   *
   * target: Any case-sensitive portion of the item name.
   */
  rollables(macroData) {
    macroData.position = macroData.position || { width: 300, top: 0, left: 0 };
    macroData.type = macroData.type || "";
    macroData.category = macroData.category || "";
    macroData.group = macroData.group || "";
    macroData.target = macroData.target || "";
    macroData.flavour = macroData.flavour || "";

    const rollables = this.items
      .filter(function (item) {
        if ("ContainerTraitPoolModifierVariableAbilityEquipment".includes(item.type)) return null;
        if (macroData.target != "") {
          const cases = macroData.target.split(",").map((word) => word.trim());
          if (!cases.some((target) => item.name.includes(target))) return null;
        }
        if (macroData.group != "") {
          const cases = macroData.group.split(",").map((word) => word.trim());
          if (!cases.some((target) => item.system.group.includes(target))) return null;
        }
        if (macroData.category != "") {
          const cases = macroData.category.split(",").map((word) => word.trim());
          if (!cases.some((target) => item.system.category?.includes(target))) return null;
        } else {
          const cases = macroData.type.split(",").map((word) => word.trim());
          if (!cases.some((target) => item.type.includes(target))) return null;
        }
        return item;
      })
      .sort((a, b) => {
        if (a.name < b.name) return -1;
        if (a.name > b.name) return 1;
        return 0;
      });

    switch (rollables.length) {
      case 0:
        ui.notifications.error("Your rollmacro call produced no results");
        break;
      case 1: // go straight to the roll dialog with modifiers
        this.modifierdialog(rollables[0]._id, macroData);
        break;
      default: // provide the choice of results
        this.rollabledialog(rollables, macroData);
        break;
    }
  }

  async rollabledialog(rollableData, macroData) {
    const actor = this;
    const template = "systems/grpga/templates/rollmacros/rollables.hbs";
    const html = await renderTemplate(template, { rollableData });

    new MyDialog({
      title: `Rollables: ${actor.name}`,
      content: html,
      buttons: {},
      options: {
        actor: actor,
        macroData: macroData
      }
    }, {
      width: macroData.position.width || 300,
      top: macroData.position.top || 0,
      left: macroData.position.left || 0
    }).render(true);
  }

  // a test method for executing token actions. SysDev part 8 covers a better way to do this.
  async modifierdialog(rollableId, macroData) {
    const rollableData = this.items.get(rollableId);
    const actor = this;
    const name = rollableData.name;
    const type = rollableData.type;
    const tempcats = {};
    const category =
      type == "Melee-Attack" || type == "Ranged-Attack"
        ? macroData.damage ? "damage" : "attack"
        : rollableData.system.category || null;

    // Is there at least one target selected so we can fetch it's data?
    const hasTarget = game.user.targets.size > 0;
    let targetdata = [];
    let targetname = null;
    if (hasTarget) {
      const target = game.user.targets.first();
      targetname = target.name;
    }
    for (let i = 0; i < targetdata.length; i++) {
      targetdata[i].system.selected = (targetdata[i]._id == macroData.targetId);
    }

    const modifiers = (category == "damage") ? [actor.system.dynamic.critmultiplier] : [actor.system.dynamic.withadvantage];
    for (const mod of Object.values(this.system.modifiers)) {
      for (const entry of mod.system.entries) {
        if (tempcats[entry.category]) {
          tempcats[entry.category]++
        } else {
          tempcats[entry.category] = 1
        }
        if (entry.formula == "") continue;
        // check to see if this entry applies to this type of roll
        switch (type) {
          case "Defence":
            if ("defence" != entry.category) continue;
            break;
          case "Rollable":
            if (category != entry.category) continue;
            break;
          case "Primary-Attribute":
            if ("skill" != entry.category) continue;
            break;
        }

        let hasRelevantTarget = entry.targets.trim();
        // does this modifier have targets?
        if (hasRelevantTarget != "") {
          hasRelevantTarget = false;
          // get the array of targets to which it applies
          const cases = entry.targets.split(",").map(word => word.trim());
          for (const target of cases) {
            // test the target against the beginning of name for a match
            if (name.startsWith(target)) {
              hasRelevantTarget = true;
              continue;
            }
          }
        } else {
          // a general modifier
          hasRelevantTarget = true;
        }
        if (hasRelevantTarget) {
          // does the modifier have a reference?
          if (entry.formula.includes("@") || entry.formula.includes("#")) {
            // is the reference to a Variable or Pool?
            const regex = /[@|#]([\w.]+)/gi;
            const reference = regex.exec(entry.formula)[1];
            const varpool =
              actor.system.tracked[reference] ||
              actor.system.dynamic[reference];
            if (varpool) {
              const vardata = this.items.get(varpool._id);
              if (!modifiers.includes(vardata)) modifiers.push(vardata);
            }
          }
          modifiers.push(mod);
        }
      }
    }
    console.log([name, type, category, tempcats]);
    const uniquemods = [...new Set(modifiers)];

    const template = "systems/grpga/templates/rollmacros/modifiers.hbs";
    const html = await renderTemplate(template, {
      actor,
      rollableData,
      uniquemods,
      hasTarget,
      targetdata,
      targetname,
      category
    });

    new MyDialog({
      title: `Rollable: ${actor.name}`,
      content: html,
      buttons: {
        back: {
          label: "Back",
          callback: (html) => {
            this.rollables(macroData);
          }
        }
      },
      options: {
        actor: actor,
        macroData: macroData,
      }
    }, {
      width: macroData.position.width || 300,
      top: macroData.position.top || 0,
      left: macroData.position.left || 0,
    }).render(true);
  }

  /** @override */
  static async createDialog(data = {}, { parent = null, pack = null, ...options } = {}) {

    // Collect data
    const documentName = this.metadata.name;

    // HERE WE PRESENT AN ABBREVIATED LIST BASED ON RULESET
    const types = [CONFIG.system.chartype]; //game.documentTypes[documentName];

    const folders = parent ? [] : game.folders.filter(f => (f.type === documentName) && f.displayed);
    const label = game.i18n.localize(this.metadata.label);
    const title = game.i18n.format("DOCUMENT.Create", { type: label });

    // Render the document creation form
    const html = await renderTemplate("templates/sidebar/document-create.html", {
      folders,
      name: data.name || game.i18n.format("DOCUMENT.New", { type: label }),
      folder: data.folder,
      hasFolders: folders.length >= 1,
      type: data.type || CONFIG[documentName]?.defaultType || types[0],
      types: types.reduce((obj, t) => {
        const label = CONFIG[documentName]?.typeLabels?.[t] ?? t;
        obj[t] = game.i18n.has(label) ? game.i18n.localize(label) : t;
        return obj;
      }, {}),
      hasTypes: types.length > 1
    });

    // Render the confirmation dialog window
    return MyDialog.prompt({
      title: title,
      content: html,
      label: title,
      callback: html => {
        const form = html[0].querySelector("form");
        const fd = new FormDataExtended(form);
        foundry.utils.mergeObject(data, fd.object, { inplace: true });
        if (!data.folder) delete data.folder;
        if (types.length === 1) data.type = types[0];
        if (!data.name?.trim()) data.name = this.defaultName();
        return this.create(data, { parent, pack, renderSheet: true });
      },
      rejectClose: false,
      options
    });
  }

}
