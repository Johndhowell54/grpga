import { system } from "../config.js";
import { Actor3D6Sheet } from "./actor3D6-sheet.js";

/**
 * Extend the original Actor3D6Sheet
 * @extends {Actor3D6Sheet}
 */
export class Actor3D6FAHRSheet extends Actor3D6Sheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actor3D6FAHR-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-nav", contentSelector: ".sheet-body", initial: "combat" }]
    });
  }

  /** @override */
  getData(options) {
    if (CONFIG.system.testMode) console.debug("entering getData() in ActorD120-sheet");

    const context = super.getData();
    // My shortcuts
    const actordata = context.system;
    const traits = actordata.traits;
    const dynamic = actordata.dynamic;
    const tracked = actordata.tracked;
    const headinfo = actordata.headerinfo = {};

    headinfo.level = dynamic.level;
    headinfo.perception = dynamic.perception;
    headinfo.initiative = dynamic.initiative;
    headinfo.dodge = dynamic.dodge;
    headinfo.xp = dynamic.xp;
    // for d20
    headinfo.hitpoints = tracked.hp;
    headinfo.apr = dynamic.apr;
    headinfo.mpa = dynamic.mpa;
    headinfo.pace = dynamic.pace;
    headinfo.mr = tracked.mr;

    for (let item of actordata.advantages) {
      switch (item?.name) {
        case "Race": headinfo.race = item; break;
        case "Character Class": headinfo.characterclass = item; break;
        case "Specific Information": headinfo.specificinfo = item; break;
        case "Additional Information": headinfo.additionalinfo = item; break;
        case "Alignment": headinfo.alignment = item; break;
      }
    }
    
    actordata.primaryattributes = [];
    for (let varname of system[context.mode].primaryAttributes) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.primaryattributes.push(dynamic[varname]);
    }

    return context;
  }

  sort(items) {
    return items.sort((a, b) => {
      if (a.sort < b.sort) return -1;
      if (a.sort > b.sort) return 1;
      return 0;
    });
  }
}
