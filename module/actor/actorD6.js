import { system } from "../config.js";

/**
 * A utility class defining behaviour for ruleset-specific actors
 */
export class ActorD6 {

  constructor(actor) {
    this.actor = actor;
    this.compendiumName = "Base Items";
  }

  prepareBaseData() {
    const actordata = this.actor.system;
    actordata.hasStatBonusTable = system.hasStatBonusTable();
  }

  prepareAdditionalData() {
    const actor = this.actor;
    const actordata = actor.system;
    if (CONFIG.system.testMode) console.debug("entering prepareDerivedData()\n", [actor, actordata]);

    // for counting the ranks stored in Variable Items for some systems
    let actorranks = {};
    let actorgcranks = {};
    let actorcombranks = {};
    let actorlimranks = {};
    let actorspecranks = {};
    let rankitems = [];
    let rankgcitems = [];
    let rankcombitems = [];
    let ranklimitems = [];
    let rankspecitems = [];

    // group the items for efficient iteration
    const actormods = [];
    const nonmods = [];
    const attackmods = [];
    const primods = [];
    const valuemods = [];
    const values = [];
    const skillmods = [];
    const skills = [];
    const reactionmods = [];
    const rollables = [];
    const primaries = [];

    // store references to primary attribute and pool items
    let previousLayer = [];
    let nextLayer = [];

    // write the items into the model
    for (const item of actordata.items) {
      const itemdata = item.system;
      let itemID = actor.slugify(item.name);
      // collect non-modifier items with formula
      if (itemdata.formula) nonmods.push(item);

      switch (item.type) {
        case "Primary-Attribute": {
          let itemID = actor.slugify(itemdata.abbr);
          // only process primaries with abbreviations
          if (itemID) {
            itemdata.moddedvalue = itemdata.value = Number(itemdata.attr);
            // store a reference to the item in the data structure
            actordata.dynamic[itemID] = item;
            // add this to the processed references
            nextLayer.push(itemID);
            primaries.push(item);
          }
          break;
        }
        case "Primary-Attribute_old": {
          let itemID = actor.slugify(itemdata.abbr);
          if (itemID) {
            // only process primaries with abbreviations
            itemdata.moddedvalue = Number(itemdata.attr);
            itemdata.value = itemdata.moddedvalue;
            // store a reference in the data structure
            actordata.dynamic[itemID] = item;
            // add this to the processed references
            nextLayer.push(itemID);
          }
          break;
        }
        case "Trait": {
          // store a reference in the data structure
          actordata.traits[itemID] = item;
          actordata.dynamic[itemID] = item;
          switch (item.name) {
            case "Keen Senses":
            case "Night Sight":
            case "Dark Sight":
            case "Darkvision":
            case "Star Sight": {
              actordata.dynamic.visionType = item.name;
            }
          }
          break;
        }
        case "Melee-Attack":
        case "Ranged-Attack":
        case "Defence":
        case "Rollable": {
          // If the formula is numeric, store it in the array of valid references
          if (Number.isNumeric(itemdata.formula)) {
            nextLayer.push(itemID);
            itemdata.value = itemdata.moddedvalue = Number(itemdata.formula);
          } else {
            itemdata.moddedformula = "";
            itemdata.value = itemdata.moddedvalue = 0;
          }
          // store a reference in the data structure
          actordata.dynamic[itemID] = item;
          // store a reference in a temporary array
          if (itemdata.category == "value") values.push(item);
          else if (itemdata.category == "combatval") values.push(item);
          else if (itemdata.category == "defence") values.push(item);
          else rollables.push(item);
          break;
        }
        case "Pool": {
          itemID = actor.slugify(itemdata.abbr);
          if (itemID) {
            itemdata.moddedvalue = itemdata.value;
            nextLayer.push(itemID);
            // store a reference in the data structure for resources
            actordata.tracked[itemID] = item;
            nonmods.push(item);
          }
          break;
        }
        case "Variable": {
          // If the formula is numeric, store it in the array of valid references
          if (Number.isNumeric(itemdata.formula)) {
            itemdata.value = itemdata.moddedvalue = Number(itemdata.formula);
            nextLayer.push(itemID);
          }
          // store a reference in the data structure
          actordata.dynamic[itemID] = item;

          // add any items whose name starts with "Ranks:" to the list to be polled next
          if (item.name.startsWith("Ranks:")) rankitems.push(item);

          break;
        }
        case "Modifier": {
          itemdata.tempName = item.name;
          // store a reference in the data structure array of modifiers
          actordata.modifiers[itemID] = item;
          // if it modifies ability stats, add it to a temporary array
          if (itemdata.primary) primods.push(item);
          if (itemdata.attack) attackmods.push(item);
          if (itemdata.defence || itemdata.combatval || itemdata.value) valuemods.push(item);
          if (itemdata.reaction) reactionmods.push(item);
          actormods.push(item);
          break;
        }
        default: {
          // should put a note indicating an existing item was not handled
        }
      }
    }

    /**
     * Only count ranks for actors of the current ruleset and possessing a rankitem.
     * 
     * Calculate the ranks, then process number formulae
     */
    if (actordata.rankitem && (actordata.ruleset == actordata.mode)) {
      if (CONFIG.system.testMode) console.debug("Rankitems to be polled:\n", [this, rankitems, rankgcitems, rankcombitems, ranklimitems, rankspecitems]);

      // count the rank standard skill items
      for (const item of rankitems) {
        for (const entry of item.system.entries) {
          let target = actor.slugify(entry.label);
          // if the name is not blank
          if (target) {
            entry.value = Number(entry.formula);
            // the target may be one that does not exist on the actor so capture it anyway
            actorranks[target] = (actorranks[target]) ? actorranks[target] + entry.value : entry.value;
          }
        }
      }
      // count the rank skill group and category items
      for (const item of rankgcitems) {
        for (const entry of item.system.entries) {
          let target = actor.slugify(entry.label);
          // if the name is not blank
          if (target) {
            entry.value = Number(entry.formula);
            // the target may be one that does not exist on the actor so capture it anyway
            actorgcranks[target] = (actorgcranks[target]) ? actorgcranks[target] + entry.value : entry.value;
          }
        }
      }
      // count the rank combination skill items
      for (const item of rankcombitems) {
        for (const entry of item.system.entries) {
          let target = actor.slugify(entry.label);
          // if the name is not blank
          if (target) {
            entry.value = Number(entry.formula);
            // the target may be one that does not exist on the actor so capture it anyway
            actorcombranks[target] = (actorcombranks[target]) ? actorcombranks[target] + entry.value : entry.value;
          }
        }
      }
      // count the rank limited skill items
      for (const item of ranklimitems) {
        for (const entry of item.system.entries) {
          let target = actor.slugify(entry.label);
          // if the name is not blank
          if (target) {
            entry.value = Number(entry.formula);
            // the target may be one that does not exist on the actor so capture it anyway
            actorlimranks[target] = (actorlimranks[target]) ? actorlimranks[target] + entry.value : entry.value;
          }
        }
      }
      // count the rank limited skill items
      for (const item of rankspecitems) {
        for (const entry of item.system.entries) {
          let target = actor.slugify(entry.label);
          // if the name is not blank
          if (target) {
            entry.value = Number(entry.formula);
            // the target may be one that does not exist on the actor so capture it anyway
            actorspecranks[target] = (actorspecranks[target]) ? actorspecranks[target] + entry.value : entry.value;
          }
        }
      }

      if (CONFIG.system.testMode) console.debug("Rankdata after polling:\n", [this, actorranks, actorgcranks, actorcombranks, actorlimranks, actorspecranks]);

      let rankprog = actordata.rankitem.system.standard || "0:1:1:1:1";

      // assign the ranks to the targets in dynamic
      for (const [name, ranks] of Object.entries(actorranks)) {
        if (actordata.dynamic[name]) {
          const item = actordata.dynamic[name];
          const itemdata = item.system;
          // calculate rank bonus to assign to value and moddedvalue
          const value = actor.rankCalculator(ranks, rankprog);
          itemdata.formula = `#${ranks}`;
          itemdata.ranks = ranks;
          itemdata.value = value;
          itemdata.moddedvalue = value;
          nextLayer.push(name);
        } else {
          if (CONFIG.system.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
        }
      }
      rankprog = actordata.rankitem.system.skillcategory || "0:1:1:1:1";
      // assign the ranks to the targets in dynamic
      for (const [name, ranks] of Object.entries(actorgcranks)) {
        if (actordata.dynamic[name]) {
          const item = actordata.dynamic[name];
          const itemdata = item.system;
          // calculate category rank bonus to assign to value and moddedvalue
          const value = actor.rankCalculator(ranks, rankprog);
          itemdata.formula = `#${ranks}`;
          itemdata.ranks = ranks;
          itemdata.value = value;
          itemdata.moddedvalue = value;
          nextLayer.push(name);
        } else {
          if (CONFIG.system.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
        }
      }
      rankprog = actordata.rankitem.system.limited || "0:1:1:1:1";
      // assign the ranks to the targets in dynamic
      for (const [name, ranks] of Object.entries(actorlimranks)) {
        if (actordata.dynamic[name]) {
          const item = actordata.dynamic[name];
          const itemdata = item.system;
          // calculate category rank bonus to assign to value and moddedvalue
          const value = actor.rankCalculator(ranks, rankprog);
          itemdata.formula = `#${ranks}`;
          itemdata.ranks = ranks;
          itemdata.value = value;
          itemdata.moddedvalue = value;
          nextLayer.push(name);
        } else {
          if (CONFIG.system.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
        }
      }
      rankprog = actordata.rankitem.system.combined || "0:1:1:1:1";
      // assign the ranks to the targets in dynamic
      for (const [name, ranks] of Object.entries(actorcombranks)) {
        if (actordata.dynamic[name]) {
          const item = actordata.dynamic[name];
          const itemdata = item.system;
          // calculate category rank bonus to assign to value and moddedvalue
          const value = actor.rankCalculator(ranks, rankprog);
          itemdata.formula = `#${ranks}`;
          itemdata.ranks = ranks;
          itemdata.value = value;
          itemdata.moddedvalue = value;
          nextLayer.push(name);
        } else {
          if (CONFIG.system.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
        }
      }
      // assign the ranks to the targets in dynamic
      for (const [name, ranks] of Object.entries(actorspecranks)) {
        if (actordata.dynamic[name]) {
          const item = actordata.dynamic[name];
          if (item.name == "PPDev") {
            rankprog = actordata.rankitem.system.powerpointdev || "0:1:1:1:1";
          } else {
            rankprog = actordata.rankitem.system.bodydev || "0:1:1:1:1";
          }
          const itemdata = item.system;
          // calculate category rank bonus to assign to value and moddedvalue
          const value = actor.rankCalculator(ranks, rankprog);
          itemdata.formula = `#${ranks}`;
          itemdata.ranks = ranks;
          itemdata.value = value;
          itemdata.moddedvalue = value;
          nextLayer.push(name);
        } else {
          if (CONFIG.system.testMode) console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
        }
      }
    }

    // Modify Primary Attribute values first
    for (const item of primods) {
      if (item.system.inEffect) {
        for (const entry of item.system.entries) {
          // this entry is a primod
          if (entry.category == "primary") {
            // process primods formulae before the mods are applied
            actor.processModifierFormula(entry, item);
            const cases = entry.targets.split(",").map(word => word.trim().toLowerCase());
            for (const key of cases) {
              const splitkey = key.split(".");
              if (splitkey[1] == "value") {
                const target = actordata.dynamic[splitkey[0]];
                if (target?.type == "Primary-Attribute") {
                  // modify the value and moddedvalue together
                  target.system.value = target.system.moddedvalue += entry.value;
                }
              }
            }
          }
        }
      }
    }
    // Set the moddedvalues for proper Primary Attributes
    for (const item of primaries) {
      const itemdata = item.system;
      let itemID = actor.slugify(itemdata.abbr);
      // only process primaries with abbreviations
      if (itemID) {
        if (actordata.hasStatBonusTable && system[actordata.mode].primaryAttributes.includes(itemdata.abbr.toLowerCase())) {
          // this makes the stat modifiers available as a reference
          itemdata.moddedvalue = system.sbs(itemdata.value);
        }
      }
    }
    // Modify Primary Attribute moddedvalues second
    for (const item of primods) {
      if (item.system.inEffect) {
        for (const entry of item.system.entries) {
          // this entry is a primod
          if (entry.category == "primary") {
            const cases = entry.targets.split(",").map(word => word.trim().toLowerCase());
            for (const key of cases) {
              const target = actordata.dynamic[key];
              if (target?.type == "Primary-Attribute") {
                target.system.moddedvalue += entry.value;
                if (actordata.hasStatBonusTable && system[actordata.mode].primaryAttributes.includes(target.system.abbr.toLowerCase())) {
                  /**
                   * This is a primary attribute and 
                   * there is a stat bonus table in effect so 
                   * the value and moddedvalue must remain separate.
                   */
                } else {
                  // this makes the "stat" behave normally
                  target.system.value = target.system.moddedvalue;
                }
              }
            }
          }
        }
      }
    }

    // this will store all valid modifier entries for later processing efficiency
    const activeModEntries = [];

    // calculate the remaining formulae one layer at a time
    while (nextLayer.length !== 0) {
      previousLayer = nextLayer;
      nextLayer = [];

      for (const item of nonmods) {
        const itemdata = item.system;

        if (item.type == "Pool") { // new pool formula functionality
          const dynID = actor.slugify(itemdata.abbr);
          //ignore numeric formulae
          if (itemdata.hasmax && itemdata.hasmin) continue;
          if (!itemdata.hasmax) { // formula has not yet been calculated
            for (const target of previousLayer) {
              // only need to process the terms once
              if (itemdata.hasmax) continue;
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.maxForm.includes(target)) {
                const formData = actor._replaceData(itemdata.maxForm);
                try {
                  itemdata.max = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, '')) * 100) / 100;
                } catch (err) {
                  itemdata.max = 0; // eliminate any old values
                  console.warn("Pool-max formula evaluation error:\n", [actor.name, item.name, itemdata.maxForm]);
                }
                itemdata.hasmax = true;
              }
            }
          }
          if (!itemdata.hasmin) { // formula has not yet been calculated
            for (const target of previousLayer) {
              // only need to process the terms once
              if (itemdata.hasmin) continue;
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.minForm.includes(target)) {
                const formData = actor._replaceData(itemdata.minForm);
                try {
                  itemdata.min = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, '')) * 100) / 100;
                } catch (err) {
                  itemdata.min = 0; // eliminate any old values
                  console.warn("Pool-min formula evaluation error:\n", [actor.name, item.name, itemdata.minForm]);
                }
                itemdata.hasmin = true;
              }
            }
          }
          if (itemdata.hasmax && itemdata.value > itemdata.max) itemdata.value = itemdata.max;
          if (itemdata.hasmin && itemdata.value < itemdata.min) itemdata.value = itemdata.min;
          itemdata.ratio = Math.trunc(itemdata.value / itemdata.max * 100);
          if (itemdata.hasmax && itemdata.hasmin)
            nextLayer.push(dynID);
        } else {
          //ignore items with no formula or whose formula has already been processed
          if (!itemdata.formula || Number.isNumeric(itemdata.formula) || itemdata.formula.startsWith("#") || (Number(itemdata.moddedformula) == itemdata.value && itemdata.moddedformula != "")) continue;
          const dynID = actor.slugify(item.name);
          if (previousLayer.includes(dynID)) continue;

          // the formula is a reference
          if (itemdata.formula.includes("@")) {
            for (const target of previousLayer) {
              // only need to process the terms once
              if (Number(itemdata.moddedformula) == itemdata.value && itemdata.moddedformula != "") continue;
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.formula.includes(target)) {
                const formData = actor._replaceData(itemdata.formula);
                try {
                  itemdata.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, '')) * 100) / 100;
                  itemdata.moddedvalue = itemdata.value; // default reference is now moddedvalue so set it now
                  itemdata.moddedformula = "" + itemdata.value; // store the result as a String
                } catch (err) {
                  // store the formula ready to be rolled
                  itemdata.moddedformula = formData.value;
                  itemdata.value = itemdata.moddedvalue = 0; // eliminate any old values
                  console.warn("Non-Modifier formula evaluation incomplete:\n", [actor.name, item.name, itemdata.moddedformula]);
                }
                // put this item into the next layer
                nextLayer.push(dynID);
              }
            }
          } else { // the formula might be a dice expression
            if (itemdata.formula.toLowerCase().includes("d")) {
              itemdata.moddedformula = itemdata.formula;
              itemdata.value = itemdata.moddedvalue = 0; // eliminate any old values
              console.debug("Encountered a Dice Expression:\n", [actor.name, item.name, itemdata.moddedformula]);
            }
          }
        }
      }
    }

    // process all modifiers again after the non-modifiers have been processed
    for (const item of actormods) {
      if (!item.system.inEffect) continue;
      for (const entry of item.system.entries) {
        actor.processModifierFormula(entry, item);
        if (activeModEntries.includes(entry) || entry.value == 0) continue;
        activeModEntries.push(entry);
      }
    }

    // calculate the modified value of the item
    for (const item of rollables) {
      const itemdata = item.system;
      // TODO: confirm that the following is now redundant
      // itemdata.moddedvalue = itemdata.value;
      // if there is no modified formula, make it the value
      if (itemdata.moddedformula == undefined) itemdata.moddedformula = "" + itemdata.value;
      switch (item.type) {
        case "Melee-Attack":
        case "Ranged-Attack": {
          itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "attack", activeModEntries);
          break;
        }
        case "Defence": {
          itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "defence", activeModEntries);
          break;
        }
        case "Rollable": {
          switch (itemdata.category) {
            case "check": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "reaction", activeModEntries);
              break;
            }
            case "technique":
            case "skill": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "skill", activeModEntries);
              break;
            }
            case "rms":
            case "spell": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "spell", activeModEntries);
              break;
            }
          }
          break;
        }
      }
    }
    // this is the end of prepareDerivedData
  }

  /**
   * rolldata consists of:
   * - name: the name of the roll
   * - type: the type of roll (used for filtering modifiers)
   * - roll: the value being processed by this function
   * - modtype: if the type is "modlist" then this is the type of the roll
   * - id: the _id of the item being rolled, so we can access all it's raw data
   */
  async roll(rolldata) {

    let flavour = rolldata.flavour || "";
    const actor = rolldata.actor;
    const actordata = actor.system;
    const item = await actor.items.get(rolldata.id);
    const damageType = item?.system.damageType || "";

    let canCrit = false;
    let hideTotal = false;

    switch (rolldata.type) {
      case "tochat":
      case "technique": { // send the notes to the chat and return
        const isplaintext = rolldata.roll.replace(/<[^>]*>?/gm, '') == rolldata.roll;
        ChatMessage.create({
          speaker: ChatMessage.getSpeaker({ actor: actor }),
          flavor: `<b>${rolldata.name}</b><hr>`,
          content: isplaintext ? rolldata.roll.replace(/(?:\r\n|\r|\n)/g, '<br>') : rolldata.roll,
        });
        return;
      }
      case "rms": { // no roll, just storing a value
        return;
      }
      case "attack": {
        const attack = item;
        const attackdata = attack.system;
        const ammopool = actordata.tracked[attackdata.minST.toLowerCase()];
        if (ammopool) {
          // has ammunition
          const ammoused = Math.min(attackdata.accuracy, ammopool.system.value);
          // Decrement Pool by the Rate of Fire value
          await actor.updateEmbeddedDocuments("Item", [{
            _id: ammopool._id,
            system: { value: Math.max(0, ammopool.system.value - ammoused) }
          }]);
          // Get remaining ammo count post-attack to display in output
          const ammo_left = ammopool.system.value;
          flavour += `<hr><b>Range:</b> ${attackdata.range}<br>`;
          if (ammoused == 0) {
            flavour += `<b>Ammo used:</b> ${ammoused} <span class="critfail">Click! Attack failed! Reload!</span><br>`;
          } else if (ammo_left == 0) {
            flavour += `<b>Ammo used:</b> ${ammoused} (Remaining: ${ammo_left}) <span class="critfail">Reload!</span><br>`;
          } else {
            flavour += `<b>Ammo used:</b> ${ammoused} (Remaining: ${ammo_left})<br>`;
          }
          flavour += `<b>Damage:</b><span class="rollable" data-item-id="{{item._id}}" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage"> ${attackdata.damage} - ${attackdata.damageType}</span><hr><b>Strike Modifiers:</b><br>Base:  `;
        } else {
          if (attack.type == "Ranged-Attack") {
            flavour += `<hr><b>Range:</b> ${attackdata.range}<br><b>Damage:</b><span class="rollable" data-item-id="{{item._id}}" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage"> ${attackdata.damage} - ${attackdata.damageType}</span><hr><b>Strike Modifiers:</b><br>Base:  `;
          } else {
            // a melee attack
            flavour += `<hr><b>Damage:</b><span class="rollable" data-item-id="${item._id}" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage"> ${attackdata.damage} - ${attackdata.damageType}</span><hr><b>Strike Modifiers:</b><br>Base:  `;
          }
        }
        break;
      }
    }
    // add the item-id to the flavour
    flavour += `<span data-item-id="${item?.id}"></span>`;

    // get the modifiers
    const modList = actor.sheet.fetchRollModifiers(actor, { type: rolldata.type, modtype: rolldata.modtype, name: rolldata.name });
    // process the modifiers
    let modformula = "";
    let rolldice = actor.toDiceAndPips(rolldata.roll, false);
    flavour += (rolldata.type != "modlist") ? ((rolldata.type == "damage") ? ` [<b>${rolldice} ${damageType}</b>]` : (rolldata.type == "block") ? ` [<b>${rolldata.roll}</b>]` : ` [<b>${rolldice}</b>]`) : `<p class="chatmod">[<b>${rolldice}</b>]: ${rolldata.name}<br>`;
    const hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      flavour += (rolldata.type != "modlist") ? `<p class="chatmod">` : "";
      for (const mod of modList) {
        sign = typeof mod.modifier === 'string' ? "" : mod.modifier > -1 ? "+" : "";
        modformula += `${sign}${mod.modifier}`;
        flavour += ` ${sign}${actor.toDiceAndPips(mod.modifier, false)} : ${mod.description} <br>`;
      }
      flavour += `</p>`;
    }

    var formula = "";
    switch (rolldata.type) {
      case "modlist": // this will render a dialog with the modifiers, not a chat message
        let roll = await new Roll(rolldata.roll).evaluate({ async: true });

        flavour += `<hr><p>${roll.result} = <b>${roll.total}</b></p>`;
        new Dialog({
          title: actor.name,
          content: flavour,
          buttons: {
            close: {
              label: "Close"
            },
          },
          default: "close"
        }).render(true)
        return;
      case "block": // send the value to the chat as a modifiable roll
        let totalpips = eval((rolldata.roll + modformula).replace(CONFIG.system.dataRgx, ''));
        formula = `${totalpips}`;
        break;
      case "damage":
      case "dodge":
      case "attack":
      case "skill":
      case "spell": {
        canCrit = true;
        let totalpips = eval((rolldata.roll + modformula).replace(CONFIG.system.dataRgx, ''));
        formula = `${actor.toDiceAndPips(totalpips - 3)}`;
        flavour += ` <p>Roll: [<b>${actor.toDiceAndPips(totalpips, false)}</b>]`;
        break;
      }
      default: { // there should not be any of these
        console.error("Failed to process rolldata roll type: " + rolldata.type);
      }
    }

    // process the modified roll
    let roll = await new Roll(formula).evaluate({ async: true });

    if (canCrit) { // an attack or defence roll in D6
      // store the structure of the main roll and set it up to be re-evaluated
      let mainrollobject = roll.toJSON();
      mainrollobject.evaluated = false;
      // roll the wild die options and store their structure
      roll = await new Roll("6+1d6x6-2d6").evaluate({ async: true });
      let rollobject = roll.toJSON();

      const wild = roll.dice[0].values[0];
      let sflavour = "";
      if (wild == 1) {
        if (roll.terms[4].values[0] == 1) {
          sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critfail">Botched</span>`;
        } else {
          sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critfail">Wild Luck</span>`;
        }
        // append " - 2d6" and the results to the formula
        mainrollobject.formula += " - 2d6";
        mainrollobject.terms.push(rollobject.terms[3]); // -
        //rollobject.terms[4].evaluated = false;
        let botchresults = [rollobject.terms[2].results[0]];
        botchresults.push(rollobject.terms[4].results[0]);
        rollobject.terms[4].results = botchresults;
        mainrollobject.terms.push(rollobject.terms[4]); // 2d6 with 1 as the first result
      } else {
        if (wild == 6) {
          sflavour = ` <i class="fas fa-arrow-right"></i> <span class="critsuccess">Wild Luck</span>`;
        }
        // append " + 1d6x6" and the results to the formula
        mainrollobject.formula += " + 1d6x6";
        mainrollobject.terms.push(rollobject.terms[1]); // +
        mainrollobject.terms.push(rollobject.terms[2]); // 1d6x6
      }
      flavour += sflavour + `</p>`;
      // re-evaluate the expression with the selected options
      roll = await Roll.fromData(mainrollobject).evaluate({ async: true });
      console.log([mainrollobject, rollobject, roll]);
    }
    // prepare the flavor in advance based on which type of die is in use.
    roll.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: actor }),
        flavor: flavour,
        flags: { hideMessageContent: hideTotal }
      }
    );
    let updates = { ["system.gmod.value"]: 0 };
    actor.resetModVars(false);
    actor.update(updates);
  }
}
