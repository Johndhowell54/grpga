
/**
* add listeners to dialog boxes
*/
Hooks.on("renderDialog", (dialog, html, data) => {

  // isolate these listeners to mydialog windows
  if (data.content?.includes("mydialog")) {
    const actor = dialog.data.options.actor;
    const macroData = dialog.data.options.macroData;
    const delay = game.settings.get("grpga", "macroDialogDelay");

    // Open Journal Entries from a dialog window
    html.on('click', '.open-journal', ev => {
      const journal = ev.currentTarget.dataset.table;
      try {
        Journal._showEntry(game.journal.getName(journal).uuid, "text", true);
      } catch (err) {
        if (journal) ui.notifications.error(`There is no matching Journal Entry for ${journal}. Perhaps you have yet to import it from the compendium?`);
      }
    });

    html.on("change", "select", async ev => {
      if (macroData || ev.currentTarget.name == "") {
        // do nothing. It will be handled by iteminput below
      } else {
        await actor.update({ [ev.currentTarget.name]: ev.currentTarget.value });
        const type = ev.currentTarget.closest(".mydialog").dataset.type;
        actor.actiondialog(type);
        dialog.close();
      }
    });

    // Update the GMod
    html.on('change', '.iteminput', async ev => {
      await actor.sheet._onInputChange(ev);
      await new Promise(r => setTimeout(r, delay));
      if (macroData) {
        macroData.position.left = ev.delegateTarget.offsetLeft;
        macroData.position.top = ev.delegateTarget.offsetTop - 3;
        actor.modifierdialog(ev.currentTarget.dataset.attackId, macroData);
      } else {
        const type = ev.currentTarget.closest(".mydialog").dataset.type;
        actor.actiondialog(type);
      }
      dialog.close();
    });

    // Select the attack to process
    html.on('click', '.selectable', async ev => {
      if (macroData) {
        macroData.position.left = ev.delegateTarget.offsetLeft;
        macroData.position.top = ev.delegateTarget.offsetTop - 3;
        actor.modifierdialog(ev.currentTarget.dataset.itemId, macroData);
      } else {
        if (ev.currentTarget.dataset.type == "refresh" && game.canvas.tokens.controlled?.length == 1) {
          // if the user is the owner of the selected token, open that sheet
          const tokenactor = game.canvas.tokens.controlled[0].actor;
          if (tokenactor?.isOwner) {
            tokenactor.actiondialog();
          }
        } else {
          await actor.update({ "data.bm.step": ev.currentTarget.dataset.direction });
          ev.currentTarget = ev.currentTarget.previousElementSibling;
          await actor.sheet._onInputChange(ev);
          await new Promise(r => setTimeout(r, delay));
          actor.actiondialog();
        }
      }
      dialog.close();
    });

    // process the rollable item
    html.on('click', '.rollable', ev => {
      actor.sheet._onRoll(ev);
      if (macroData && ev.currentTarget.dataset.type != "modlist") dialog.close();
    });

    // process the rollable item
    html.on('click', '.rollmacro', ev => {
      actor.sheet._rollmacro(ev);
    });

    // allow modifiers to be toggled
    html.on('click', '.item-toggle', async ev => {
      await actor.sheet._onToggleItem(ev);
      await new Promise(r => setTimeout(r, delay));
      if (macroData) {
        macroData.position.left = ev.delegateTarget.offsetLeft;
        macroData.position.top = ev.delegateTarget.offsetTop - 3;
        actor.modifierdialog(ev.currentTarget.dataset.attackId, macroData);
      } else {
        const type = ev.currentTarget.closest(".mydialog").dataset.type;
        actor.actiondialog(type);
      }
      dialog.close();
    });
  }
});