/**
 * add listeners to chat messages in the log
 */
Hooks.on("getJournalSheetHeaderButtons", (log, headerbuttons) => {

  if (log.document.pages.entries().next().value[1].text.content.includes("spell-choice")) { // this is a spell description journal
    log.position.height = 500;
    log.position.width = 435;
  }
});

/**
* add listeners to rendered journal text pages
*/
Hooks.on("renderJournalTextPageSheet", (log, html, data) => {

  // open a Journal Entry
  html.on('click', '.open-journal', ev => {
    ev.preventDefault();

    const journal = ev.currentTarget.dataset.table;
    Journal._showEntry(game.journal.getName(journal).uuid, "text", true);
  });

  // send a message to the chat log
  html.on('click', '.rollable', ev => {
    ev.preventDefault();
    // data stored in the enclosing element
    let parentset = "";
    // data stored in this element
    let dataset = ev.currentTarget.dataset;
    let message = "";
    let armour = "";
    let crit = "";

    switch (dataset.type) {
      case "attack": {
        parentset = ev.currentTarget.parentElement.dataset;
        switch (dataset.armour) {
          case "na": armour = "No Armor"; break;
          case "la": armour = "Light Armor"; break;
          case "ma": armour = "Medium Armor"; break;
          case "ha": armour = "Heavy Armor"; break;
        }
        switch (dataset.crit) {
          case "Sup": crit = "Superficial"; break;
          case "Lig": crit = "Light +10"; break;
          case "Mod": crit = "Moderate +20"; break;
          case "Gri": crit = "Grievous +30"; break;
          case "Let": crit = "Lethal +50"; break;
        }
        message += `<table class="attack-result"><tr><td>${game.i18n.localize(`local.rolls.table`)}:</td><td>${parentset.table}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.roll`)}:</td><td>${parentset.roll}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.against`)}:</td><td>${armour}</td></tr></table>`;
        message += `<p class="damageresult" data-type="damage" data-hits="${dataset.hits}"><b>${dataset.hits}</b> hits</p>`;
        if (crit) {
          message += `<p class="critseverity" data-type="severity" data-crit="${crit}"> and a <b>${crit}</b> critical strike</p>`;
        }
        game.journal.getName(parentset.table).sheet.close();
        break;
      }
      case "fearsaveroll":
      case "reactionrolls":
      case "spellfailure":
      case "spellcasting":
      case "missilefumble":
      case "meleethrownfumble":
      case "magicalresonance": {
        message += `<table class="critical-result"><tr><td>${game.i18n.localize(`local.rolls.table`)}:</td><td>${dataset.table}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.roll`)}:</td><td>${dataset.roll}</td></tr></table>`;
        message += `<p><b>${dataset.result}</b></p>`;
        break;
      }
      case "critical": {
        // todo: do we have access to the actor name for the speaker?
        // todo: include a link to the crit table we just rolled so it can be re-opened quickly
        message += `<table class="critical-result"><tr><td>${game.i18n.localize(`local.rolls.table`)}:</td><td class="open-journal" data-table="${dataset.table}">${dataset.table}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.roll`)}:</td><td>${dataset.roll}</td></tr></table>`;
        if (dataset.condition != "null") {
          message += `<p><b>${dataset.result}</b></p>`;
          message += `<p class="critresult" data-type="critical" data-message="${dataset.result}" data-woundtitle="${dataset.woundtitle}" data-hits="${dataset.hits1}" data-bleed="${dataset.bleed1}" data-action="${dataset.action1}" data-effect="${dataset.effect1}">Has ${dataset.condition}</p>`;

          message += `<p class="critresult" data-type="critical" data-message="${dataset.result}" data-woundtitle="${dataset.woundtitle}" data-hits="${dataset.hits2}" data-bleed="${dataset.bleed2}" data-action="${dataset.action2}" data-effect="${dataset.effect2}">Does not</p>`;
        } else { // there is no condition, only a single outcome
          message += `<p class="critresult" data-type="critical" data-message="${dataset.result}" data-woundtitle="${dataset.woundtitle}" data-hits="${dataset.hits1}" data-bleed="${dataset.bleed1}" data-action="${dataset.action1}" data-effect="${dataset.effect1}">${dataset.result}</p>`;
        }
        game.journal.getName(dataset.table).sheet.close();
        break;
      }
      case "spell": {
        message += `<table class="spell-choice"><tr class="open-journal" data-table="${dataset.table}"><td>${game.i18n.localize(`local.rolls.spell.lore`)}:</td><td>${dataset.table}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.spell.weave`)}:</td><td>${dataset.weave}</td></tr>`;
        message += `<tr class="open-journal" data-table="${dataset.spell}"><td>${game.i18n.localize(`local.rolls.spell.spell`)}:</td><td class="spellname">${dataset.spell}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.spell.range`)}:</td><td>${dataset.range}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.spell.aoe`)}:</td><td>${dataset.aoe}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.spell.duration`)}:</td><td>${dataset.duration}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.spell.save`)}:</td><td>${dataset.save}</td></tr></table>`;
        // todo: add description and warps for premium content
        game.journal.getName(dataset.spell).sheet.close();
        break;
      }
      default: {
        ui.notifications.info(`Journal rolling for Type:${dataset.type} is not supported yet.`);
        return;
      }
    }
    ChatMessage.create({
      speaker: {actor: game.settings.get("grpga", "currentActor")},
      content: message,
    });
  });
  /*
  let target = token;
  if (!target) return ui.notifications.error(`Please select a single target.`);
  let curtHP = target.actor.getRollData().attributes.hp.value;
  let maxHP = target.actor.getRollData().attributes.hp.max;
  let the_message = `Current Life Total: ${curtHP}/${maxHP}`;
  ChatMessage.create({
    content: the_message,
    whisper: ChatMessage.getWhisperRecipients("GM"),
    speaker: ChatMessage.getSpeaker({ alias: `${token.name} Stats` })
  });
  */
});