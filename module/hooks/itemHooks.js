Hooks.on('createItem', (document, options, userId) => {
  if (CONFIG.system.testMode) console.debug("createItem:\n", [document, options, userId]);
  const docdata = document.system;

  // items dragged from the sidebar will lose their moddedformula if they have one so calculate it again
  switch (document.type) {
    case "Melee-Attack":
    case "Ranged-Attack":
    case "Defence":
    case "Rollable": {
      if (CONFIG.system.testMode) console.debug("Item Data:\n", docdata);
      if (docdata.formula) { // we will recalculate the value if the formula does not contain an @dependency
        let formula = docdata.formula;
        if (Number.isNumeric(formula)) {
          // the formula is a number
          docdata.value = Number(formula);
        } else if (formula.includes("#") || formula.includes("@")) {
          // the formula has ranks or a reference and will be processed in prepareDerivedData
        } else {
          // the formula should be a valid dice expression so has no value
          docdata.value = 0;
          docdata.moddedvalue = 0;
          docdata.moddedformula = formula;
        }
      }
      break;
    }
    case "Modifier": {
      if (docdata.alwaysOn) docdata.inEffect = true;
      break;
    }
  }
});

Hooks.on('preCreateItem', (document, data, options, userId) => {
  if (CONFIG.system.testMode) console.debug("preCreateItem:\n", [document, data, options, userId]);

  let itemdata = data.system;

  if (document.id) {// This item exists already but may have the wrong chartype
    if (document.actor) {
      // if there is an actor and the chartype matches then exit
      if (document.actor.type == itemdata.chartype) return;
      itemdata.chartype = document.actor.type;
    } else {
      // if the chartype matches the ruleset then exit
      if (CONFIG.system.chartype == itemdata.chartype) return;
      itemdata.chartype = CONFIG.system.chartype;
    }
    // correct the chartype then exit
    document.updateSource(data);
    return;
  } else if (itemdata == undefined) {
    // the item is being created from the item sidebar so give it a chartype
    itemdata = document.system;
    itemdata.chartype = CONFIG.system.chartype;
  }

  if (!itemdata.group) { // initialise group field to match item category or type
    itemdata.group = itemdata.category || data.type;
  }

  if (data.name != data.type) {
    // this is an existing item being dropped in a container
    document.updateSource(data);
    return;
  }
  switch (itemdata.chartype) {
    case "CharacterD100": {
      switch (data.type) {
        case "Melee-Attack": {
          data.img = "icons/svg/sword.svg";
          itemdata.armourDiv = 5;
          itemdata.damage = "";
          itemdata.damageType = "";
          itemdata.minST = "oeh";
          itemdata.notes = "Enter the attack and critical table names from the journal entries you have made for them, or leave them blank."
          itemdata.reach = 5;
          itemdata.weight = 100;
          break;
        }
        case "Ranged-Attack": {
          data.img = "icons/svg/target.svg";
          itemdata.armourDiv = 5;
          itemdata.damage = "";
          itemdata.damageType = "";
          itemdata.minST = "oeh";
          itemdata.notes = "Enter the attack and critical table names from the journal entries you have made for them, or leave them blank."
          itemdata.rof = 5;
          itemdata.accuracy = 100;
          itemdata.range = "10";
          break;
        }
        case "Wound": { // Wound
          data.img = "icons/svg/blood.svg";
          data.name = "Wound";
          break;
        }
        case "Defence": { // dodge(std), parry(oeh), block(oe)
          data.img = "icons/svg/mage-shield.svg";
          data.name = "Various";
          break;
        }
        default: {
          data.img = "icons/svg/mystery-man-black.svg";
          break;
        }
      }
      break;
    }
    case "Character3D6": {
      switch (data.type) {
        case "Melee-Attack": {
          data.img = "icons/svg/sword.svg";
          itemdata.formula = "@dx -2";
          itemdata.armourDiv = 1;
          itemdata.damage = "1d6";
          itemdata.damageType = "cut";
          itemdata.minST = "8";
          itemdata.notes = ""
          itemdata.reach = 1;
          itemdata.weight = 0;
          break;
        }
        case "Ranged-Attack": {
          data.img = "icons/svg/target.svg";
          itemdata.formula = "@dx -2";
          itemdata.armourDiv = 1;
          itemdata.damage = "1d6";
          itemdata.damageType = "imp";
          itemdata.minST = "8";
          itemdata.notes = ""
          itemdata.range = "10/15";
          itemdata.rof = 1;
          itemdata.accuracy = 0;
          itemdata.shots = "0";
          itemdata.bulk = 0;
          itemdata.recoil = "0";
          break;
        }
        default: {
          data.img = "icons/svg/mystery-man-black.svg";
          break;
        }
      }
      break;
    }
    default: {
      if (!data.img) {
        data.img = "icons/svg/mystery-man-black.svg";
      }
      break;
    }
  }
  document.updateSource(data);
});
