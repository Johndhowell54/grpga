// Import Modules
import { BaseActor } from "./module/actor/baseActor.js";
import { Actor3D6Sheet } from "./module/actor/actor3D6-sheet.js";
import { Actor3D6FAHRSheet } from "./module/actor/actor3D6FAHR-sheet.js";
import { ActorMnMSheet } from "./module/actor/actorMnM-sheet.js";
import { ActorD100Sheet } from "./module/actor/actorD100-sheet.js";
import { ActorD120Sheet } from "./module/actor/actorD120-sheet.js";
import { ActorBBFSheet } from "./module/actor/actorBBF-sheet.js";
import { ActorD6Sheet } from "./module/actor/actorD6-sheet.js";
import { BaseItem } from "./module/item/item.js";
import { BaseItemSheet, PoolItemSheet, ModifierItemSheet, VariableItemSheet, ContainerItemSheet } from "./module/item/item-sheet.js";
import { system } from "./module/config.js";
import * as FANSpeedProvider from './module/speed-provider.js'
import { GRPGATokenDocument, GRPGAToken, TokenEffects } from "./module/token.js";
import { GRPGACombat, BBFCombat } from "./module/combat/GRPGACombat.js";
import { ActionPointCombat, ActionPointCombatTracker, ActionPointCombatant, ActionPointCombatantConfig } from "./module/combat/actionPointCombat.js";

// Pre-load templates
async function preloadHandlebarsTemplates() {
  const templatePaths = [
    "systems/grpga/templates/partials/gmod.hbs",
    "systems/grpga/templates/partials/modifiers.hbs",
    "systems/grpga/templates/partials/notes.hbs",
    "systems/grpga/templates/partials/sectiontitles.hbs"
  ];
  return loadTemplates(templatePaths);
};

Roll.CHAT_TEMPLATE = "systems/grpga/templates/dice/roll.hbs";
Roll.TOOLTIP_TEMPLATE = "systems/grpga/templates/dice/tooltip.hbs";

Hooks.once('init', () => {

  CONFIG.system = system;
  FANSpeedProvider.init()
  // Define custom Entity classes
  CONFIG.Actor.documentClass = BaseActor;
  CONFIG.Item.documentClass = BaseItem;
  CONFIG.Token.documentClass = GRPGATokenDocument;
  CONFIG.Token.objectClass = GRPGAToken;

  // Enable the modulus modifier for dice terms
  CONFIG.Dice.terms.d.MODIFIERS.mod = function () {
    for (let r of this.results) {
      r.result = r.result % this.faces;
    }
  };

  // Roll a merged exploding die (all explosions merged into a single result)
  CONFIG.Dice.terms.d.MODIFIERS.xx = function (modifier) {
    // Match xx, xx9, xx>9, xx2>9
    const rgx = /xx([0-9]+)?([<>=]+)?([0-9]+)?/i;
    const match = modifier.match(rgx);
    if (!match) return false;
    let [depth, comparison, target] = match.slice(1);

    // If no comparison or target are provided, treat the depth as the target value
    if (depth && !(target || comparison)) {
      target = depth;
      depth = null;
    }

    // Determine target values
    target = Number.isNumeric(target) ? parseInt(target) : this.faces;
    comparison = comparison || ">=";

    // Determine the number of allowed explosions
    depth = Number.isNumeric(depth) ? parseInt(depth) : 100;

    // Recursively explode until there are no remaining results to explode
    let current = 0;
    let newresults = [];

    // only cycle through the original results
    const initial = this.results.length;
    while (current < initial) {
      let thisdepth = depth; // allow current result to explode depth times
      let r = this.results[current];
      newresults.push(r);
      current++;
      if (!r.active) continue;

      // Determine whether to explode this result
      if (DiceTerm.compareResult(r.result, comparison, target)) {
        r.exploded = true;
        let discard = {...r};
        discard.discarded = true;
        discard.active = false;
        newresults.push(discard);
        let newrollindex = this.results.length;
        this.roll();
        thisdepth--;
        let newresult = this.results[newrollindex];
        newresult.discarded = true;
        newresult.active = false;
        newresults.push(newresult);
        r.result += newresult.result;
        while (DiceTerm.compareResult(newresult.result, comparison, target) && thisdepth > 0) {
          newresult.exploded = true;
          newrollindex++;
          this.roll();
          thisdepth--;
          newresult = this.results[newrollindex];
          newresult.discarded = true;
          newresult.active = false;
          newresults.push(newresult);
          r.result += newresult.result;
        }
      }
    }
    this.results = newresults;
  };

  // Roll an Open-Ended die
  CONFIG.Dice.terms.d.MODIFIERS.oe = function (modifier) {
    // Match oe2<5>96, oe2=5, oe5, oe
    const rgx = /oe([0-9]+)?([<>=]+)?([0-9]+)?([<>]+)?([0-9]+)?/i;
    const match = modifier.match(rgx);
    if (!match) return false;
    let [depth, lowcomp, lowtarget, highcomp, hightarget] = match.slice(1);

    // set the default range
    let range = Math.ceil(this.faces / 20);

    // If there is no depth, set it to the default range
    depth = Number.isNumeric(depth) ? parseInt(depth) : range;

    // If no comparison or target are provided, use the default range
    if (depth && !(lowtarget || lowcomp)) {
      lowtarget = depth + 1;
      hightarget = this.faces - depth;
      depth = null;
    }
    // Determine lowtarget values
    lowtarget = Number.isNumeric(lowtarget) ? parseInt(lowtarget) : range + 1;

    if (depth && !(hightarget || highcomp)) {
      if (lowcomp == "=") {
        lowtarget += 1;
      }
      hightarget = this.faces - lowtarget + 1;
    }
    // Determine hightarget values
    hightarget = Number.isNumeric(hightarget) ? parseInt(hightarget) : this.faces - range;

    // Recursively explode until there are no remaining results to explode
    let current = 0;
    let newresults = [];
    if (depth == null) depth = 100;

    // only cycle through the original results
    const initial = this.results.length;
    while (current < initial) {
      let thisdepth = depth; // allow current result to explode depth times
      let r = this.results[current];
      newresults.push(r);
      current++;
      if (!r.active) continue;

      // Determine whether to explode this result
      if (DiceTerm.compareResult(r.result, ">", hightarget)) {
        r.exploded = true;
        let newrollindex = this.results.length;
        this.roll();
        thisdepth--;
        let newresult = this.results[newrollindex];
        newresults.push(newresult);
        while (DiceTerm.compareResult(newresult.result, ">", hightarget) && thisdepth > 0) {
          newresult.exploded = true;
          newrollindex++;
          this.roll();
          thisdepth--;
          newresult = this.results[newrollindex];
          newresults.push(newresult);
        }
      } else if (DiceTerm.compareResult(r.result, "<", lowtarget)) {
        r.exploded = true;
        let newrollindex = this.results.length;
        this.roll();
        thisdepth--;
        let newresult = this.results[newrollindex];
        newresult.result = -newresult.result;
        newresults.push(newresult);
        while (DiceTerm.compareResult(-newresult.result, ">", hightarget) && thisdepth > 0) {
          newresult.exploded = true;
          newrollindex++;
          this.roll();
          thisdepth--;
          newresult = this.results[newrollindex];
          newresult.result = -newresult.result;
          newresults.push(newresult);
        }
      }
    }
    this.results = newresults;
  };

  // Which actor in this browser most recently made a roll
  game.settings.register("grpga", "currentActor", {
    name: "SETTINGS.currentActor.name",
    hint: "SETTINGS.currentActor.hint",
    scope: "client",
    config: false,
    default: "null",
    type: String
  });

  // Which Ruleset are we using
  game.settings.register("grpga", "rulesetChoice", {
    name: "SETTINGS.rulesetChoice.name",
    hint: "SETTINGS.rulesetChoice.hint",
    scope: "world",
    config: true,
    default: "d120",
    type: String,
    requiresReload: true,
    choices: {
      "3d6": "A 3D6 Ruleset like GURPS",
      "d120": "A Palladium Games Ruleset (or hijack)",
      "bbf": "BareBones Fantasy Ruleset",
      "d6": "A Flexible D6 Ruleset",
      "d100": "An Open-Ended D100 Ruleset",
      "mnm": "Mutants & Masterminds Ruleset"
    }
  });
  const rschoice = game.settings.get("grpga", "rulesetChoice");

  // Are we hijacking Palladium for another game system
  game.settings.register("grpga", "palladiumHijack", {
    name: "SETTINGS.palladiumHijack.name",
    hint: "SETTINGS.palladiumHijack.hint",
    scope: "world",
    config: true,
    default: "UsePal",
    type: String,
    requiresReload: true,
    choices: {
      "UsePal": "SETTINGS.UsePal",
      "UseD6Pool": "SETTINGS.UseD6Pool",
      "UsePnP": "SETTINGS.UsePnP",
      "Use2D6": "SETTINGS.Use2D6",
      "Use3D6": "SETTINGS.Use3D6",
      "UseD10": "SETTINGS.UseD10",
      "Use2D10": "SETTINGS.Use2D10",
      "UseNovus": "SETTINGS.UseNovus",
      "UseBlackNight": "SETTINGS.UseBlackNight",
      "UseD20": "SETTINGS.UseD20",
      "UseAlternity": "SETTINGS.UseAlternity",
      "UsePercent": "SETTINGS.UsePercent"
    }
  });
  CONFIG.system.hijack = game.settings.get("grpga", "palladiumHijack");

  // Register whether modifying a hijack with Sine Nomine conventions
  game.settings.register("grpga", "useSineNomine", {
    name: "SETTINGS.UseSineNomine",
    hint: "SETTINGS.UseSineNomineHint",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });
  CONFIG.system.useSineNomine = game.settings.get("grpga", "useSineNomine");

  // Register whether modifying a hijack with Marvel 616 dice conventions
  game.settings.register("grpga", "use616dice", {
    name: "SETTINGS.Use616dice",
    hint: "SETTINGS.Use616diceHint",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });
  CONFIG.system.use616dice = game.settings.get("grpga", "use616dice");

  if (rschoice != "d120") {
    CONFIG.system.hijack = "UsePal";
    CONFIG.system.useSineNomine = false;
    CONFIG.system.use616dice = false;
  }

  game.settings.register("grpga", "narrateJustifyLeft", {
    name: "Narrative Tools Left-Justify",
    hint: "Narrative Tools chat messages will be left-justified instead of centred.",
    scope: "client",
    config: true,
    default: false,
    type: Boolean
  });
  CONFIG.system.narrateJustifyLeft = game.settings.get("grpga", "narrateJustifyLeft");

  // Do you want to use a RollTable to define the stat bonuses?
  game.settings.register("grpga", "statBonusTableName", {
    name: "SETTINGS.StatBonusTableName",
    hint: "SETTINGS.StatBonusTableNameHint",
    scope: "world",
    config: true,
    default: "",
    requiresReload: true,
    type: String,
    onChange: rule => _setStatBonusTable(rule)
  });
  // process this on init even if there is no change
  _setStatBonusTable(game.settings.get("grpga", "statBonusTableName"));

  function _setStatBonusTable(rule) {
    CONFIG.system.statBonusTableName = rule.trim();
  }
  // Register Macro Dialog Delay
  game.settings.register("grpga", "macroDialogDelay", {
    name: "SETTINGS.macroDialogDelay.name",
    hint: "SETTINGS.macroDialogDelay.hint",
    scope: "client",
    config: true,
    default: 500,
    requiresReload: false,
    type: Number
  });

  // Register Combat Tracker in use
  game.settings.register("grpga", "combatTrackerInUse", {
    name: "SETTINGS.CombatTrackerInUse",
    hint: "SETTINGS.CombatTrackerInUseHint",
    scope: "world",
    config: true,
    default: "default",
    requiresReload: true,
    type: String,
    choices: {
      "default": "Foundry Default",
      "pal": "Palladium/ShadowRun4"
    }
  });
  CONFIG.Combat.tracker = game.settings.get("grpga", "combatTrackerInUse");

  game.settings.register("grpga", "systemMigrationVersion", {
    name: "System Migration Version",
    scope: "world",
    config: true,
    type: Number,
    default: 3.18
  });

  game.settings.register("grpga", "systemRefreshOnReady", {
    name: "Refresh items and actors on Ready",
    scope: "world",
    config: false,
    type: Boolean,
    default: false
  });

  // Register initiative rule
  game.settings.register("grpga", "initiativeRule", {
    name: "SETTINGS.initiativeRule.name",
    hint: "SETTINGS.initiativeRule.hint",
    scope: "world",
    config: true,
    default: "rolled",
    requiresReload: true,
    type: String,
    choices: {
      "default": "SETTINGS.initiativeRule.InitDefault",
      "house": "SETTINGS.initiativeRule.InitHouse",
      "house2": "SETTINGS.initiativeRule.InitHouse2",
      "bbf": "SETTINGS.initiativeRule.BBF",
      "dnd": "SETTINGS.initiativeRule.D20",
      "d120": "SETTINGS.initiativeRule.D120",
      "d100": "SETTINGS.initiativeRule.D100",
      "rolled": "SETTINGS.initiativeRule.Rolled",
      "unrolled": "SETTINGS.initiativeRule.Unrolled",
      "oats": "SETTINGS.initiativeRule.OaTS",
      "percent": "SETTINGS.initiativeRule.percent"
    },
    onChange: rule => _setInitiativeRule(rule)
  });
  // process this on init even if there is no change
  _setInitiativeRule(game.settings.get("grpga", "initiativeRule"));

  function _setInitiativeRule(initMethod) {
    let formula;
    switch (initMethod) {
      case "default": formula = "(1d100 - 1) / 1000000 + @dynamic.dx.system.value / 10000 + @bs.value"; break;
      case "house": formula = "2d100 / 1000 + @dynamic.dx.system.value / 1000 + @bs.value"; break;
      case "house2": formula = "(1d750 / 1000) - 0.251 + @bs.value"; break;
      case "bbf": formula = "(@dynamic.initiative.system.moddedvalue)d10kh"; break;
      case "dnd": formula = "1d20 + @dynamic.initiative.system.moddedvalue"; break;
      case "d120": formula = "1d20 + @dynamic.initiative.system.moddedvalue + (1d100 - 1) / 100"; break;
      case "oats": formula = "3d6 + @dynamic.initiative.system.moddedvalue + (1d100 - 1) / 100"; break;
      case "d100": formula = "2d10 + @dynamic.initiative.system.moddedvalue"; break;
      case "rolled": formula = "@bs.value"; break;
      case "unrolled": formula = "@dynamic.initiative.system.moddedvalue"; break;
      case "percent": formula = "1d10 + @dynamic.initiative.system.moddedvalue"; break;
    }

    let decimals = (initMethod == "default") ? 6 : 3;
    CONFIG.Combat.initiative = {
      formula: formula,
      decimals: decimals
    }
  }

  if (CONFIG.system[rschoice]) {
    // Register Primary Attributes
    game.settings.register("grpga", "primaryAttributes", {
      name: "SETTINGS.primaryAttributes.name",
      hint: "SETTINGS.primaryAttributes.hint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.system[rschoice].primaryAttributes.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.system[rschoice].primaryAttributes = game.settings.get("grpga", "primaryAttributes").split(",").map(word => word.trim());

    // Register Header Variables
    game.settings.register("grpga", "headerVariables", {
      name: "SETTINGS.headerVariables.name",
      hint: "SETTINGS.headerVariables.hint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.system[rschoice].headerVariables.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.system[rschoice].headerVariables = game.settings.get("grpga", "headerVariables").split(",").map(word => word.trim());

    // Register Skill Variables
    game.settings.register("grpga", "skillVariables", {
      name: "SETTINGS.skillVariables.name",
      hint: "SETTINGS.skillVariables.hint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.system[rschoice].skillVariables.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.system[rschoice].skillVariables = game.settings.get("grpga", "skillVariables").split(",").map(word => word.trim());

    // Register Spell Variables
    game.settings.register("grpga", "spellVariables", {
      name: "SETTINGS.spellVariables.name",
      hint: "SETTINGS.spellVariables.hint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.system[rschoice].spellVariables.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.system[rschoice].spellVariables = game.settings.get("grpga", "spellVariables").split(",").map(word => word.trim());

    // Register Defence Variables
    game.settings.register("grpga", "defenceVariables", {
      name: "SETTINGS.defenceVariables.name",
      hint: "SETTINGS.defenceVariables.hint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.system[rschoice].defenceVariables.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.system[rschoice].defenceVariables = game.settings.get("grpga", "defenceVariables").split(",").map(word => word.trim());

    // Register Attack Variables
    game.settings.register("grpga", "attackVariables", {
      name: "SETTINGS.attackVariables.name",
      hint: "SETTINGS.attackVariables.hint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.system[rschoice].attackVariables.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.system[rschoice].attackVariables = game.settings.get("grpga", "attackVariables").split(",").map(word => word.trim());
  };

  // Register Automatically making critical rolls
  game.settings.register("grpga", "autoCriticalRolls", {
    name: "SETTINGS.autoCriticalRolls.name",
    hint: "SETTINGS.autoCriticalRolls.hint",
    scope: "world",
    config: true,
    default: true,
    type: Boolean
  });

  // Register Show Test Data
  game.settings.register("grpga", "showTestData", {
    name: "SETTINGS.showTestData.name",
    hint: "SETTINGS.showTestData.hint",
    scope: "world",
    config: true,
    default: false,
    requiresReload: false,
    type: Boolean,
    onChange: rule => _setTestMode(rule)
  });
  // process this on init even if there is no change
  _setTestMode(game.settings.get("grpga", "showTestData"));

  function _setTestMode(rule) {
    CONFIG.system.testMode = rule;
  }

  // Register Show Hooks Data
  game.settings.register("grpga", "showHooks", {
    name: "SETTINGS.showHooks.name",
    hint: "SETTINGS.showHooks.hint",
    scope: "world",
    config: true,
    default: false,
    requiresReload: false,
    type: Boolean,
    onChange: rule => _setShowHooks(rule)
  });
  // process this on init even if there is no change
  _setTestMode(game.settings.get("grpga", "showHooks"));

  function _setShowHooks(rule) {
    CONFIG.debug.hooks = rule;
  }

  // Register Resetting of Temporary Items on sheet closing
  game.settings.register("grpga", "resetTemps", {
    name: "SETTINGS.resetTemps.name",
    hint: "SETTINGS.resetTemps.hint",
    scope: "client",
    config: true,
    default: false,
    requiresReload: true,
    type: Boolean
  });
  CONFIG.system.resetTemporaryItemsOnSheetClose = game.settings.get("grpga", "resetTemps");

  game.settings.register("grpga", "narrateJustifyLeft", {
    name: "SETTINGS.narrateJustifyLeft.name",
    hint: "SETTINGS.narrateJustifyLeft.hint",
    scope: "client",
    config: true,
    default: false,
    requiresReload: true,
    type: Boolean
  });
  CONFIG.system.narrateJustifyLeft = game.settings.get("grpga", "narrateJustifyLeft");

  // Register whether using FourAmigos House Rules
  game.settings.register("grpga", "useFAHR", {
    name: "SETTINGS.useFAHR.name",
    hint: "SETTINGS.useFAHR.hint",
    scope: "world",
    config: true,
    default: false,
    requiresReload: true,
    type: Boolean
  });

  /*
  game.settings.register("grpga", "effectSize", {
    name: "SETTINGS.TokenEffectSize",
    hint: "SETTINGS.TokenEffectSizeHint",
    default: CONFIG.tokenEffects.effectSizeChoices.small,
    scope: "client",
    type: String,
    choices: CONFIG.tokenEffects.effectSizeChoices,
    config: true,
    onChange: s => {
      TokenEffects.patchCore();
      canvas.draw();
    }
  });
  TokenEffects.patchCore();
*/
  // ================ end of system settings

  // ================ beginning of system setup
  if (CONFIG.Combat.tracker == "pal") {
    CONFIG.Combat.documentClass = ActionPointCombat;
    CONFIG.ui.combat = ActionPointCombatTracker;
    CONFIG.Combatant.documentClass = ActionPointCombatant;
    CONFIG.Combat.sheetClass = ActionPointCombatantConfig;
    CONFIG.time.roundTime = 10;
  } else {
    // use the Foundry Default settings
    CONFIG.Combat.documentClass = GRPGACombat;
  }

  // Change the thickness of the border around Objects. Default = 4
  CONFIG.Canvas.objectBorderThickness = 8;
  CONFIG.system.ruleset = rschoice;
  CONFIG.statusEffects = CONFIG[`statusEffects${rschoice}`];

  switch (rschoice) {
    case "3d6": {
      CONFIG.system.chartype = "Character3D6";
      CONFIG.system.itemtypes = [
        "Modifier",
        "Variable",
        "Pool",
        "Defence",
        "Equipment",
        "Hit-Location",
        "Melee-Attack",
        "Primary-Attribute",
        "Ranged-Attack",
        "Rollable",
        "Container",
        "Trait"
      ];
      break;
    }
    case "d100": {
      CONFIG.system.chartype = "CharacterD100";
      CONFIG.system.itemtypes = [
        "Modifier",
        "Variable",
        "Pool",
        "Defence",
        "Equipment",
        "Hit-Location",
        "Melee-Attack",
        "Primary-Attribute",
        "Ranged-Attack",
        "Rollable",
        "Container",
        "Trait",
        "Rank-Progression",
        "Wound"
      ];
      break;
    }
    case "mnm": {
      CONFIG.system.chartype = "CharacterMnM";
      CONFIG.system.itemtypes = [
        "Modifier",
        "Variable",
        "Pool",
        "Defence",
        "Equipment",
        "Hit-Location",
        "Melee-Attack",
        "Primary-Attribute",
        "Ranged-Attack",
        "Rollable",
        "Container",
        "Trait",
        "Power",
        "Advantage"
      ];
      break;
    }
    case "bbf": {
      CONFIG.system.chartype = "CharacterBBF";
      CONFIG.system.itemtypes = [
        "Modifier",
        "Variable",
        "Pool",
        "Melee-Attack",
        "Primary-Attribute",
        "Ranged-Attack",
        "Container",
        "Rollable"
      ];
      break;
    }
    case "d6": {
      CONFIG.system.chartype = "CharacterD6";
      CONFIG.system.itemtypes = [
        "Modifier",
        "Variable",
        "Pool",
        "Defence",
        "Melee-Attack",
        "Primary-Attribute",
        "Ranged-Attack",
        "Container",
        "Trait",
        "Rollable"
      ];
      break;
    }
    default: {
      CONFIG.system.chartype = "CharacterD120";
      CONFIG.system.itemtypes = [
        "Modifier",
        "Variable",
        "Pool",
        "Defence",
        "Equipment",
        "Hit-Location",
        "Melee-Attack",
        "Primary-Attribute",
        "Ranged-Attack",
        "Rollable",
        "Container",
        "Trait",
        "Rank-Progression"
      ];
      break;
    }
  }

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Items.unregisterSheet("core", ItemSheet);

  Actors.registerSheet("grpga", Actor3D6Sheet, {
    types: ["Character3D6"],
    makeDefault: true,
    label: "Dynamic 3D6 Character"
  });
  Actors.registerSheet("grpga", Actor3D6FAHRSheet, {
    types: ["Character3D6"],
    makeDefault: false,
    label: "Four Amigos Style 3D6 Character"
  });

  Actors.registerSheet("grpga", ActorD100Sheet, {
    types: ["CharacterD100"],
    makeDefault: true,
    label: "Dynamic D100 Character"
  });

  Actors.registerSheet("grpga", ActorMnMSheet, {
    types: ["CharacterMnM"],
    makeDefault: true,
    label: "Mutants & Masterminds Character"
  });

  Actors.registerSheet("grpga", ActorBBFSheet, {
    types: ["CharacterBBF"],
    makeDefault: true,
    label: "BareBones Fantasy Character"
  });

  Actors.registerSheet("grpga", ActorD6Sheet, {
    types: ["CharacterD6"],
    makeDefault: true,
    label: "D6 Character"
  });

  Actors.registerSheet("grpga", ActorD120Sheet, {
    types: ["CharacterD120"],
    makeDefault: true,
    label: "Palladium Character"
  });

  Items.registerSheet("grpga", BaseItemSheet, {
    types: ["Primary-Attribute", "Melee-Attack", "Ranged-Attack", "Rank-Progression", "Rollable", "Power", "Trait", "Defence", "Equipment", "Hit-Location", "Advantage", "Wound"],
    makeDefault: true,
    label: "Items"
  });
  Items.registerSheet("grpga", PoolItemSheet, {
    types: ["Pool"],
    makeDefault: true,
    label: "Pool Item"
  });
  Items.registerSheet("grpga", ModifierItemSheet, {
    types: ["Modifier"],
    makeDefault: true,
    label: "Modifier Item"
  });
  Items.registerSheet("grpga", VariableItemSheet, {
    types: ["Variable"],
    makeDefault: true,
    label: "Variable Item"
  });
  Items.registerSheet("grpga", ContainerItemSheet, {
    types: ["Container"],
    makeDefault: true,
    label: "Container Item"
  });

  preloadHandlebarsTemplates();

  // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper('concat', function () {
    var outStr = '';
    for (var arg in arguments) {
      if (typeof arguments[arg] != 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('toLowerCase', function (str) {
    return str.toLowerCase();
  });

  Handlebars.registerHelper('contains', function (str, text) {
    return str.includes(text);
  });

  Handlebars.registerHelper('isModulus', function (value, div, rem) {
    return value % div == rem;
  });

  Handlebars.registerHelper('debug', function (text, content) {
    return console.debug(text, content);
  });

  Handlebars.registerHelper('toSentenceCase', function (str) {
    return str.replace(
      /\w\S*/g,
      function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }
    );
  });

  Handlebars.registerHelper('times', function (n, content) {
    let result = "";
    for (let i = 0; i < n; ++i) {
      result += content.fn(i);
    }
    return result;
  });

  /**
   * A helper to create a set of radio checkbox input elements in a named set.
   * The provided keys are the possible radio values while the provided values are human readable labels.
   *
   * @param {string} name         The radio checkbox field name
   * @param {object} choices      A mapping of radio checkbox values to human readable labels
   * @param {string} options.checked    Which key is currently checked?
   * @param {boolean} options.localize  Pass each label through string localization?
   * @return {Handlebars.SafeString}
   *
   * @example <caption>The provided input data</caption>
   * let groupName = "importantChoice";
   * let choices = {a: "Choice A", b: "Choice B"};
   * let chosen = "a";
   *
   * @example <caption>The template HTML structure</caption>
   * <div class="form-group">
   *   <label>Radio Group Label</label>
   *   <div class="form-fields">
   *     {{radioBoxes groupName choices checked=chosen localize=true}}
   *   </div>
   * </div>
   */
  Handlebars.registerHelper('radioLabels', function (name, choices, options) {
    const checked = options.hash['checked'] || null;
    const localize = options.hash['localize'] || false;
    let html = "";
    for (let [key, label] of Object.entries(choices)) {
      if (localize) label = game.i18n.localize(label);
      const isChecked = checked === key;
      html += `<input type="radio" id="${label}" name="${name}" value="${key}" ${isChecked ? "checked" : ""}><label for="${label}">${label}</label>`;
    }
    return new Handlebars.SafeString(html);
  });
});
